FROM node:8.6

ARG MTBLC_NPM_TOKEN
ARG STRIPE_PUBLIC_KEY
ENV APP_DIR /usr/src/app

RUN mkdir -p $APP_DIR
WORKDIR $APP_DIR

RUN echo "@mtblc:registry=https://npm.mtblc.co/\n\
          @mtblc:always-auth=true\n\
          //npm.mtblc.co/:_authToken=\${MTBLC_NPM_TOKEN}" > $APP_DIR/.npmrc

COPY package.json yarn.lock ./
RUN yarn install && rm $APP_DIR/.npmrc

COPY . $APP_DIR
RUN yarn run client-build

CMD [ "node", "server/server.js" ]
