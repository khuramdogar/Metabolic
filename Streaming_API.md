# Streaming API

We use [Primus](https://github.com/primus/primus) as an abstraction layer for the streaming API.
It's configured to use Engine.io so it will have all the possible transport options available there.
The main one being Websockets.

## Client library

We have a client library to connect to Primus with all the required plugins. It can be downloaded from `/primus/primus.js`.

### Emitting events

We are using [primus-emit](https://github.com/primus/primus-emit) plugin so in order to send events
from the client you need to do

```js
primus.emit('eventName', data);
```

## Connection endpoint

The endpoint to connect to is `/primus`.

## Authentication

The initial connection must contain the access token as a query parameter with the name of `access_token`.

## Received events

### entries:subscribe

Subscribes to receive events related to a specific entry.

#### Params

* `data`: object with:
  * `entryId`: id of an Entry. **Required**.

### entries:unsubscribe

Unsubscribes to a previously subscribed entry. If the entry was not subscribed it does nothing.

#### Params

* `data`: object with:
  * `entryId`: id of an Entry. **Required**.

### queues:subscribe

Subscribes to receive events related to the entry queues, considering both, the pending entries and
the agent specific entries.

### queues:unsubscribe

Unsubscribe to all entries queues for this user.

### nutrition-tips:subscribe

Subscribes to receive events related to a specific nutrition tip.

#### Params

* `data`: object with:
  * `nutritionTipId`: id of an NutritionTip. **Required**.

### nutrition-tips:unsubscribe

Unsubscribes from a previously subscribed nutrition tip. If the nutrition tip was not subscribed it does nothing.

### feed:subscribe

Subscribes the current user to receive events from multiple sources.

### feed:unsubscribe

Unsubscribes from the user's feed.

### users:metabolic-chat:subscribe

Subscribes to receive metabolic-chat messages related to a specific user.

#### Params

* `data`: object with:
  * `userId`: id of a user. **Required**.

#### Example

```javascript
    primus.emit('users:metabolic-chat:subscribe', { userId });
```

### users:metabolic-chat:unsubscribe

Unsubscribes the user from a previously subscribed chat. If the user was not subscribed it does nothing.

#### Data

* `data`: object with:
  * `userId`: id of a user. **Required**.

### metabolic-chat:list:subscribe

Subscribes to receive events related to the *MetabolicChat List*.

### metabolic-chat:list:unsubscribe

Unsusbcribes from *MetabolicChat List*.

## Sent events

### entries:subscribed

Subscription to an entry was successful (after sending [entries:subscribe](#entries-subscribe)).

#### Data

Object with:
* `entryId`: id of the entry

### entries:chat:messages

After subscribing successfully (and receiving [entries:subscribed](#entries-subscribed) event), this will be sent with all the chat messages currently available for the subscribed entry.

#### Data

Object with:

* `entryId`: id of the entry
* `messages`: Array with all the messages

### entries:chat:new-message

Any new message in a subscribed entry will be emitted with this event.

#### Data

Object with:

* `entryId`: id of the entry
* `message`: A message instance with the following attributes:
  * `text`: text of the message
  * `imagesData`: array of images information (`name`, ' location')
  * `authorId`: author's user id
  * `createdAt`: creation date

### entries:error

Sent when something related to entries went wrong.

#### Data: string explaining the error

### entries:unsubscribed

Sent when unsubscription (with [entries:unsubscribe](#entries-unsubscribe)) was successful.

#### Data

* `entryId`: id of the entry

### queues:subscribed

Sent after a successful subscription to entries queues.

### queues:current

After a successful subscription to agent queues ([queues:subscribe](queues-subscribe)) this event will be emitted
for and will contain current state of all queues.

The queues and the corresponding entry summaries (according to their status) are:
* **pending**: entries in the _pending_ status
* **active**: entries in the _claimed_ status
* **waiting**: entries in the _needsInfo_ status
* **inactive**: entries in either _answered_ or _evaluated_ status

#### Data

Object with:
* `queues`: object with a key per queue
  * &lt;queueName&gt;: Array of entries

### queues:changes

Emitted to notify of change in the queues.

#### Data

Object with:
* `queue`: the name of the queue where a change occurred
* `operation`: either `add` or `remove`
* `entryId`: id of the entry being added or removed
* `entry`: JSON object with the entry's details when it's an `add` operation

### queues:unsubscribed

Sent after a successful unsubscription to entries queues.

### nutrition-tips:subscribed

Subscription to a nutrition tip was successful (after sending [nutrition-tips:subscribe](#nutrition-tips-subscribe)).

#### Data

Object with:
* `nutritionTipId`: id of the nutrition tip

### nutrition-tips:chat:messages

After subscribing successfully (and receving [nutrition-tips:subscribed](#nutrition-tips-subscribed) event), this will be sent with all the chat messages currently available for the subscribed nutrition tip.

#### Data

Object with:

* `nutritionTipId`: id of the nutrition tip
* `messages`: Array with all the messages

### nutrition-tips:chat:new-message

Any new message in a subscribed nutrition tip will be emitted with this event.

#### Data

Object with:

* `nutritionTipId`: id of the nutrition tip
* `message`: A message instance with the following attributes:
  * `id`: id of the message
  * `text`: text of the message
  * `imagesData`: array of images information (`name`, `location`)
  * `authorId`: author's user id
  * `author`: object with author information (`name`, `email`)
  * `fromAgent`: boolean, true if the author is an agent
  * `createdAt`: creation date
  * `updatedAt`: last update date

### nutrition-tips:error

Sent when something related to nutrition tips went wrong.

#### Data: string explaining the error

### nutrition-tips:unsubscribed

Sent when an unsubscription (with [nutrition-tips:unsubscribe](#nutrition-tips-unsubscribe)) was successful.

#### Data

* `nutritionTipId`: id of the nutrition tip

### feed:subscribed

Subscription to the user's feed was successful (after sending [feed:subscribe](#feed-subscribe)).

### feed:unsubscribed

Sent when an unsubscription (with [feed:unsubscribe](#feed-unsubscribe)) was successful.

### feed:update

Sent when there is an update to the user's feed.

#### Data

* `entity`: Object, representation of the updated entity:
  * `id`: id of the entity
  * `type`: type of the entity (eg: NutritionTip)
* `metadata`: Object, update information:
  * `action`: string, what triggered the update (eg: update)
  * `badge`: integer, a number that can be used as a badge (eg: number of unread messages)

### users:metabolic-chat:subscribed

A user's subscription was successful (after sending [users:metabolic-chat:subscribe](#users:metabolic-chat:subscribe)).

### users:metabolic-chat:unsubscribed

Sent after a successful unsubscription to a user's chat.

### users:metabolic-chat:error

Sent when something related to the subscribed user went wrong.

#### Data

Just a plain string explaining the error.

#### Example

```javascript
primus.on('users:metabolic-chat:error', (error) => {
  console.error(error);
});
```

### users:metabolic-chat:messages

After subscribing successfully (and receiving [users:metabolic-chat:subscribed](#users-metabolic-chat-subscribed) event), this will be sent with all the chat messages currently available for the subscribed user.

#### Data

Object with:

* `userId`: id of the user
* `messages`: Array with all the messages (see bellow for the message schema)

#### Example

```javascript
primus.on('users:metabolic-chat:messages', (data) => {
  const { userId, messages } = data;
});
```

### users:metabolic-chat:new-message

Any new message in a subscribed user will be emitted with this event.

#### Data

A message has the following schema:

* `userId`: id of the user
* `message`: A message instance with the following attributes:
  * `id`: id of the message
  * `imagesData`: array of images information (`name`, `location`)
  * `authorId`: author's user id
  * `author`: object with author information (`name`, `email`)
  * `text`: text of the message
  * `sourceId`: string, id of the source
  * `sourceType`: string, model name of the source
  * `source`: object with source information. Has the following attributes:
      * If `sourceType === 'NutritionTip'`, attributes are:
        * `id`: nutritionTip id
        * `message`: string, message of the nutrition tip
  * `fromAgent`: boolean, true if the author is an agent
  * `createdAt`: creation date
  * `updatedAt`: last update date

**NOTE**: A message can have only the text or the source, not both.

#### Example

```javascript
primus.on('users:metabolic-chat:messages', (data) => {
  const { userId, message } = data;
  if (message.source) {
    if (message.sourceType === 'NutritionTip') {
      console.log('message:', message.source.message);
    }
  } else {
      console.log('message:', message.text);
  }
});
```

### metabolic-chat:list:subscribed

Subscription to MetabolicChat's list was successful.

### metabolic-chat:list:users

Triggered after subscribing successfully (and receiving [metabolic-chat:list:subscribed](#metabolic-chat-list-subscribed)).

You will receive a list with all users, sorted by last message's creation date.

#### Data

An object with the following schema:

* `chats`: Array of objects. Every object of this array has the following attributes:
  * `user`:
    * `id`: string
    * `name`: string
    * `email`: string
    * `phoneNumber`: string
    * `timezone`: string
    * `avatarData`: object,
      * `name`: string,
      * `location`: string, image URL of the avatar
      * `mediaType`: string, mediaType of the image
      * `id`: string
      * `thumbnailLocation`: string
  * `lastMessageAt`: string, date of the last message sent in the user's channel.

#### Example

```javascript
primus.on('metabolic-chat:list:users', (data) => {
  const { chats } = data;
  chats.forEach((chat) => {
    const { user, lastMessageAt } = chat;
  })
})
```

### metabolic-chat:list:unsubscribed

Unsubscription to MetabolicChat's list was successful.

### metabolic-chat:list:new-message

Triggered when a MetabolicMessage is created.

#### Data

An object with the following schema:

* `user`:
  * `id`: string
  * `name`: string
  * `email`: string
  * `phoneNumber`: string
  * `timezone`: string
  * `avatarData`: object,
    * `name`: string,
    * `location`: string, image URL of the avatar
    * `mediaType`: string, mediaType of the image
    * `id`: string
    * `thumbnailLocation`: string
* `lastMessageAt`: string, date of the last message sent in the user's channel.

#### Example

```javascript
primus.on('metabolic-chat:list:new-message', (chat) => {
  const { user, lastMessageAt } = chat;
})
```

### metabolic-chat:list:error

Sent when something related to the _MetabolicChat list_ went wrong.

#### Data

Just a plain string with the explanation.
