# REST API

Most of the REST API is documented in the explorer you can find in `/explorer`
(for example, https://vitamind.mtblc.co/explorer in the development server). But that swagger UI
does not have the entire documentation for some endpoints, so here is the missing one. Only endpoints
with missing documentation will be listed here, and only the missing documentation for the moment.

## Agent

### POST /agents/login

#### Request body

JSON object with:

* `email`
* `password`

## Entries

### PATCH /entries/{id}

Updates this entry. To add photos use the same photos attribute you use when creating an entry. Always send the photosData representing photos you want to keep; the ones not present will be removed.

#### Request Body

JSON object with all the properties of an entry plus `photos` attribute for the new added photos. The `photosData`
property only requires the `id` property of each existing photo to keep.

### POST /entries/{id}/chat/messages

Creates a new message in the entry's chat.

#### Request Body

JSON object if request does not contain images or multipart/form-data if it does, wit the following properties:

* `text`: string
* `photos`: image file as formatted from `FormData`

### PUT /entries/{id}/review

Creates or replace the current review for this entry. This can only be called by the agent who claimed
the entry in question.

This endpoint allows you to send the following data:

* `action`: **required** parameter to indicate the flow taken by the agent. The possible values can be:
  * `evaluate`: the agent wants to submit evaluation (evaluation parameters will be required)
  * `answer`: the agent will just submit a message and mark the entry as answered
  * `requestInfo`: the agent will just send a message and mark the entry as it needs info from the user
* `eatenFoods` or `nutritionalData`: if the action is `evaluate` then you need to send any of these two. See [createEvaluation](#put-entries-id-evaluation) for details on these parameters.
* `message`: optional chat message to append to this entry as part of this review. It can be like a comment to the user for the evaluation, the actual answer when this was a question-type entry or what's the requested info when the agent needs input from the user. As when [creating a chat message](#post-entries-id-chat-messages) it supports the following subproperties:
  * `text`
  * `photos`: this one can be sent many times, and it will be considered as an array of images
* `nutritionTip`: optional, if the agent wants to set a new current nutrition tip to be displayed in the top section in day view. It just receives `message` attribute containing the text to be displayed.
* `analytics`: optional, if the agent wants to set an evaluation or classify this review. Actually a traffic light evaluation (green, yellow, red) is available.

These parameters can be sent in JSON format, as form-urlencoded format or as multipart/form-data. The latter should only be used if you are sending files (`message.photos`), and the recommended way from JS clients is to use `FormData` class. If there are no files in the request, sending multipart format will cause an error.

Example body in JSON format for an evaluation with a text-only message (and nutrition tip:

```json
{
  "message": {
    "text": "Nice healthy meal, well done!"
  },
  "action": "evaluate",
  "nutritionalData": {
    "protein": 20,
    "carbs": 10,
    "fat": 30,
    "calories": 390
  },
  "nutritionTip": {
    "message": "Considering adding some seeds or avocado for some very healthy fat"
  },
  "analytics": {
    "trafficLightEvaluation": "green"
  }
}
```

Example parameters to be sent for an answer with a photo & text message (so multipart/form-data required):

* `action=answer`
* `message[text]=Choose the hamburger, but try to switch the fries by some vegetable sticks`
* `message[photos]=<the photo file>`
* `message[photos]=<another photo file>`

### PUT /entries/{id}/evaluation

Creates or replaces the nutritional evaluation for this entry.

#### Request Body

JSON object with the following properties:

* `eatenFoods`: Array of objects, each one representing a food (in measure terms) considered by this entry, specified with:
  * `foodId`: internal id of the included food
  * `measureId`: internal id of the measure for this food
  * `measureAmount`: amount of measure for this food in this meal
* `nutritionalData`: manual way of entering nutrients when no foods seem correct. Object with:
  * `carbs`: number of grams
  * `protein`: number of grams
  * `fat`: number of grams
  * `calories`: number of kcal

Either `eatenFoods` or `nutritionalData` should be present. If both are present, `eatenFoods` will be ignored.

## Foods

### GET /food

Searches for foods

#### Response

JSON array with each object having the following properties:

* `name`: food's description
* `externalId`: id for this food in the external source
* `externalSource`: which external source this food belongs to

## Nutrition Tips

### POST /NutritionTips/{id}/chat/messages

Creates a new message in the nutrition tip's chat.

#### Request Body

JSON object if request does not contain images or multipart/form-data if it does, with the following properties:

* `text`: string
* `photos`: image file as formatted from `FormData`

## Users (MetabolicUsers)

### POST /users

Signs up a new user.

#### Request body

JSON with user's properties. Only the following ones are required (the rest should not be given at signup).

* `email`
* `name`
* `password`

### GET /users/{id}/day/{date}

Gets the info for a specific user's day

#### Path parameters

* `date`: ISO format for dates (YYYY-MM-DD)

#### Query parameters

* timezone: optional parameters to force response considering a specific timezone to evaluate the day

#### Response

JSON object with:

* `date`: given date
* `timezone`: timezone considered in the calculation
* `currentNutritionTip`: current nutrition tip object (`message` is the most important property here)
* `nutritionSummary`: array of objects each summarizing a specific nutrient, with the following properties:
  * `name`: nutrient's name
  * `unit`: unit used for this nutrient
  * `humanName`: to display to humans
  * `value`: number in the specified unit
* `entries`: array with the summary of each entry considered for this day. Each entry will have the following properties:
  * `id`
  * `parentEntryId`
  * `userId`
  * `user`: a summary of the client (`id`, `name`, `email`, `phoneNumber`, `avatarData` and `timezone`)
  * `agentId`
  * `agent`: a summary of the agent, if it corresponds (`id`, `email`, `phoneNumber`, `name` and `avatarData`)
  * `text`
  * `photosData`
  * `status`
  * `datetime`
  * `timezone`
  * `createdAt`

### GET /users/{id}/day/{date}/newMessages

Gets the amount of unread messages per entry for user since selected day

#### Path parameters

* `id`: the id of the user who owns the entries
* `date`: ISO format for dates, or in the form YYYY-MM-DD

#### Response

JSON object which maps the `id` of each entry with the amount of unread messages on it. For example:

```json
{
  "5989eac917eaf83964f6b0b5": 4,
  "5989eac917eaf83964f6b0b7": 3,
  "5989eac917eaf83964f6b0b2": 5
}
```

### GET /users/{id}/entries/search

Search in the user's entries

#### Request body

JSON object with:

* `date`: Start and end date for search. `{ "start": "YYYY-MM-DD", "end": "YYYY-MM-DD" }`
* `time`: Start and end time for search. `{ "start": "HH:MM:SS", "end": "HH:MM:SS" }`. This parameter is optional, and can specify start, end or both. Default value for `start` is 00:00:00 and for `end` is 23:59:59.
* `preset`: Preset search. Can take the values: `lastWeekMeals`, `lastWeekBreakfasts`, `lastWeekLunches` and `lastWeekDinners`.

**Date or preset** must to be present in the request.

Other filters are:

* `status`: Entry's status. This value can be a string or an array of string with values: `pending`, `claimed`, `needsInfo`, `answered`, `evaluated`.
* `analytics`: Entry's analytics. JSON object to filter by this field.

Example of request JSON:

```json
{
  "date": {
    "start": "2017-05-22",
    "end": "2017-08-22"
  },
  "time": {
    "start": "13:05"
  },
  "status": "pending",
  "analytics": {
    "trafficLightEvaluation": ["green", "red"]
  }
}
```

Another example of request:

```json
{
  "preset": "lastWeekMeals",
  "status": ["pending", "evaluate"]
}
```

### POST /users/{id}/entries/withImages

#### Request body

JSON object if request does not contain photos or multipart/form-data if it does, wit the following properties:

* `text`: description of this entry
* `timezone`: current device timezone when creating this entry
* `photos`: image file as formatted from `FormData`

### PATCH /users/{id}/updateProfile

#### Request body

JSON object if request does not contain photos or multipart/form-data if it does, with the following properties:

* `name`: user's real name
* `phoneNumber`: formatted string representing the phone number
* `avatar`: image to be used as user's avatar, as formatted by `FormData`

### PATCH /users/{id}/timezone

#### Request body

JSON object with the following properties:

* `timezone`: user's new timezone

#### Response

JSON object with user's information

### POST /users/login

#### Request body

JSON object with:

* `email`
* `password`

### GET /users/{id}/friendships/latestEntries

Gets the latest entry (according to the `createdAt` property) for each one of the user's friends.

#### Response

Array with the summary of each entry.

### GET /users/{id}/nutritionTips/unread

Gets all unread nutrition tips for the selected user created during the past seven days.

#### Response

Array of unread nutrition tips.

### POST /users/{id}/metabolicChat/messages

#### JSON object with:

JSON object if request does not contain images or multipart/form-data if it does, with the following properties:

* `text`: string
* `photos`: image file as formatted from `FormData`

## Spelling

### POST /spelling/check

#### Request body

JSON object with:

* `text`

#### Response

JSON object with:

* `result`: if there are errors or not in the proxied request.
* `errors`: array of spelling errors.
