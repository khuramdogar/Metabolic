import { combineReducers } from 'redux';
import customers from './customers';
import summaries from './summaries';
import entities from './entities';
import oneChangePeriods from './one-change-periods';

export default combineReducers({
  customers,
  summaries,
  entities,
  oneChangePeriods,
});
