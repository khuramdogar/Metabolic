import { Map } from 'immutable';
import { normalize } from 'normalizr';
import { createSelector } from 'reselect';
import { showNotification } from '../../../../common/store/actions/ui/notification';
import { getData } from './common';
import { getEntities } from './entities';
import apiService from '../../../services/api';
import { customerSchema } from '../../../../common/schemas';

const INITIAL_STATE = new Map({
  stats: new Map(),
  entriesLogs: new Map(),
  dayStats: new Map(),
  weekStats: new Map(),
  entriesByDay: new Map(),
  overview: new Map(),
});

const SET_CUSTOMERS = 'customers/SET';
const ADD_CUSTOMER = 'customers/ADD';
const SET_CUSTOMER_STATS = 'customers/stats/SET';
const SET_CUSTOMER_ENTRIES_LOG = 'customers/entriesLog/SET';
const SET_CUSTOMER_DAY_STATS = 'customers/dayStats/SET';
const SET_CUSTOMER_WEEK_STATS = 'customers/weekStats/SET';
const SET_CUSTOMER_ENTRIES_BY_DAY = 'customers/entriesByDay/SET';
const SET_CUSTOMERS_OVERVIEW = 'customers/overview/SET';

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_CUSTOMER_STATS:
      return state.setIn(['stats', action.payload.customerId], action.payload);
    case SET_CUSTOMER_ENTRIES_LOG:
      return state.setIn(['entriesLogs', action.payload.customerId], action.payload);
    case SET_CUSTOMER_DAY_STATS:
      return state.setIn(['dayStats', action.payload.customerId], action.payload);
    case SET_CUSTOMER_WEEK_STATS:
      return state.setIn(['weekStats', action.payload.customerId], action.payload);
    case SET_CUSTOMER_ENTRIES_BY_DAY:
      return state.setIn(['entriesByDay', action.payload.customerId], action.payload);
    case SET_CUSTOMERS_OVERVIEW:
      return state.setIn(['overview', action.payload.day], action.payload.result);
    default:
      return state;
  }
}

export function loadCustomer(userId) {
  return dispatch => apiService.users.getById(userId)
    .then((user) => {
      const normalizedCustomer = normalize(user, customerSchema);
      dispatch({ type: ADD_CUSTOMER, payload: normalizedCustomer });
    });
}

export function loadCustomers() {
  return dispatch => apiService.users.list()
    .then((users) => {
      const normalizedCustomers = normalize(users, [customerSchema]);
      dispatch({ type: SET_CUSTOMERS, payload: normalizedCustomers });
    });
}

export function loadCustomerStats(customerId) {
  return dispatch => apiService.customerStats.overview(customerId)
    .then(payload => dispatch({ type: SET_CUSTOMER_STATS, payload }))
    .catch(() => dispatch(
      showNotification({
        message: 'There was an error loading the stats.',
        duration: 7000,
      })));
}

export function loadCustomerEntriesLog(customerId) {
  return dispatch => apiService.customerStats.log(customerId)
    .then(payload => dispatch({ type: SET_CUSTOMER_ENTRIES_LOG, payload }))
    .catch(() => dispatch(
      showNotification({
        message: 'There was an error loading the stats.',
        duration: 7000,
      })));
}

export function loadCustomerDayStats(customerId) {
  return dispatch => apiService.customerStats.day(customerId)
    .then(payload => dispatch({ type: SET_CUSTOMER_DAY_STATS, payload }))
    .catch(() => dispatch(
      showNotification({
        message: 'There was an error loading the stats.',
        duration: 7000,
      })));
}

export function loadCustomerWeekStats(customerId) {
  return dispatch => apiService.customerStats.week(customerId)
    .then(payload => dispatch({ type: SET_CUSTOMER_WEEK_STATS, payload }))
    .catch(() => dispatch(
      showNotification({
        message: 'There was an error loading the stats.',
        duration: 7000,
      })));
}

export function loadCustomerEntriesByDay(customerId) {
  return dispatch => apiService.customerStats.entriesByDay(customerId)
    .then(payload => dispatch({ type: SET_CUSTOMER_ENTRIES_BY_DAY, payload }))
    .catch(() => dispatch(
      showNotification({
        message: 'There was an error loading the stats.',
        duration: 7000,
      })));
}

export function loadCustomersOverview(day) {
  return dispatch => apiService.customerStats.generalOverview(day)
    .then((result) => {
      const data = normalize(result, { data: [{ user: customerSchema }] });
      dispatch({ type: SET_CUSTOMERS_OVERVIEW, payload: { ...data, day } });
    })
    .catch(() => dispatch(
      showNotification({
        message: 'There was an error loading the stats.',
        duration: 7000,
      })));
}

export const getCustomers = createSelector(getData, data => data.customers);

const getCustomersEntities = createSelector(getEntities, entities => entities.get('customers'));

export const getCustomerEntitiesList = createSelector(
  getCustomersEntities,
  customerEntities => customerEntities.toList(),
);

function getCustomerId(state, props) {
  return props.customerId;
}

function getCurrentDate(state, props) {
  return props.currentDate;
}

export const getCustomer = createSelector(
  getCustomerId,
  getCustomersEntities,
  (customerId, customersEntities) => customersEntities.get(customerId),
);

const getCustomerStatsMap = createSelector(
  getCustomers,
  customers => customers.get('stats'),
);

const getCustomerEntriesLogsMap = createSelector(
  getCustomers,
  customers => customers.get('entriesLogs'),
);

const getCustomerDayStatsMap = createSelector(
  getCustomers,
  customers => customers.get('dayStats'),
);

const getCustomerWeekStatsMap = createSelector(
  getCustomers,
  customers => customers.get('weekStats'),
);

const getCustomerEntriesByDayMap = createSelector(
  getCustomers,
  customers => customers.get('entriesByDay'),
);

const getCustomersOverviewMap = createSelector(
  getCustomers,
  customers => customers.get('overview'),
);

export const getCustomerStats = createSelector(
  getCustomerId,
  getCustomerStatsMap,
  (customerId, stats) => stats.get(customerId),
);

export const getCustomerEntriesLog = createSelector(
  getCustomerId,
  getCustomerEntriesLogsMap,
  (customerId, entriesLogs) => entriesLogs.get(customerId),
);

export const getCustomerDayStats = createSelector(
  getCustomerId,
  getCustomerDayStatsMap,
  (customerId, dayStats) => dayStats.get(customerId),
);

export const getCustomerWeekStats = createSelector(
  getCustomerId,
  getCustomerWeekStatsMap,
  (customerId, weekStats) => weekStats.get(customerId),
);

export const getCustomerEntriesByDay = createSelector(
  getCustomerId,
  getCustomerEntriesByDayMap,
  (customerId, entriesByDay) => entriesByDay.get(customerId),
);

export const getCustomersOverview = createSelector(
  getCurrentDate,
  getCustomersOverviewMap,
  getCustomersEntities,
  (currentDate, overviewMap, customerEntities) => {
    const result = overviewMap.get(currentDate);
    if (result && result.data && result.data.length) {
      return {
        ...result,
        data: result.data.map(item => ({ ...item, user: customerEntities.get(item.user) })),
      };
    }
    return result;
  },
);
