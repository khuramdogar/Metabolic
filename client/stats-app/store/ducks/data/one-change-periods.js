import { Map } from 'immutable';
import { normalize } from 'normalizr';
import { createSelector } from 'reselect';
import { getData } from './common';
import { showNotification } from '../../../../common/store/actions/ui/notification';
import { getEntities } from './entities';
import apiService from '../../../services/api';
import { oneChangePeriodSchema } from '../../../../common/schemas';

const INITIAL_STATE = new Map({
  customerToCurrentOneChange: new Map(),
});

const SET_CUSTOMER_CURRENT_ONE_CHANGE_PERIOD = 'oneChangePeriods/current/SET';

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_CUSTOMER_CURRENT_ONE_CHANGE_PERIOD:
      return state.setIn(['customerToCurrentOneChange', action.payload.customerId], action.payload.result);
    default:
      return state;
  }
}

function setCustomerOneChangePeriod(oneChangePeriod) {
  const normalizedOCP = normalize(oneChangePeriod, oneChangePeriodSchema);
  return {
    type: SET_CUSTOMER_CURRENT_ONE_CHANGE_PERIOD,
    payload: { ...normalizedOCP, customerId: oneChangePeriod.customerId },
  };
}

export function replaceCurrent(customerId, oneChange, update) {
  return dispatch => apiService.oneChangePeriods.replaceCurrent(customerId, oneChange, update)
    .then(oneChangePeriod => dispatch(setCustomerOneChangePeriod(oneChangePeriod)))
    .catch(() => dispatch(
      showNotification({
        message: 'There was an error replacing the One Change.',
        duration: 7000,
      })));
}

export function loadCustomerCurrentOneChangePeriod(customerId) {
  return dispatch => apiService.oneChangePeriods.currentForCustomer(customerId)
    .then(oneChangePeriod => dispatch(setCustomerOneChangePeriod(oneChangePeriod)))
    .catch(() => dispatch(
      showNotification({
        message: 'There was an error loading the One Change.',
        duration: 7000,
      })));
}

const getOneChangePeriodEntities = createSelector(getEntities, entities => entities.get('oneChangePeriods'));

export const getOneChangePeriods = createSelector(getData, data => data.oneChangePeriods);

function getCustomerId(state, props) {
  return props.customerId;
}

export const getCustomerOneChangePeriod = createSelector(
  getOneChangePeriods,
  getOneChangePeriodEntities,
  getCustomerId,
  (oneChangePeriods, oneChangePeriodEntities, customerId) => {
    const oneChangePeriodId = oneChangePeriods.getIn(['customerToCurrentOneChange', customerId]);
    return oneChangePeriodEntities.get(oneChangePeriodId);
  });
