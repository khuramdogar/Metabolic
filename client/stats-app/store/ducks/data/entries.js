import { normalize } from 'normalizr';
import { createSelector } from 'reselect';
import apiService from '../../../services/api';
import { showNotification } from '../../../../common/store/actions/ui/notification';
import { entrySchema } from '../../../../common/schemas';
import { getEntities } from './entities';

const SET_ENTRY = 'entries/SET';

export function setEntry(normalizedEntry) {
  return {
    type: SET_ENTRY,
    payload: normalizedEntry,
  };
}

export function getEntryFromApi(entryId) {
  return dispatch => (
    apiService.entries.getEntry(entryId)
      .then((entry) => {
        dispatch(setEntry(normalize(entry, entrySchema)));
      })
      .catch(() => {
        dispatch(showNotification({
          message: 'There was an error while getting this entry.',
          duration: 7000,
        }));
      })
  );
}

const getEntriesEntities = createSelector(getEntities, entities => entities.get('entries'));

function getEntryId(state, props) {
  return props.entryId;
}

export const getEntry = createSelector(
  getEntryId,
  getEntriesEntities,
  (entryId, entriesEntities) => entriesEntities.get(entryId),
);
