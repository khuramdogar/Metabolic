import { createSelector } from 'reselect';
import ACTION_TYPES from '../../../common/store/actions/types';
import {
  setCurrentUser,
  unsetCurrentUser,
} from '../../../common/store/actions/current-user';
import apiService from '../../services/api';
import storageService from '../../../common/services/storage';

const CURRENT_USER_ACTION_TYPES = ACTION_TYPES.CURRENT_USER;

const INITIAL_STATE = null;

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CURRENT_USER_ACTION_TYPES.SET_CURRENT_USER:
      return action.payload;
    case CURRENT_USER_ACTION_TYPES.UNSET_CURRENT_USER:
      return INITIAL_STATE;
    default:
      return state;
  }
}

export function assignCurrentUser() {
  return dispatch =>
    apiService.agents.me()
      .then(currentUser => dispatch(setCurrentUser(currentUser)))
      .catch(() => {
        dispatch(unsetCurrentUser());
        storageService.set('redux-store-agents', { session: null });
      });
}

export function unassignCurrentUser() {
  return unsetCurrentUser();
}

function getCurrentUser(state) {
  return state.currentUser;
}

export const getCurrentUserTimezone = createSelector(
  getCurrentUser,
  currentUser => currentUser.timezone,
);
