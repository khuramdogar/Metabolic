import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import data from './data';
import ui from './ui';
import currentUser from './current-user';
import session, { SIGN_OUT } from './session';

const appReducer = combineReducers({
  data,
  ui,
  currentUser,
  session,
  form: formReducer,
  routing: routerReducer,
});

export default function rootReducer(state, action) {
  if (action.type === SIGN_OUT.SUCCESS || action.type === SIGN_OUT.FAILURE) {
    // eslint-disable-next-line no-param-reassign
    state = undefined;
  }

  return appReducer(state, action);
}
