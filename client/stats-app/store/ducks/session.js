import { Map } from 'immutable';
import pick from 'lodash/pick';
import apiService from '../../services/api';
import storageService from '../../../common/services/storage';
import {
  assignCurrentUser,
  unassignCurrentUser,
} from './current-user';
import { REHYDRATE } from './rehydration';
import { buildApiActionTypes } from '../helpers';
import routeHelper from '../../routes-helpers';

// action types
const SIGN_IN = buildApiActionTypes('session/SIGN_IN');
export const SIGN_OUT = buildApiActionTypes('session/SIGN_OUT');

const INITIAL_STATE = new Map({});

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case REHYDRATE:
      if (action.payload && action.payload.session) {
        return new Map(pick(action.payload.session, 'accessToken', 'userId'));
      }
      return state;
    case SIGN_IN.START:
      return state.merge({
        accessToken: undefined,
        userId: undefined,
        signInError: undefined,
      });
    case SIGN_IN.SUCCESS:
      return state.merge({
        accessToken: action.payload.id,
        userId: action.payload.userId,
      });
    case SIGN_IN.FAILURE:
      return state.merge({
        signInError: action.error,
      });
    case SIGN_OUT.SUCCESS:
    case SIGN_OUT.FAILURE:
      return INITIAL_STATE;
    default:
      return state;
  }
}

export function signIn(credentials) {
  return (dispatch, getState) => {
    dispatch({ type: SIGN_IN.START });
    return apiService.agents.signIn(credentials)
      .then((payload) => {
        dispatch({ type: SIGN_IN.SUCCESS, payload });
        const { session } = getState();
        storageService.set('stats-redux-store', { session });
        dispatch(assignCurrentUser());
      })
      .catch((error) => {
        dispatch({ type: SIGN_IN.FAILURE, error });
        throw error;
      });
  };
}

export function signOut() {
  return (dispatch) => {
    dispatch({ type: SIGN_OUT.START });
    return apiService.users.signOut()
      .catch((error) => {
        if (error.statusCode === 401) {
          return undefined;
        }
        throw error;
      })
      .then((payload) => {
        dispatch({ type: SIGN_OUT.SUCCESS, payload });
        storageService.set('stats-redux-store', { session: null });
        dispatch(unassignCurrentUser());
        dispatch(routeHelper.goToHome());
      })
      .catch((error) => {
        dispatch({ type: SIGN_OUT.FAILURE, error });
        throw error;
      });
  };
}
