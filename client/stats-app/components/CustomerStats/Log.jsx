import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
} from 'material-ui/Table';
import Avatar from 'material-ui/Avatar';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Title from 'material-ui/svg-icons/editor/title';
import injectSheet from 'react-jss';
import Image from '../../../common/components/Image';
import { customerShape } from '../../../common/prop-types/agents';
import RefreshStats from '../RefreshStats';
import Entry from '../../containers/Entry';

const styles = {
  dialogRoot: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 0,
  },
  dialogContent: {
    position: 'relative',
    width: '80vw',
    transform: '',
  },
  dialogBody: {
    paddingBottom: 0,
  },
  tableRow: {
    whiteSpace: 'normal',
  },
  entries: {
    '& button': {
      display: 'inline-block',
      verticalAlign: 'middle',
    },
  },
  entryButton: {
    border: 'none',
    background: 'none',
    cursor: 'pointer',
  },
};

const THUMBS_SIZE = 90;

class Log extends Component {
  constructor(props) {
    super(props);
    this.state = { dialogOpen: false, dialogEntry: undefined };
    this.closeDialog = this.closeDialog.bind(this);
    this.renderEntry = this.renderEntry.bind(this);
  }

  openDialog(entry) {
    this.setState({ dialogOpen: true, dialogEntry: entry });
  }

  closeDialog() {
    this.setState({ dialogOpen: false, dialogEntry: undefined });
  }

  renderEntry(entry) {
    const photoUrl = entry.photosData[0] &&
      (entry.photosData[0].thumbnailLocation || entry.photosData[0].location);
    const imageIcon = photoUrl
      ? <Image src={photoUrl} size={THUMBS_SIZE} thumbnail />
      : <Avatar icon={<Title />} size={THUMBS_SIZE} />;
    return (
      <button
        key={entry.id}
        onClick={() => this.openDialog(entry)}
        className={this.props.classes.entryButton}
      >
        {imageIcon}
      </button>
    );
  }

  render() {
    const { entriesLog, reloadEntriesLog, customer, classes } = this.props;
    return (
      <div>
        <RefreshStats createdAt={entriesLog.createdAt} refresh={reloadEntriesLog} />
        <Table selectable={false}>
          <TableBody displayRowCheckbox={false} showRowHover stripedRows>
            {entriesLog.log.map(hourData => (
              <TableRow key={hourData.hour}>
                <TableHeaderColumn>{`${hourData.hour}:00`}</TableHeaderColumn>
                <TableRowColumn style={styles.tableRow}>
                  <div className={classes.entries}>
                    {hourData.entries.map(this.renderEntry)}
                  </div>
                </TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>

        {this.state.dialogEntry &&
          <Dialog
            actions={<FlatButton label="Close" primary onClick={this.closeDialog} />}
            open={this.state.dialogOpen}
            onRequestClose={this.closeDialog}
            style={styles.dialogRoot}
            bodyStyle={styles.dialogBody}
            contentStyle={styles.dialogContent}
            repositionOnUpdate={false}
            autoScrollBodyContent
          >
            <Entry entryId={this.state.dialogEntry.id} customer={customer} />
          </Dialog>
        }
      </div>
    );
  }
}

Log.propTypes = {
  reloadEntriesLog: PropTypes.func.isRequired,
  entriesLog: PropTypes.shape({
    createdAt: PropTypes.string.isRequired,
    log: PropTypes.arrayOf(
      PropTypes.shape({
        hour: PropTypes.number.isRequired,
        entries: PropTypes.arrayOf(PropTypes.shape({})),
      }),
    ),
  }).isRequired,
  customer: customerShape.isRequired,
  classes: PropTypes.shape({
    entries: PropTypes.string.isRequired,
    entryButton: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(Log);
