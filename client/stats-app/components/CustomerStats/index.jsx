import React from 'react';
import PropTypes from 'prop-types';
import { Tabs, Tab } from 'material-ui/Tabs';
import FlatButton from 'material-ui/FlatButton';
import BackIcon from 'material-ui/svg-icons/navigation/chevron-left';
import keyMirror from 'keymirror';
import Overview from '../../containers/CustomerStats/Overview';
import Log from '../../containers/CustomerStats/Log';
import Day from '../../containers/CustomerStats/Day';
import Week from '../../containers/CustomerStats/Week';
import EntriesByDay from '../../containers/CustomerStats/EntriesByDay';
import CurrentOneChangePeriod from '../../containers/CustomerStats/OneChangePeriod/Current';
import { customerShape } from '../../../common/prop-types/agents';
import { container } from '../../../common/theme';

const styles = {
  container,
  backButton: {
    marginRight: '10px',
  },
};

const sections = keyMirror({
  overview: null,
  log: null,
  day: null,
  week: null,
  entriesByDay: null,
  oneChange: null,
});

export default function CustomerStats({ customer, tab, handleTabChange, goBack }) {
  return (
    <div style={styles.container}>
      <h1>
        <FlatButton
          label="Back"
          icon={<BackIcon />}
          onClick={goBack}
          style={styles.backButton}
        />
        {customer.name}
      </h1>
      <Tabs value={tab} onChange={handleTabChange}>
        <Tab label="Overview" value={sections.overview}>
          <Overview customerId={customer.id} />
        </Tab>
        <Tab label="Log" value={sections.log}>
          <Log customerId={customer.id} />
        </Tab>
        <Tab label="Day" value={sections.day}>
          <Day customerId={customer.id} />
        </Tab>
        <Tab label="Week" value={sections.week}>
          <Week customerId={customer.id} />
        </Tab>
        <Tab label="Entries By Day" value={sections.entriesByDay}>
          <EntriesByDay customerId={customer.id} />
        </Tab>
        <Tab label="One Change" value={sections.oneChange}>
          <CurrentOneChangePeriod customerId={customer.id} />
        </Tab>
      </Tabs>
    </div>
  );
}

CustomerStats.propTypes = {
  customer: customerShape.isRequired,
  tab: PropTypes.string,
  handleTabChange: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
};

CustomerStats.defaultProps = {
  tab: sections.overview,
};
