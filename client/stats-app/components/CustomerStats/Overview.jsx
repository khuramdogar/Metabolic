import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
} from 'material-ui/Table';
import get from 'lodash/get';
import RefreshStats from '../RefreshStats';

const STATS_TO_RENDER = [
  { key: 'daysSinceSignUp', humanName: 'Number of days' },
  { key: 'fullStats.totalEntries', humanName: 'Number of entries' },
  { key: 'hoursSinceLastMeal', humanName: 'Hours since last meal' },
  { key: 'fullStats.daysWithoutEntries', humanName: 'Days without entries' },
  { key: 'fullStats.daysWithLessThanThreeEntries', humanName: '< 3 entries days' },
  { key: 'fullStats.avgEntriesPerDay', humanName: 'Avg. entries per day' },
  { key: 'fullStats.avgEntriesPerDayRaw', humanName: 'Raw avg. entries per day' },
  { key: 'lastSevenDays.avgCarbs', humanName: 'Avg. daily carbs last 7 days' },
  { key: 'lastSevenDays.deltaCarbs', humanName: 'Carbs delta with previous 7 days' },
  { key: 'fullStats.avgCarbsPerDay', humanName: 'Avg. carbs per day' },
  { key: 'fullStats.avgCarbsPerDayRaw', humanName: 'Raw avg. carbs per day' },
  { key: 'lastTip.message', humanName: 'Last tip text' },
  { key: 'lastTip.createdAt', humanName: 'Last tip datetime' },
];

function renderValue(value) {
  if (!value) {
    return '–';
  }
  if (typeof value === 'number') {
    if (Number.isInteger(value)) {
      return value;
    }
    return Math.round(value * 10) / 10;
  }
  return value;
}

export default function Overview(props) {
  return (
    <div>
      <RefreshStats createdAt={props.overview.createdAt} refresh={props.reloadCustomerStats} />
      <Table selectable={false}>
        <TableBody displayRowCheckbox={false} showRowHover stripedRows>
          {STATS_TO_RENDER.map(stat => (
            <TableRow key={stat.key}>
              <TableHeaderColumn>{stat.humanName}</TableHeaderColumn>
              <TableRowColumn>{renderValue(get(props.overview, stat.key))}</TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
}

Overview.propTypes = {
  reloadCustomerStats: PropTypes.func.isRequired,
  overview: PropTypes.shape({
    createdAt: PropTypes.string.isRequired,
    customerId: PropTypes.string.isRequired,
    daysSinceSignUp: PropTypes.number.isRequired,
    hoursSinceLastMeal: PropTypes.number,
    lastTip: PropTypes.shape({
      message: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
    }),
    fullStats: PropTypes.shape({
      avgCarbsPerDay: PropTypes.number,
      avgCarbsPerDayRaw: PropTypes.number,
      avgEntriesPerDay: PropTypes.number,
      avgEntriesPerDayRaw: PropTypes.number,
      daysWithEntries: PropTypes.number,
      daysWithLessThanThreeEntries: PropTypes.number,
      daysWithoutEntries: PropTypes.number,
      totalEntries: PropTypes.number,
    }),
    lastSevenDays: PropTypes.shape({
      avgCarbs: PropTypes.number,
    }),
    pastSevenDays: PropTypes.shape({
      avgCarbs: PropTypes.number,
    }),
  }).isRequired,
};
