import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import injectSheet from 'react-jss';
import SmallPaper from '../../common/components/SmallPaper';
import routeHelper from '../routes-helpers';

const styles = {
  dashboardsList: {
    textAlign: 'left',
  },
};

function Dashboard(props) {
  return (
    <div>
      <SmallPaper>
        <h1>Dashboards</h1>
        <ul className={props.classes.dashboardsList}>
          <li><Link to={routeHelper.getCustomersDashboardPath()}>Customers</Link></li>
        </ul>
      </SmallPaper>
    </div>
  );
}

Dashboard.propTypes = {
  classes: PropTypes.shape({
    dashboardsList: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(Dashboard);
