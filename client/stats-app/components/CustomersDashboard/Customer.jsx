import React from 'react';
import { Link } from 'react-router';
import routeHelper from '../../routes-helpers';
import { customerSummaryShape } from '../../../common/prop-types/agents';
import UserAvatar from '../../../common/components/UserAvatar';

const styles = {
  container: {
    alignItems: 'center',
    display: 'flex',
  },
  avatar: {
    marginRight: '10px',
  },
  name: {
    marginTop: '15px',
    marginBottom: '2.5px',
    fontSize: '1.25em',
  },
  email: {
    marginTop: '2.5px',
    marginBottom: '15px',
    color: 'rgba(0, 0, 0, 0.54)',
  },
};

function Customer({ customer }) {
  return (
    <Link style={styles.container} to={routeHelper.getCustomerPath(customer)}>
      <UserAvatar avatarData={customer.avatarData} style={styles.avatar} />
      <div>
        <p style={styles.name}>{customer.name}</p>
        <p style={styles.email}>{customer.email}</p>
      </div>
    </Link>
  );
}

Customer.propTypes = {
  customer: customerSummaryShape.isRequired,
};

export default Customer;
