import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {
  Table,
  TableBody,
  TableHeader,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
} from 'material-ui/Table';
import find from 'lodash/find';
import flatMap from 'lodash/flatMap';
import minBy from 'lodash/minBy';
import maxBy from 'lodash/maxBy';
import round from 'lodash/round';
import get from 'lodash/get';
import pick from 'lodash/pick';
import LinesEllipsis from 'react-lines-ellipsis';
import theme, { textCentered } from '../../../common/theme';
import Customer from './Customer';
import { INSIGHTS } from '../../../../common/lib/insights';
import INSIGHTS_HUMAN_NAMES from '../../../common/lib/insights';
import DailySummary from './Summaries/DailySummary';
import NotesDialog from '../../containers/CustomersDashboard/notes-dialog';
import { customerSummaryShape } from '../../../common/prop-types/agents';

const MAX_NOTE_HEIGHT = '80px';
const CELL_WIDTH = '100px';

const styles = {
  good: {
    background: theme.palette.goodColor,
    cursor: 'pointer',
    ...textCentered,
  },
  ok: {
    background: theme.palette.okColor,
    cursor: 'pointer',
    ...textCentered,
  },
  bad: {
    background: theme.palette.badColor,
    color: 'white',
    cursor: 'pointer',
    ...textCentered,
  },
  tableColumn: {
    width: CELL_WIDTH,
    border: '1px solid #EEEEEE',
    ...textCentered,
  },
  insightsTable: {
    flex: '0 0 200px',
  },
  insightTitle: {
    width: '150px',
    border: '1px solid transparent',
  },
  tableBody: {
    overflow: 'visible',
  },
  customerSummaryContainer: {
    marginTop: '10px',
  },
  tableContainer: {
    display: 'flex',
    alignItems: 'start',
  },
  notesCell: {
    textAlign: 'left',
    height: MAX_NOTE_HEIGHT,
    maxHeight: MAX_NOTE_HEIGHT,
    whiteSpace: 'normal',
    width: CELL_WIDTH,
  },
  noteTitle: {
    height: MAX_NOTE_HEIGHT,
  },
};

function renderInsightValue(key, value, status) {
  return (
    <TableRowColumn
      key={key}
      style={{ ...styles[status], ...styles.tableColumn }}
    >
      {value}
    </TableRowColumn>
  );
}

function renderInsight(customerSummary, insightKey, date) {
  const summary = customerSummary.dailySummaries[date];
  const emptyRowColumn = <TableRowColumn key={date} style={styles.tableColumn} />;
  if (!summary || !summary.insightsData) {
    return emptyRowColumn;
  }
  const insight = find(summary.insightsData, { key: insightKey });
  if (!insight) {
    return emptyRowColumn;
  }
  return renderInsightValue(
    date,
    round(insight.metadata.problemIndex, 2),
    insight.metadata.status,
  );
}

function renderCustomerInsights(customerSummary, dateRange) {
  return INSIGHTS.map(insightKey =>
    <TableRow key={insightKey}>
      {dateRange.map(date => renderInsight(customerSummary, insightKey, date))}
    </TableRow>,
  );
}

function renderInsightsTable() {
  return (
    <div style={styles.insightsTable}>
      <Table selectable={false}>
        <TableHeader displaySelectAll={false}>
          <TableRow>
            <TableHeaderColumn />
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          <TableRow>
            <TableRowColumn style={{ ...styles.insightTitle, ...styles.noteTitle }}>
              Notes
            </TableRowColumn>
          </TableRow>
          {
            INSIGHTS.map(insightKey =>
              <TableRow key={insightKey}>
                <TableRowColumn style={styles.insightTitle}>
                  {INSIGHTS_HUMAN_NAMES[insightKey]}
                </TableRowColumn>
              </TableRow>,
            )
          }
        </TableBody>
      </Table>
    </div>
  );
}

function renderNote(customerSummary, date, editNotesFor) {
  const summary = customerSummary.dailySummaries[date];
  if (summary) {
    return (
      <TableRowColumn key={date} style={{ ...styles.tableColumn, ...styles.notesCell }}>
        <LinesEllipsis maxLine={3} text={summary.notes} />
        <FlatButton
          primary
          onClick={() => editNotesFor(summary, customerSummary.customer)}
        >
          Edit
        </FlatButton>
      </TableRowColumn>
    );
  }
  // Should not be the case, but if summary doesn't exist we should not be able to add notes
  return <TableRowColumn key={date} style={styles.tableColumn} />;
}

function renderNotes(customerSummary, dateRange, editNotesFor) {
  return (
    <TableRow>
      {dateRange.map(date => renderNote(customerSummary, date, editNotesFor))}
    </TableRow>
  );
}

function renderCustomer(customerSummary, dateRange, editNotesFor, handleCellClick) {
  return (
    <div key={customerSummary.customer.id} style={styles.customerSummaryContainer}>
      <Customer customer={customerSummary.customer} />
      <div style={styles.tableContainer}>
        {renderInsightsTable()}
        <Table
          key={customerSummary.customer.id}
          selectable={false}
          bodyStyle={styles.tableBody}
          onCellClick={(row, column) =>
            handleCellClick(customerSummary, dateRange[column], INSIGHTS[row - 1])}
        >
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              {
                dateRange.map(date =>
                  <TableHeaderColumn
                    key={date}
                    style={styles.tableColumn}
                  >
                    {date}
                  </TableHeaderColumn>,
                )
              }
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {renderNotes(customerSummary, dateRange, editNotesFor)}
            {renderCustomerInsights(customerSummary, dateRange)}
          </TableBody>
        </Table>
      </div>
    </div>
  );
}

function generateDateRange(customersSummaries) {
  const customersSummariesDates = flatMap(customersSummaries,
    customer => Object.keys(customer.dailySummaries));
  if (!customersSummariesDates.length) {
    return [];
  }

  const startMoment = moment(minBy(customersSummariesDates));
  const endMoment = moment(maxBy(customersSummariesDates));
  const dates = [];
  for (const currentDate = endMoment; currentDate >= startMoment; currentDate.subtract(1, 'day')) {
    dates.push(currentDate.format('YYYY-MM-DD'));
  }
  return dates;
}

class CustomersDailySummary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dailyCustomerSummary: null,
      customer: null,
      lastClickedInsight: null,
      lastClickedSummary: null,
      openDialog: false,
    };

    this.editNotesFor = this.editNotesFor.bind(this);
    this.handleCellClick = this.handleCellClick.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.onEditNotesClose = this.onEditNotesClose.bind(this);
  }

  onEditNotesClose() {
    this.setState({
      dailyCustomerSummary: null,
      customer: null,
    });
  }

  editNotesFor(dailyCustomerSummary, customer) {
    this.setState({
      dailyCustomerSummary,
      customer,
    });
  }

  handleCellClick(customerSummary, date, insightKey) {
    const dailySummary = customerSummary.dailySummaries[date];
    const insight = dailySummary &&
      find(dailySummary.insightsData, ['key', insightKey]);
    if (insight) {
      this.setState({
        lastClickedInsight: insight,
        lastClickedSummary: pick(dailySummary, ['day']),
        openDialog: true,
      });
    }
  }

  handleCloseDialog() {
    this.setState({
      openDialog: false,
      lastClickedInsight: undefined,
      lastClickedSummary: undefined,
    });
  }

  render() {
    const { customersDailySummaries, updateNotes } = this.props;
    const { dailyCustomerSummary, customer } = this.state;
    const dateRange = generateDateRange(customersDailySummaries);
    return (
      <div>
        {customersDailySummaries.map(customerSummary => renderCustomer(
          customerSummary, dateRange, this.editNotesFor, this.handleCellClick),
        )}
        <Dialog
          title={'Insight information'}
          modal={false}
          open={this.state.openDialog}
          onRequestClose={this.handleCloseDialog}
          actions={<FlatButton label="Close" primary onClick={this.handleCloseDialog} />}
          repositionOnUpdate
          autoScrollBodyContent
        >
          {this.state.lastClickedInsight &&
          <DailySummary
            insight={this.state.lastClickedInsight}
            summary={this.state.lastClickedSummary}
          />}
        </Dialog>
        {dailyCustomerSummary &&
          <NotesDialog
            open
            noteableId={get(dailyCustomerSummary, 'id', '')}
            updateNotes={updateNotes}
            title={`Update notes of ${get(customer, 'name', '')} for ${get(dailyCustomerSummary, 'day', '')}`}
            initialNotes={get(dailyCustomerSummary, 'notes', '')}
            onClose={this.onEditNotesClose}
          />
        }
      </div>
    );
  }
}

CustomersDailySummary.propTypes = {
  customersDailySummaries: PropTypes.arrayOf(PropTypes.shape({
    customer: customerSummaryShape.isRequired,
    customersDailySummaries: PropTypes.shape({}),
  })).isRequired,
  updateNotes: PropTypes.func.isRequired,
};

export default CustomersDailySummary;
