import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Insights from './Insights';

export default function WeeklySummary({ summary, insight }) {
  return (
    <div>
      <Table selectable={false}>
        <TableBody displayRowCheckbox={false}>
          <TableRow>
            <TableRowColumn>From date</TableRowColumn>
            <TableRowColumn>{summary.startedAt.split('T')[0]}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>To date</TableRowColumn>
            <TableRowColumn>{summary.finishedAt.split('T')[0]}</TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
      <Insights insight={insight} />
    </div>
  );
}

WeeklySummary.propTypes = {
  summary: PropTypes.shape({}).isRequired,
  insight: PropTypes.shape({}).isRequired,
};
