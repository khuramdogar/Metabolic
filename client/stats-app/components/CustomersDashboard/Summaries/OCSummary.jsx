import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import INSIGHTS_HUMAN_NAMES from '../../../../common/lib/insights';
import Insights from './Insights';

export default function OCSummary({ summary, insight }) {
  return (
    <div>
      <Table selectable={false}>
        <TableBody displayRowCheckbox={false}>
          <TableRow>
            <TableRowColumn>From date:</TableRowColumn>
            <TableRowColumn>{summary.startedAt.split('T')[0]}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>To date:</TableRowColumn>
            <TableRowColumn>{summary.finishedAt.split('T')[0]}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>One Change:</TableRowColumn>
            <TableRowColumn>{INSIGHTS_HUMAN_NAMES[summary.oneChange]}</TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
      <Insights insight={insight} />
    </div>
  );
}

OCSummary.propTypes = {
  summary: PropTypes.shape({}).isRequired,
  insight: PropTypes.shape({}).isRequired,
};
