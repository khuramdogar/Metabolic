import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import injectSheet from 'react-jss';
import FlatButton from 'material-ui/FlatButton';
import BackIcon from 'material-ui/svg-icons/navigation/chevron-left';
import CustomerList from '../CustomerList';
import { customerSummaryShape } from '../../../../common/prop-types/common';
import { container } from '../../../../common/theme';

const styles = {
  container,
};

function Analytics({ customers, goToCustomer, goBack, classes }) {
  return (
    <div className={classes.container}>
      <h1>
        <FlatButton
          label="Back"
          icon={<BackIcon />}
          onClick={goBack}
        />
        Analytics
      </h1>
      <CustomerList customers={customers} goToCustomer={goToCustomer} />
    </div>
  );
}

Analytics.propTypes = {
  customers: ImmutablePropTypes.listOf(customerSummaryShape).isRequired,
  goToCustomer: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(Analytics);
