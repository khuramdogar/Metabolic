import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { List } from 'material-ui/List';
import TextField from 'material-ui/TextField';
import memoize from 'lodash/memoize';
import { customerSummaryShape } from '../../../../common/prop-types/common';
import CustomerListItem from './Item';

function filterUsers(textFilter, customers) {
  const filterToLowerCase = textFilter.toLowerCase();
  const matches = text => text.toLowerCase().indexOf(filterToLowerCase) >= 0;

  return textFilter
    ? customers.filter(({ name, email }) => matches(name) || matches(email))
    : customers;
}

class CustomerList extends Component {
  constructor(props) {
    super(props);

    this.state = { textFilter: '' };

    this.onSearch = this.onSearch.bind(this);
    this.filterUsers = memoize(filterUsers);
  }

  onSearch(event, textFilter) {
    this.setState({ textFilter });
  }

  renderListItems() {
    return this.filterUsers(this.state.textFilter, this.props.customers)
      .map(customer =>
        <CustomerListItem
          key={customer.id}
          customer={customer}
          onClick={() => this.props.goToCustomer({ id: customer.id })}
        />,
      );
  }

  render() {
    return (
      <div>
        <TextField hintText="Search Customers" onChange={this.onSearch} />

        <List>
          {this.renderListItems()}
        </List>
      </div>
    );
  }
}

CustomerList.propTypes = {
  customers: ImmutablePropTypes.listOf(customerSummaryShape).isRequired,
  goToCustomer: PropTypes.func.isRequired,
};

export default CustomerList;
