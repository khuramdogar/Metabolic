import React from 'react';
import PropTypes from 'prop-types';
import { ListItem } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import Account from 'material-ui/svg-icons/action/account-circle';
import Forward from 'material-ui/svg-icons/navigation/chevron-right';
import Image from '../../../../common/components/Image';
import { customerSummaryShape } from '../../../../common/prop-types/common';

const renderAvatar = (avatarData) => {
  if (avatarData) {
    return (
      <Image
        src={avatarData.thumbnailLocation || avatarData.location}
        alt="Customer Image"
        loadingIcon={Account}
        thumbnail
      />
    );
  }

  return <Avatar icon={<Account />} />;
};

function CustomerListItem(props) {
  const { name, email, avatarData } = props.customer;
  const iconButton = (
    <IconButton>
      <Forward />
    </IconButton>
  );

  return (
    <div>
      <Divider />
      <ListItem
        primaryText={name}
        secondaryText={email}
        leftAvatar={renderAvatar(avatarData)}
        rightIconButton={iconButton}
        onClick={props.onClick}
      />
    </div>
  );
}

CustomerListItem.propTypes = {
  customer: customerSummaryShape.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default CustomerListItem;
