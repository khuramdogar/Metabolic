import URI from 'urijs';
import { getAccessToken } from '../../common/store/selectors/session';

import {
  buildUrl,
  jsonFetch,
} from '../../common/services/api-utils';

let currentAccessToken;

export default function ApiService(state) {
  currentAccessToken = getAccessToken(state);
}

async function authenticatedJsonFetch(url, options) {
  const finalOptions = options || {};
  finalOptions.headers = finalOptions.headers || {};
  finalOptions.headers.Authorization =
    finalOptions.headers.Authorization || currentAccessToken;
  return jsonFetch(url, finalOptions);
}

Object.assign(ApiService, {
  agents: {
    checkAccessToken: accessToken => jsonFetch(buildUrl('agents/me'), {
      headers: {
        Authorization: accessToken,
      },
    }),
    me: () => authenticatedJsonFetch(buildUrl('agents/me')),
    signIn: credentials => jsonFetch(buildUrl('agents/login'), {
      method: 'POST',
      body: JSON.stringify(credentials),
    }),
  },
  users: {
    signOut: () => authenticatedJsonFetch(buildUrl('users/logout'), {
      method: 'POST',
    }),
    getById: userId => authenticatedJsonFetch(buildUrl(`users/${userId}`)),
    list: () => authenticatedJsonFetch(buildUrl('users')),
    search: (query = '', limit = 20, skip = 0) => authenticatedJsonFetch(buildUrl(`users?filter=${JSON.stringify({ where: { name: { regexp: `/${query}/i` } }, skip, limit })}`)),
  },
  customerStats: {
    overview: customerId =>
      authenticatedJsonFetch(buildUrl(`customerStats/${customerId}/overview`)),
    log: customerId =>
      authenticatedJsonFetch(buildUrl(`customerStats/${customerId}/log`)),
    day: customerId =>
      authenticatedJsonFetch(buildUrl(`customerStats/${customerId}/day`)),
    week: customerId =>
      authenticatedJsonFetch(buildUrl(`customerStats/${customerId}/week`)),
    entriesByDay: customerId =>
      authenticatedJsonFetch(buildUrl(`customerStats/${customerId}/entries-by-day`)),
    generalOverview: day =>
      authenticatedJsonFetch(buildUrl(`customerStats/overview/${day}`)),
  },
  entries: {
    getEntry: id => authenticatedJsonFetch(buildUrl(`entries/${id}`)),
  },
  summaries: {
    oneChangePeriodSummaries: () =>
      authenticatedJsonFetch(buildUrl('oneChangePeriodSummaries')),
    getWeeklySummaries: (fromDay, toDay) =>
      authenticatedJsonFetch(buildUrl(URI('/weekly-customer-summaries').query({ fromDay: fromDay || '', toDay: toDay || '' }).toString())),
    dailySummaries: (startDate, endDate) =>
      authenticatedJsonFetch(buildUrl(URI('/daily-customer-summaries').query({ startDate: startDate || '', endDate: endDate || '' }).toString())),
    updateOneChangePeriodSummaryNotes: (id, notes) =>
      authenticatedJsonFetch(buildUrl(`oneChangePeriodSummaries/${id}/notes`), {
        method: 'PUT',
        body: JSON.stringify({ notes }),
      }),
    updateDailyCustomerSummaryNotes: (id, notes) =>
      authenticatedJsonFetch(buildUrl(`daily-customer-summaries/${id}/notes`), {
        method: 'PUT',
        body: JSON.stringify({ notes }),
      }),
    updateWeeklyCustomerSummaryNotes: (id, notes) =>
      authenticatedJsonFetch(buildUrl(`weekly-customer-summaries/${id}/notes`), {
        method: 'PUT',
        body: JSON.stringify({ notes }),
      }),
  },
  oneChangePeriods: {
    currentForCustomer: customerId =>
      authenticatedJsonFetch(buildUrl(`one-change-periods/${customerId}/one-change`)),
    replaceCurrent: (customerId, oneChange, updateCurrent = false) =>
      authenticatedJsonFetch(buildUrl(`one-change-periods/${customerId}/one-change`), {
        method: 'PUT',
        body: JSON.stringify({ customerId, oneChange, updateCurrent }),
      }),
  },
});
