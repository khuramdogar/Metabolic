import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSignedIn } from '../../common/store/selectors/session';
import routeHelper from '../routes-helpers';

class Home extends Component {
  componentWillMount() {
    if (this.props.signedIn) {
      this.props.goToDashboard();
    } else {
      this.props.goToSignIn();
    }
  }

  render() {
    return null;
  }
}

Home.propTypes = {
  signedIn: PropTypes.bool.isRequired,
  goToDashboard: PropTypes.func.isRequired,
  goToSignIn: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  signedIn: getSignedIn(state),
});

const mapDispatchToProps = {
  goToSignIn: routeHelper.goToSignIn,
  goToDashboard: routeHelper.goToDashboard,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
