import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../../common/components/Loading';
import EntriesByDay from '../../components/CustomerStats/EntriesByDay';
import {
  getCustomer,
  getCustomerEntriesByDay,
  loadCustomerEntriesByDay,
} from '../../store/ducks/data/customers';

class EntriesByDayContainer extends Component {
  constructor(props) {
    super(props);
    this.reloadEntriesByDay = this.reloadEntriesByDay.bind(this);
  }

  componentWillMount() {
    this.reloadEntriesByDay();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.customerId !== nextProps.customerId && !nextProps.entriesByDay) {
      this.props.loadCustomerEntriesByDay(nextProps.customerId);
    }
  }

  reloadEntriesByDay() {
    return this.props.loadCustomerEntriesByDay(this.props.customerId);
  }

  render() {
    return (
      this.props.entriesByDay
        ? <EntriesByDay {...this.props} reloadEntriesByDay={this.reloadEntriesByDay} />
        : <Loading />
    );
  }
}

EntriesByDayContainer.propTypes = {
  customerId: PropTypes.string.isRequired,
  loadCustomerEntriesByDay: PropTypes.func.isRequired,
  entriesByDay: PropTypes.shape({}),
};

EntriesByDayContainer.defaultProps = {
  entriesByDay: undefined,
};

function mapStateToProps(state, ownProps) {
  return {
    entriesByDay: getCustomerEntriesByDay(state, ownProps),
    customer: getCustomer(state, ownProps),
  };
}

const mapDispatchToProps = {
  loadCustomerEntriesByDay,
};

export default connect(mapStateToProps, mapDispatchToProps)(EntriesByDayContainer);
