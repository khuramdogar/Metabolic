import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../../common/components/Loading';
import Overview from '../../components/CustomerStats/Overview';
import { getCustomerStats, loadCustomerStats } from '../../store/ducks/data/customers';

class OverviewContainer extends Component {
  constructor(props) {
    super(props);
    this.reloadCustomerStats = this.reloadCustomerStats.bind(this);
  }

  componentWillMount() {
    this.reloadCustomerStats();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.customerId !== nextProps.customerId && !nextProps.overview) {
      this.props.loadCustomerStats(nextProps.customerId);
    }
  }

  reloadCustomerStats() {
    return this.props.loadCustomerStats(this.props.customerId);
  }

  render() {
    return (
      this.props.overview
        ? <Overview {...this.props} reloadCustomerStats={this.reloadCustomerStats} />
        : <Loading />
    );
  }
}

OverviewContainer.propTypes = {
  customerId: PropTypes.string.isRequired,
  loadCustomerStats: PropTypes.func.isRequired,
  overview: PropTypes.shape({}),
};

OverviewContainer.defaultProps = {
  overview: undefined,
};

function mapStateToProps(state, ownProps) {
  return {
    overview: getCustomerStats(state, ownProps),
  };
}

const mapDispatchToProps = {
  loadCustomerStats,
};

export default connect(mapStateToProps, mapDispatchToProps)(OverviewContainer);
