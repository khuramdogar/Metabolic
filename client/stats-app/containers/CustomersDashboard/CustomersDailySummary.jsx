import { connect } from 'react-redux';
import { loadCustomersDailySummaries, getCustomersDailySummaries, updateDailyCustomerSummaryNotes } from '../../store/ducks/data/summaries';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import CustomersDailySummary from '../../components/CustomersDashboard/CustomersDailySummary';

const promiseFn = props => ({
  customersDailySummariesLoading: props.loadCustomersDailySummaries(),
});

function mapStateToProps(state) {
  return {
    customersDailySummaries: getCustomersDailySummaries(state),
  };
}

const mapDispatchToProps = {
  loadCustomersDailySummaries,
  updateNotes: updateDailyCustomerSummaryNotes,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, CustomersDailySummary),
);
