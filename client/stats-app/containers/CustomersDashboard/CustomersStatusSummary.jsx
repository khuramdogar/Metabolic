import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import CustomersStatusSummary from '../../components/CustomersDashboard/CustomersStatusSummary';
import Loading from '../../../common/components/Loading';
import routeHelper from '../../routes-helpers';
import { getCurrentUserTimezone } from '../../store/ducks/current-user';
import { loadCustomersOverview, getCustomersOverview } from '../../store/ducks/data/customers';
import { toISODateFormat } from '../../../common/lib/datetime';
import { tabValues } from '../../components/CustomersDashboard/OldCustomersDashboard';

class CustomersStatusSummaryContainer extends Component {
  constructor(props) {
    super(props);
    this.reloadOverview = this.reloadOverview.bind(this);
  }

  componentDidMount() {
    this.reloadOverview();
  }

  componentWillReceiveProps(nextProps) {
    // if there will be no overview from redux, we load it
    if (!nextProps.overview) {
      this.reloadOverview(nextProps.currentDate);
    }
  }

  reloadOverview(date) {
    this.props.loadCustomersOverview(date || this.props.currentDate);
  }

  render() {
    return (
      <div>
        {this.props.overview
          ? <CustomersStatusSummary {...this.props} reloadData={this.reloadOverview} />
          : <Loading />}
      </div>
    );
  }
}

CustomersStatusSummaryContainer.propTypes = {
  currentDate: PropTypes.string.isRequired,
  overview: PropTypes.shape({}),
  loadCustomersOverview: PropTypes.func.isRequired,
};

CustomersStatusSummaryContainer.defaultProps = {
  overview: undefined,
};

function mapStateToProps(state, ownProps) {
  const currentUserTimezone = getCurrentUserTimezone(state);
  const currentDate = ownProps.currentDate || toISODateFormat(moment.tz(currentUserTimezone));
  return {
    currentUserTimezone,
    currentDate,
    overview: getCustomersOverview(state, { currentDate }),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onDaySwitch: date =>
      dispatch(routeHelper.goToCustomersTracking({
        date: toISODateFormat(date),
        tab: tabValues.overview,
      })),
    loadCustomersOverview: date => dispatch(loadCustomersOverview(date)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomersStatusSummaryContainer);
