import React from 'react';
import { Route, IndexRoute } from 'react-router';
import PublicLayout from '../common/containers/PublicLayout';
import EnsureSignedIn from '../common/containers/EnsureSignedIn';
import AppLoader from './containers/AppLoader';
import StatsLayout from './containers/StatsLayout';
import Home from './containers/Home';
import SignIn from './containers/SignIn';
import Dashboard from './components/Dashboard';
import CustomersDashboard from './components/CustomersDashboard';
import Tracking from './containers/CustomersDashboard/Tracking';
import CustomerStats from './containers/CustomerStats';
import Analytics from './containers/CustomersDashboard/Analytics';

export default [
  <Route component={AppLoader}>
    <Route path="/" component={Home} />
    <Route component={PublicLayout}>
      <Route path="/sign-in" component={SignIn} />
    </Route>
    <Route component={EnsureSignedIn}>
      <Route component={StatsLayout}>
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/customers">
          <IndexRoute component={CustomersDashboard} />
          <Route path="tracking" component={Tracking} />
          <Route path="analytics">
            <IndexRoute component={Analytics} />
            <Route path=":customerId(/:statsSection)" component={CustomerStats} />
          </Route>
        </Route>
      </Route>
    </Route>
  </Route>,
];
