import { generate } from '../common/lib/route-helpers';
import routesJSON from './routes.json';

const routes = generate(routesJSON);

export default routes;
