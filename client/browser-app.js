const loopback = require('loopback');
const boot = require('loopback-boot');

const app = loopback();
boot(app, {});
module.exports = app;
