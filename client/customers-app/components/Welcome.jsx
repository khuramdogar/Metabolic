import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DownloadMobileApp from './DownloadMobileApp';

export default class Welcome extends Component {
  componentDidMount() {
    if (!this.props.name && this.props.handleNoName) {
      this.props.handleNoName();
    }
  }

  render() {
    return (
      <div>
        <h1>Welcome aboard, {this.props.name}!</h1>
        <p>
          Go download the Metabolic app on your phone and let’s get started.
        </p>
        <DownloadMobileApp />
        <p>
          Check your inbox for a confirmation e-mail.
        </p>
      </div>
    );
  }
}

Welcome.propTypes = {
  name: PropTypes.string,
  handleNoName: PropTypes.func,
};

Welcome.defaultProps = {
  name: undefined,
  handleNoName: undefined,
};
