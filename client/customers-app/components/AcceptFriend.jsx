import React from 'react';
import PropTypes from 'prop-types';
import DownloadMobileApp from './DownloadMobileApp';

export default function AcceptFriend(props) {
  const friendName = props.friend.name || props.friend.email;
  return (<div>
    <h1>You and {friendName} are now Friends!</h1>
    <p>You can now see each other meals and leave feedback in Metabolic Coach</p>
    <p>Download the app if you don&apos;t have it yet and start sharing!</p>
    <DownloadMobileApp />
  </div>);
}

AcceptFriend.propTypes = {
  friend: PropTypes.shape({ name: PropTypes.string, email: PropTypes.string }),
};

AcceptFriend.defaultProps = {
  friend: undefined,
};
