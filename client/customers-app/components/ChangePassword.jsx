import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  TextField,
} from 'redux-form-material-ui';
import Notification from '../../common/components/Notification';
import FormActions from '../../common/components/FormActions';
import SubmitButton from '../../common/components/SubmitButton';
import { required, repeatPassword } from '../../common/forms/validators';

export default function ResetPassword(props) {
  const { handleSubmit, submitting, error } = props;
  let errorMessage;
  if (error) {
    errorMessage = error.getHumanMessage();
  }
  return (
    <div>
      <Notification type="error" message={errorMessage} />
      <h1>Change your password</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <Field
            name="currentPassword" component={TextField}
            type="password" hintText="******" floatingLabelText="Your current password"
            disabled={submitting} validate={required}
          />
        </div>
        <div>
          <Field
            name="password" component={TextField}
            type="password" hintText="******" floatingLabelText="Your new password"
            disabled={submitting} validate={required}
          />
        </div>
        <div>
          <Field
            name="repeatPassword" component={TextField}
            type="password" hintText="******" floatingLabelText="Repeat your new password"
            disabled={submitting} validate={repeatPassword}
          />
        </div>
        <FormActions>
          <SubmitButton
            label="Change password" submitting={submitting} disabled={submitting}
          />
        </FormActions>
      </form>
    </div>
  );
}

ResetPassword.propTypes = {
  error: PropTypes.shape({ getHumanMessage: PropTypes.func.isRequired }),
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

ResetPassword.defaultProps = {
  error: undefined,
};
