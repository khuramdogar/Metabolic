import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  TextField,
} from 'redux-form-material-ui';
import Notification from '../../common/components/Notification';
import FormActions from '../../common/components/FormActions';
import SubmitButton from '../../common/components/SubmitButton';
import { required, email } from '../../common/forms/validators';

export default function VerifyEmail(props) {
  const { handleSubmit, submitting, error } = props;
  let errorMessage;
  if (error) {
    errorMessage = error.getHumanMessage();
  }
  return (
    <div>
      <Notification type="error" message={errorMessage} />
      <h1>Resend verification e-mail</h1>
      <p>If you did not receive the validation link when signing up,
      please enter your email to resend you one</p>
      <form onSubmit={handleSubmit}>
        <div>
          <Field
            name="email" component={TextField}
            hintText="your.email@example.org" floatingLabelText="Your e-mail"
            disabled={submitting} validate={[required, email]}
          />
        </div>
        <FormActions>
          <SubmitButton
            label="Send me a verification e-mail" submitting={submitting} disabled={submitting}
          />
        </FormActions>
      </form>
    </div>
  );
}

VerifyEmail.propTypes = {
  error: PropTypes.shape({ getHumanMessage: PropTypes.func.isRequired }),
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

VerifyEmail.defaultProps = {
  error: undefined,
};
