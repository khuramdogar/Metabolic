import React from 'react';
import PropTypes from 'prop-types';
import Notification from '../../common/components/Notification';

export default function ErrorHandler(props) {
  return (
    <div>
      <Notification type="error" message={props.error.getHumanMessage()} />
    </div>
  );
}

ErrorHandler.propTypes = {
  error: PropTypes.shape({ getHumanMessage: PropTypes.func.isRequired }).isRequired,
};
