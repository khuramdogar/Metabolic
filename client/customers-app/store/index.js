import { createStore, applyMiddleware, compose } from 'redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import ACTION_TYPES from './actions/types';
import storageService from '../../common/services/storage';

let store;

/* eslint-disable no-underscore-dangle, global-require */
export default function configureStore() {
  if (store) {
    return store;
  }
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const middleware = applyMiddleware(routerMiddleware(browserHistory), thunk);
  store = createStore(
    rootReducer,
    composeEnhancers(middleware),
  );
  store.dispatch({ type: ACTION_TYPES.REHYDRATE, payload: storageService.get('redux-store') });
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const nextReducer = require('./reducers').default;
      store.replaceReducer(nextReducer);
    });
  }
  return store;
}
