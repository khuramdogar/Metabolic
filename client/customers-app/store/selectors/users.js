import { createSelector } from 'reselect';

export default function usersSelector(state) {
  return state.users;
}

export const signedUpUser = createSelector(usersSelector, users => users.get('signedUpUser'));
export const signedUpUserName = createSelector(signedUpUser, user =>
  user && (user.get('name') || user.get('email')));
