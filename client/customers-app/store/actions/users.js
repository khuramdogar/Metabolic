import pick from 'lodash/pick';
import ACTION_TYPES from './types';
import { goToWelcome } from './routes';
import apiService from '../../services/api';
import { signIn } from './session';

const { API } = ACTION_TYPES;

export function startSignUp() {
  return { type: API.SIGNUP.START };
}

export function successSignUp(payload) {
  return { type: API.SIGNUP.SUCCESS, payload };
}

export function failureSignUp(error) {
  return { type: API.SIGNUP.FAILURE, error };
}

export function signUp(data) {
  return (dispatch) => {
    dispatch(startSignUp());
    return apiService.users.signUp(data)
      .then((payload) => {
        dispatch(successSignUp(payload));
        dispatch(signIn(pick(data, 'email', 'password')))
          .then(() => {
            dispatch(goToWelcome());
          });
      })
      .catch((error) => {
        dispatch(failureSignUp(error));
        throw error;
      });
  };
}

export default {
  signUp,
};
