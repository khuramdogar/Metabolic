import mapValues from 'lodash/mapValues';
import keyMirror from 'keymirror';

const API_CALL_ACTION_TYPES = keyMirror({
  START: null,
  SUCCESS: null,
  FAILURE: null,
});

function buildApiActions(apiCall) {
  return mapValues(API_CALL_ACTION_TYPES, (value, key) => `API_${apiCall}_${key}`);
}

const apiActions = {
  API: mapValues({
    SIGNIN: null,
    SIGNOUT: null,
    SIGNUP: null,
  }, (value, key) => buildApiActions(key)),
};

export default Object.assign(apiActions, keyMirror({
  REHYDRATE: null,
  SET_SIGN_IN_REDIRECTION: null,
  CLEAR_SIGN_IN_REDIRECTION: null,
  SHOW_NOTIFICATION: null,
  HIDE_NOTIFICATION: null,
}));
