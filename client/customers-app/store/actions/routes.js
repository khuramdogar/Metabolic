import { push } from 'react-router-redux';

export function goToRoot() {
  return push('/');
}

export function goToSignIn() {
  return push('/sign-in');
}

export function goToSignUp() {
  return push('/sign-up');
}

export function goToDashboard() {
  return push('/dashboard');
}

export function goToWelcome() {
  return push('/welcome');
}

export default {
  goToDashboard,
  goToRoot,
  goToSignIn,
  goToSignUp,
  goToWelcome,
};
