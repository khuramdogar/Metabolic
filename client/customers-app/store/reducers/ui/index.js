import { combineReducers } from 'redux';
import notifications from '../../../../common/store/reducers/ui/notification';
import redirections from '../../../../common/store/reducers/ui/redirections';

export default combineReducers({
  notifications,
  redirections,
});
