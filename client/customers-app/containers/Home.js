import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSignedIn } from '../../common/store/selectors/session';
import { goToSignUp, goToDashboard } from '../store/actions/routes';

class Home extends Component {
  componentWillMount() {
    if (this.props.signedIn) {
      this.props.goToDashboard();
    } else {
      this.props.goToSignUp();
    }
  }

  render() {
    return null;
  }
}

Home.propTypes = {
  signedIn: PropTypes.bool.isRequired,
  goToDashboard: PropTypes.func.isRequired,
  goToSignUp: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  signedIn: getSignedIn(state),
});

const mapDispatchToProps = {
  goToSignUp,
  goToDashboard,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
