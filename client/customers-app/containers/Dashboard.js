import { connect } from 'react-redux';
import { signOut } from '../store/actions/session';
import Dashboard from '../components/Dashboard';

const mapDispatchToProps = {
  onSignOut: signOut,
};

export default connect(null, mapDispatchToProps)(Dashboard);
