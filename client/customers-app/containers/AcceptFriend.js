import PromiseLoadingWrapper from '../../common/hoc/PromiseLoadingWrapper';
import AcceptFriend from '../components/AcceptFriend';
import apiService from '../services/api';
import ErrorHandler from '../components/ErrorHandler';

const promiseFn = props => ({
  friend: apiService.friends.acceptFriendship(props.params.inviteToken)
    .then(friendship => friendship.user),
});

export default PromiseLoadingWrapper(promiseFn, AcceptFriend, ErrorHandler);
