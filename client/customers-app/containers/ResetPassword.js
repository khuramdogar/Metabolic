import { reduxForm } from 'redux-form';
import { routes, notification } from '../store/actions';
import apiErrorsTranslator from '../../common/forms/api-errors';
import ResetPassword from '../components/ResetPassword';
import apiService from '../services/api';

export default reduxForm({
  form: 'resetPassword',
  enableReinitialize: true,
  onSubmit: (values, dispatch, props) => {
    const { accessToken } = props.params;
    return apiService.users.resetPassword(accessToken, values.password)
      .then(() => dispatch(notification.showNotification({
        message: 'Password set successfully! Now you can proceed to sign in',
        duration: 7000,
      })))
      .then(() => dispatch(routes.goToSignIn()))
      .catch((error) => {
        if (error.statusCode === 401) {
          error.tokenExpired = true; // eslint-disable-line no-param-reassign
        }
        throw error;
      })
      .catch(apiErrorsTranslator);
  },
})(ResetPassword);
