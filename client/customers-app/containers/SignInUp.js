import pick from 'lodash/pick';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import moment from 'moment-timezone';
import { users, session, routes, redirections } from '../store/actions';
import { getSignInRedirection } from '../../common/store/selectors/ui/redirections';
import apiErrorsTranslator from '../../common/forms/api-errors';
import SignInUp from '../components/SignInUp';
import StripeProvider from '../hoc/StripeProvider';

const FormComponent = reduxForm({
  form: 'signInUp',
  enableReinitialize: true,
  onSubmit: (values, dispatch, props) => {
    if (props.signIn) {
      return dispatch(session.signIn(values))
        .then(() => {
          if (props.signInRedirection) {
            const { signInRedirection } = props;
            dispatch(redirections.clearSignInRedirection());
            dispatch(push(signInRedirection));
          } else {
            dispatch(routes.goToDashboard());
          }
        })
        .catch((error) => {
          if (error.code === 'LOGIN_FAILED') {
            // eslint-disable-next-line no-param-reassign
            error.humanMessage = 'Incorrect e-mail or password';
          }
          throw error;
        })
        .catch(apiErrorsTranslator);
    }
    const signUpValues = pick(values, 'name', 'email', 'password', 'coupon');
    signUpValues.timezone = moment.tz.guess();
    return props.stripeContext.createToken()
      .then(result => result.token.id)
      .then((tokenizedCreditCard) => {
        signUpValues.tokenizedCreditCard = tokenizedCreditCard;
        return dispatch(users.signUp(signUpValues));
      })
      .catch(apiErrorsTranslator);
  },
})(SignInUp);

function mapStateToProps(state, ownProps) {
  return {
    signInRedirection: getSignInRedirection(state),
    signIn: ownProps.route.path === 'sign-in',
  };
}

export default StripeProvider(connect(mapStateToProps)(FormComponent));
