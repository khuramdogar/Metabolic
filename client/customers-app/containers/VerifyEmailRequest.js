import { reduxForm } from 'redux-form';
import { routes, notification } from '../store/actions';
import apiErrorsTranslator from '../../common/forms/api-errors';
import VerifyEmailRequest from '../components/VerifyEmailRequest';
import apiService from '../services/api';

export default reduxForm({
  form: 'VerifyEmailRequest',
  enableReinitialize: true,
  onSubmit: (values, dispatch) => apiService.users.resendVerificationEmail(values.email)
    .then(() => dispatch(notification.showNotification({
      message: 'Check your e-mail for further instructions',
      duration: 7000,
    })))
    .then(() => dispatch(routes.goToSignIn()))
    .catch(apiErrorsTranslator),
})(VerifyEmailRequest);
