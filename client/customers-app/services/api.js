import configureStore from '../store';
import { getAccessToken } from '../../common/store/selectors/session';

import {
  buildUrl,
  jsonFetch,
  authenticatedJsonFetchWrapper,
} from '../../common/services/api-utils';

const store = configureStore();

const authenticatedJsonFetch = authenticatedJsonFetchWrapper(store, getAccessToken);

export default {
  users: {
    changePassword: (oldPassword, newPassword) =>
      authenticatedJsonFetch(buildUrl('users/change-password'), {
        method: 'POST',
        body: JSON.stringify({ oldPassword, newPassword }),
      }),
    checkAccessToken: accessToken => jsonFetch(buildUrl('users/me'), {
      headers: {
        Authorization: accessToken,
      },
    }),
    me: () => authenticatedJsonFetch(buildUrl('users/me')),
    requestPasswordReset: email => jsonFetch(buildUrl('users/reset'), {
      method: 'POST',
      body: JSON.stringify({ email }),
    }),
    resetPassword: (accessToken, newPassword) => jsonFetch(buildUrl('users/reset-password'), {
      method: 'POST',
      body: JSON.stringify({ newPassword }),
      headers: {
        Authorization: accessToken,
      },
    }),
    resendVerificationEmail: email => jsonFetch(buildUrl('users/resend-verification-email'), {
      method: 'POST',
      body: JSON.stringify({ email }),
    }),
    signIn: credentials => jsonFetch(buildUrl('users/login'), {
      method: 'POST',
      body: JSON.stringify(credentials),
    }),
    signUp: data => jsonFetch(buildUrl('users'), {
      method: 'POST',
      body: JSON.stringify(data),
    }),
    signOut: () => authenticatedJsonFetch(buildUrl('users/logout'), {
      method: 'POST',
    }),
  },
  friends: {
    acceptFriendship: inviteToken => jsonFetch(buildUrl(`friendships/${inviteToken}/accept`), {
      headers: {
        Authorization: getAccessToken(store.getState()),
      },
    }),
  },
};
