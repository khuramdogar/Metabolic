import { schema } from 'normalizr';
import omit from 'lodash/omit';
import * as commonSchemas from '../../common/schemas';

export const agentSchema = new schema.Entity('agents');

export const agentSummarySchema = commonSchemas.agentSummarySchema;

export const messageSchema = new schema.Entity('messages');

export const customerSchema = commonSchemas.customerSchema;

export const customerSummarySchema = commonSchemas.customerSummarySchema;

export const entrySchema = commonSchemas.entrySchema;

export const entrySummarySchema = new schema.Entity('entrySummaries', {
  agentSummary: agentSummarySchema,
  userSummary: customerSummarySchema,
}, {
  processStrategy: (value) => {
    const newValue = omit(value, 'agent', 'user');
    newValue.agentSummary = value.agent;
    newValue.userSummary = value.user;
    return newValue;
  },
});

export const customerDaySchema = new schema.Entity('customerDays', {
  entrySummaries: [entrySummarySchema],
}, {
  processStrategy: (value) => {
    const newValue = omit(value, 'entries');
    newValue.entrySummaries = value.entries;
    return newValue;
  },
});

export const entryMessageSchema = {
  message: messageSchema,
};

export const entryMessagesSchema = {
  messages: [messageSchema],
};

export const queueSchema = {
  active: [entrySummarySchema],
  inactive: [entrySummarySchema],
  pending: [entrySummarySchema],
  waiting: [entrySummarySchema],
};

export const foodSearchResultSchema = new schema.Entity('foodSearchResults', {}, {
  idAttribute: 'externalId',
});

export const foodSearchResultsSchema = {
  foodSearchResults: [foodSearchResultSchema],
};

export const foodSchema = new schema.Entity('food');

export const foodArraySchema = {
  food: [foodSchema],
};

export const nutritionTipSchema = new schema.Entity('nutritionTips');

export const metabolicChatSchema = new schema.Entity('metabolicChats', {
  customerSummary: customerSummarySchema,
}, {
  idAttribute: value => value.user.id,
  processStrategy: (value) => {
    const newValue = omit(value, 'user');
    newValue.customerSummary = value.user;
    return newValue;
  },
});

export const metabolicChatListSchema = {
  chats: [metabolicChatSchema],
};

export const metabolicMessageSchema = new schema.Entity('metabolicMessages');

export const metabolicMessagesSchema = {
  messages: [metabolicMessageSchema],
};

export const newMetabolicMessageSchema = {
  message: metabolicMessageSchema,
};

export const customerBudgetSchema = new schema.Entity('currentCustomerBudget');

export const currentCustomerBudgetSchema = {
  currentCustomerBudget: customerBudgetSchema,
};
