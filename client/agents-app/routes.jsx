import React from 'react';
import { Route } from 'react-router';
import PublicLayout from '../common/containers/PublicLayout';
import AgentLayout from './containers/AgentLayout';
import RootRedirection from './containers/RootRedirection';
import EnsureSignedIn from '../common/containers/EnsureSignedIn';
import SignIn from './containers/SignIn';
import Home from './containers/Home';
import EntryReview from './containers/EntryReview';
import ReadOnlyEntryReview from './containers/EntryReview/ReadOnly';
import CustomerProfile from './containers/CustomerProfile';
import MetabolicChat from './containers/MetabolicChat';
import ChangePassword from './containers/ChangePassword';
import AppLoader from './containers/AppLoader';

export default [
  <Route component={AppLoader}>
    <Route path="/" component={RootRedirection} />
    <Route component={PublicLayout}>
      <Route path="/sign-in" component={SignIn} />
    </Route>
    <Route component={EnsureSignedIn}>
      <Route component={AgentLayout}>
        <Route path="/home" component={Home} />
        <Route path="entries/:id/review" component={EntryReview} />
        <Route path="entries/:id" component={ReadOnlyEntryReview} />
        <Route path="customers/:id" component={CustomerProfile} />
        <Route path="metabolic-chats/:customerId" component={MetabolicChat} />
        <Route path="change-password" component={ChangePassword} />
      </Route>
    </Route>
  </Route>,
];
