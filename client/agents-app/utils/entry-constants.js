import keyMirror from 'keymirror';

export const entryStatuses = keyMirror({
  answered: null,
  evaluated: null,
  claimed: null,
  pending: null,
  needsInfo: null,
});

export default {
  entryStatuses,
};
