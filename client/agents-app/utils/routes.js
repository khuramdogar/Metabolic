export const readOnlyEntry = id => `/entries/${id}`;

export default {
  readOnlyEntry,
};
