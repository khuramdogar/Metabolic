import keyMirror from 'keymirror';

export const goalStatuses = keyMirror({ good: null, bad: null, ok: null, unrated: null });

export default {
  goalStatuses,
};
