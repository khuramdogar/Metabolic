import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMessagesForEntry } from '../../store/selectors/data/entryMessages';
import { getMetabolicMessagesForUser } from '../../store/selectors/data/metabolic-chats';
import Chat from '../../components/Chat';
import { getUserId } from '../../../common/store/selectors/session';

/*
  The source for the messages is determined by which prop (entryId or customerId) is present.
    - entryId: messages will be retrieved from the entry
    - customerId: messages will be retrieved from that user's metabolicChat.
*/
class ChatContainer extends Component {
  constructor(props) {
    super(props);

    this.state = { lightBoxOpen: false };

    this.onOpenLightBox = this.onOpenLightBox.bind(this);
    this.onCloseLightBox = this.onCloseLightBox.bind(this);
  }

  onOpenLightBox(lightBoxImages, lightBoxStartingIndex) {
    this.setState({ lightBoxOpen: true, lightBoxImages, lightBoxStartingIndex });
  }

  onCloseLightBox() {
    this.setState({ lightBoxOpen: false });
  }

  render() {
    return (
      <Chat
        lightBoxOpen={this.state.lightBoxOpen}
        lightBoxImages={this.state.lightBoxImages}
        lightBoxStartingIndex={this.state.lightBoxStartingIndex}
        handleOpenLightBox={this.onOpenLightBox}
        handleCloseLightBox={this.onCloseLightBox}
        {...this.props}
      />
    );
  }
}

function mapStateToProps(state, ownProps) {
  const messages = ownProps.entryId
    ? getMessagesForEntry(state, ownProps)
    : getMetabolicMessagesForUser(state, ownProps);

  return {
    messages,
    currentUserId: getUserId(state),
  };
}

export default connect(mapStateToProps)(ChatContainer);
