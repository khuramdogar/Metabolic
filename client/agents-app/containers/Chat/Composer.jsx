import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import uniqBy from 'lodash/uniqBy';
import { getComposerPhotos, getComposerText } from '../../store/selectors/forms/composer';
import Composer from '../../components/Chat/Composer';

class ComposerContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropzoneVisible: false,
      cursorPosition: 0,
    };

    this.onDropzoneToggle = this.onDropzoneToggle.bind(this);
    this.onSaveCursorPosition = this.onSaveCursorPosition.bind(this);
    this.onEmojiPick = this.onEmojiPick.bind(this);
    this.onPhotoChanges = this.onPhotoChanges.bind(this);
    this.onReset = this.onReset.bind(this);
  }

  onDropzoneToggle(event) {
    event.preventDefault();
    this.setState({ dropzoneVisible: !this.state.dropzoneVisible });
  }

  onSaveCursorPosition(event) {
    this.setState({ cursorPosition: event.target.selectionEnd });
  }

  onEmojiPick(emoji) {
    const { text } = this.props;
    const newText = text
      ? `${text.slice(0, this.state.cursorPosition)}${emoji.native}${text.slice(this.state.cursorPosition)}`
      : emoji.native;

    this.props.change('text', newText);
  }

  onPhotoChanges(newPhotos, previousPhotos) {
    const allPhotos = previousPhotos ? [...previousPhotos, ...newPhotos] : newPhotos;
    this.props.change('photos', uniqBy(allPhotos, photo => photo.name));
  }

  onReset(event) {
    event.preventDefault();
    this.props.reset();
  }

  render() {
    return (
      <Composer
        handleDropzoneToggle={this.onDropzoneToggle}
        handlePhotoChanges={this.onPhotoChanges}
        handleSaveCursorPosition={this.onSaveCursorPosition}
        handleEmojiPick={this.onEmojiPick}
        handleReset={this.onReset}
        dropzoneVisible={this.state.dropzoneVisible}
        disabled={this.props.disabled}
        {...this.props}
      />
    );
  }
}

ComposerContainer.propTypes = {
  text: PropTypes.string,
  reset: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

ComposerContainer.defaultProps = {
  text: undefined,
  disabled: false,
};

const mapStateToProps = state => ({
  photos: getComposerPhotos(state),
  text: getComposerText(state),
});

export default reduxForm({
  form: 'composer',
})(connect(mapStateToProps)(ComposerContainer));
