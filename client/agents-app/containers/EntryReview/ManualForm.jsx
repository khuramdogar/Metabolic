import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import zipObject from 'lodash/zipObject';
import isEmpty from 'lodash/isEmpty';
import { getIsSubmitting } from '../../store/selectors/ui/reviews';
import getManualReviewValues, { getManualNutrients, getManualPercentages } from '../../store/selectors/forms/manual-review';
import ManualForm from '../../components/EntryReview/ManualForm';
import { pluralizeNutrientName, caloriesPerGram } from '../../../common/lib/nutrients';
import customPropTypes from '../../prop-types';

class ManualFormContainer extends Component {
  constructor(state) {
    super(state);

    this.state = {
      dirtyCalories: false,
    };

    this.calculateCalories = this.calculateCalories.bind(this);
    this.onNutrientChanged = this.onNutrientChanged.bind(this);
    this.onCaloriesChanged = this.onCaloriesChanged.bind(this);
  }

  onNutrientChanged(event, value) {
    if (this.state.dirtyCalories) {
      return;
    }

    this.calculateCalories({
      ...this.props.nutrients,
      [event.target.name.toLowerCase()]: parseInt(value, 10),
    });
  }

  onCaloriesChanged(event, value) {
    if (value === '') {
      event.preventDefault();
      this.calculateCalories(this.props.nutrients);
    }

    this.setState({ dirtyCalories: value !== '' });
  }

  calculateCalories({ proteins, fats, carbs }) {
    this.props.change('calories', (caloriesPerGram.proteins * proteins) + (caloriesPerGram.fats * fats) + (caloriesPerGram.carbs * carbs));
  }

  render() {
    return (
      <ManualForm
        handleNutrientChanged={this.onNutrientChanged}
        handleCaloriesChanged={this.onCaloriesChanged}
        {...this.props}
      />
    );
  }
}

ManualFormContainer.propTypes = {
  nutrients: customPropTypes.nutrients.isRequired,
  change: PropTypes.func.isRequired,
};

function setInitialValues(state, props) {
  const formValues = getManualReviewValues(state);
  if (!isEmpty(formValues)) {
    return formValues;
  } else if (!props.nutrientsSummary) {
    return {};
  }

  return zipObject(
    props.nutrientsSummary.map(nutrient => pluralizeNutrientName(nutrient.name)),
    props.nutrientsSummary.map(nutrient => nutrient.value.toFixed(2)),
  );
}

const mapStateToProps = (state, ownProps) => ({
  initialValues: setInitialValues(state, ownProps),
  percentages: getManualPercentages(state),
  nutrients: getManualNutrients(state),
  submitting: getIsSubmitting(state),
});

export default connect(mapStateToProps)(reduxForm({
  form: 'manualReview',
})(ManualFormContainer));
