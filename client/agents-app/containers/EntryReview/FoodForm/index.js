import { connect } from 'react-redux';
import { addFood } from '../../../store/actions/data/reviews';
import { preloadFood } from '../../../store/actions/ui/reviews';
import { getFoodIdsToLoad } from '../../../store/selectors/data/food';
import {
  getEntryAddedFoodEntities,
  getEatenFood,
  getAlreadyLoadedFood,
} from '../../../store/selectors/data/reviews';
import PromiseLoadingWrapper from '../../../../common/hoc/PromiseLoadingWrapper';
import FoodForm from '../../../components/EntryReview/FoodForm';

const promiseFn = (props) => {
  props.addFood(props.entryId, props.alreadyLoadedFood);
  return {
    preloadingFood: props.preloadFood(props.entryId, props.foodIdsToLoad),
  };
};

const mapStateToProps = (state, ownProps) => ({
  foodIdsToLoad: getFoodIdsToLoad(state, ownProps),
  alreadyLoadedFood: getAlreadyLoadedFood(state, ownProps),
  addedFood: getEntryAddedFoodEntities(state, ownProps),
  eatenFood: getEatenFood(state, ownProps),
});

const mapDispatchToProps = { preloadFood, addFood };

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, FoodForm),
);
