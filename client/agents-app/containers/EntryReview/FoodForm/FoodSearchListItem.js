import { connect } from 'react-redux';
import FoodSearchListItem from '../../../components/EntryReview/FoodForm/FoodSearchListItem';

import {
  addFoodFromSearch,
  removeFoodFromSearch,
  addNormalizedFoodAgain,
} from '../../../store/actions/data/reviews';

import {
  getIsFoodAddedInEntry,
  getIsFoodLoadingInEntry,
  getFoodEntityFromExternalData,
} from '../../../store/selectors/data/reviews';

const mapStateToProps = (state, ownProps) => ({
  entryId: ownProps.entryId,
  added: getIsFoodAddedInEntry(state, ownProps),
  loading: getIsFoodLoadingInEntry(state, ownProps),
  entity: getFoodEntityFromExternalData(state, ownProps),
});

const mapDispatchToProps = {
  handleTap: (entryId, food, added, entity) => {
    const shouldAdd = !added;

    // If the food has already been loaded from api, and an entity already exists.
    if (shouldAdd && entity) {
      return addNormalizedFoodAgain(entryId, entity);
    }

    // If the food has not been loaded from api, and a fetch is necessary.
    if (shouldAdd) {
      return addFoodFromSearch(entryId, food);
    }

    return removeFoodFromSearch(entryId, food.externalSource, food.externalId);
  },
};

export default connect(mapStateToProps, mapDispatchToProps)(FoodSearchListItem);
