import { connect } from 'react-redux';
import SimilarEntry from '../../../components/EntryReview/SimilarEntries/SimilarEntry';
import { getEntryFromApi } from '../../../store/actions/data/entries';
import { copyEntryReview } from '../../../store/actions/data/reviews';
import { getEntry } from '../../../store/selectors/data/entries';
import { getEntryAddedFoodEntities } from '../../../store/selectors/data/reviews';
import PromiseLoadingWrapper from '../../../../common/hoc/PromiseLoadingWrapper';

function promiseFn(props) {
  return {
    entryLoading: props.getEntryFromApi(props.entryId),
  };
}

function mapStateToProps(state, ownProps) {
  return {
    entry: getEntry(state, ownProps),
    addedFood: getEntryAddedFoodEntities(state, ownProps),
  };
}

const mapDispatchToProps = { getEntryFromApi, copyEntryReview };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PromiseLoadingWrapper(promiseFn, SimilarEntry));
