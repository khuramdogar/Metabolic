import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import get from 'lodash/get';
import { submitReviewWithHumanFoodNames, submitReviewWithHumanFoodNamesAndSpellCheck } from '../../store/actions/ui/reviews';
import { goToCustomer } from '../../store/actions/ui/routes';
import { addEntrySubscription, removeEntrySubscription } from '../../store/actions/data/subscriptions';
import { getEntryFromApiPrefilled } from '../../store/actions/data/reviews';
import { getEntry } from '../../store/selectors/data/entries';
import { getCustomerSummary } from '../../store/selectors/data/customers';
import { getReviewState, getReviewType, getIsSubmitting, getDisabledSubmit, getSubmitBody } from '../../store/selectors/ui/reviews';
import { getHumanNamesAsFoods } from '../../store/selectors/forms/human-names';
import { getIsSpellChecking } from '../../store/selectors/ui/spell-check';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import EntryReview from '../../components/EntryReview';
import { origins } from '../../store/reducers/ui/spell-check';
import customPropTypes from '../../prop-types';

function promiseFn(props) {
  return {
    entryLoading: get(props, 'location.state.claimed') && props.entry
      ? Promise.resolve(props.entry)
      : props.getEntryFromApi(props.params.id),
  };
}

class EntryReviewContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spellCheckModalOpen: false,
      evaluationError: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onCloseSpellCheckModal = this.onCloseSpellCheckModal.bind(this);
    this.handleAllImagesReviewedCheck = this.handleAllImagesReviewedCheck.bind(this);
  }

  componentWillMount() {
    this.props.addEntrySubscription(this.props.entry.id);
  }

  componentWillUnmount() {
    this.props.removeEntrySubscription(this.props.entry.id);
  }

  onCloseSpellCheckModal() {
    this.setState({ spellCheckModalOpen: false });
  }

  setAllImagesReviewedError(allImagesReviewed) {
    this.setState({
      evaluationError: allImagesReviewed ? '' : 'Please go through all the Images in order to submit evaluation',
    });
  }

  handleSubmit(submitDirectly) {
    const text = get(this.props.submitBody, 'message.text');
    const { entry, submitBody, foodWithHumanNames } = this.props;
    if (submitDirectly || !text) {
      this.props.submitReviewDirectly(entry.id, submitBody, foodWithHumanNames);
    } else {
      this.props.submitReviewWithSpellCheck(text, entry.id, submitBody, foodWithHumanNames)
        .then(hasErrors => this.setState({ spellCheckModalOpen: hasErrors }));
    }
  }

  handleAllImagesReviewedCheck(allImagesReviewed) {
    return this.props.disabled && this.setAllImagesReviewedError(allImagesReviewed);
  }

  render() {
    return (
      <EntryReview
        {...this.props}
        spellCheckModalOpen={this.state.spellCheckModalOpen}
        handleSubmit={this.handleSubmit}
        handleCloseSpellCheckModal={this.onCloseSpellCheckModal}
        handleAllImagesReviewedCheck={this.handleAllImagesReviewedCheck}
        evaluationError={this.state.evaluationError}
      />
    );
  }
}

EntryReviewContainer.propTypes = {
  entry: customPropTypes.entry.isRequired,
  submitBody: PropTypes.shape({
    action: PropTypes.string,
    nutritionalData: customPropTypes.nutritionalData,
    eatenFoods: customPropTypes.eatenFoods,
    message: customPropTypes.reviewMessage,
    similarEntryId: PropTypes.string,
  }),
  foodWithHumanNames: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    humanName: PropTypes.string,
  })).isRequired,
  disabled: PropTypes.bool.isRequired,
  addEntrySubscription: PropTypes.func.isRequired,
  removeEntrySubscription: PropTypes.func.isRequired,
  submitReviewDirectly: PropTypes.func.isRequired,
  submitReviewWithSpellCheck: PropTypes.func.isRequired,
};

EntryReviewContainer.defaultProps = {
  submitBody: undefined,
};

function mapStateToProps(state, ownProps) {
  const entry = getEntry(state, { entryId: ownProps.params.id });
  const props = { entryId: ownProps.params.id, customerId: get(entry, 'userId'), origin: origins.composer };

  return {
    entry,
    customer: entry && getCustomerSummary(state, props),
    currentReviewState: getReviewState(state),
    currentReviewType: getReviewType(state),
    submitBody: getSubmitBody(state, props),
    foodWithHumanNames: getHumanNamesAsFoods(state),
    loading: getIsSubmitting(state) || getIsSpellChecking(state, props),
    disabled: entry && getDisabledSubmit(state, props),
  };
}

const mapDispatchToProps = {
  getEntryFromApi: getEntryFromApiPrefilled,
  addEntrySubscription,
  removeEntrySubscription,
  goToCustomer,
  goBack,
  submitReviewDirectly: submitReviewWithHumanFoodNames,
  submitReviewWithSpellCheck: submitReviewWithHumanFoodNamesAndSpellCheck,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, EntryReviewContainer),
);
