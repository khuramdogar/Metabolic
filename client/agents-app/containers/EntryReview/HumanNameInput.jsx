import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import injectSheet from 'react-jss';
import customPropTypes from '../../prop-types';

const styles = {
  inputWrapper: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginTop: '20px',
  },
  foodName: {
    textAlign: 'right',
    marginRight: '25px',
    width: '200px',
  },
};

const HumanNameInput = ({
  input: { value, onChange },
  meta: { error },
  foodName,
  classes,
  disabled,
}) => (
  <div className={classes.inputWrapper}>
    <span className={classes.foodName}>{foodName}</span>
    <TextField
      floatingLabelText="Display Name"
      hintText="e.g. Red Apple"
      errorText={error}
      value={value}
      onChange={(event, humanName) => onChange(humanName)}
      disabled={disabled}
    />
  </div>
);

HumanNameInput.propTypes = {
  input: PropTypes.shape({
    value: customPropTypes.eatenFood,
  }),
  disabled: PropTypes.bool.isRequired,
  meta: PropTypes.shape({ error: PropTypes.string }),
  foodName: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    inputWrapper: PropTypes.string.isRequired,
    foodName: PropTypes.string.isRequired,
  }).isRequired,
};

HumanNameInput.defaultProps = {
  input: { value: undefined },
  meta: { error: undefined },
  disabled: false,
};

export default injectSheet(styles)(HumanNameInput);
