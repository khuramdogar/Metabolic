import { connect } from 'react-redux';
import EntryReviewToolbar from '../components/EntryReviewToolbar';
import { setReviewState, setReviewType } from '../store/actions/ui/reviews';

const mapDispatchToProps = { setReviewState, setReviewType };

export default connect(null, mapDispatchToProps)(EntryReviewToolbar);
