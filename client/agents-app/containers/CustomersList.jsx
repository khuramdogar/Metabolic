import { connect } from 'react-redux';
import CustomersList from '../components/CustomersList';
import PromiseLoadingWrapper from '../../common/hoc/PromiseLoadingWrapper';
import { loadCustomers, searchUsers } from '../store/actions/data/customers';
import { goToCustomer } from '../store/actions/ui/routes';
import {
  getCustomersList,
  getOtherCustomersList,
  getIsSearchingUsers,
} from '../store/selectors/data/customers';


const promiseFn = props => ({
  loadedCustomers: props.loadCustomers(),
});

const mapStateToProps = state => ({
  engagedList: getCustomersList(state),
  othersList: getOtherCustomersList(state),
  searchingUsers: getIsSearchingUsers(state),
});

const mapDispatchToProps = {
  loadCustomers,
  searchUsers,
  onClick: goToCustomer,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PromiseLoadingWrapper(promiseFn, CustomersList));
