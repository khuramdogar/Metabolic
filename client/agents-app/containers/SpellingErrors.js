import { connect } from 'react-redux';
import { getSpellingErrors } from '../store/selectors/ui/spell-check';
import SpellingErrors from '../components/SpellingErrors';

const mapStateToProps = (state, ownProps) => ({
  spellingErrors: getSpellingErrors(state, ownProps),
});

export default connect(mapStateToProps)(SpellingErrors);
