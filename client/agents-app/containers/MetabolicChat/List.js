import { connect } from 'react-redux';
import { goToMetabolicChat } from '../../store/actions/ui/routes';
import {
  getOrderedMetabolicChatsEntities,
  getMetabolicChatListInitialized,
} from '../../store/selectors/data/metabolic-chats';
import MetabolicChatList from '../../components/MetabolicChat/List';
import LoadingWrapper from '../../../common/hoc/LoadingWrapper';

function mapStateToProps(state) {
  return {
    loading: !getMetabolicChatListInitialized(state),
    metabolicChats: getOrderedMetabolicChatsEntities(state),
  };
}

const mapDispatchToProps = {
  goToMetabolicChat,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoadingWrapper(MetabolicChatList));
