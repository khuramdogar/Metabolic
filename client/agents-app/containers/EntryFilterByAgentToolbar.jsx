import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import EntryFilterByAgentToolbar from '../components/EntryFilterByAgentToolbar';
import { getAgentsList, getAreAgentsLoaded } from '../store/selectors/data/agents';
import { getAgentIdFilter } from '../store/selectors/ui/home';
import { loadAgents } from '../store/actions/data/agents';
import { changeAgentFilter } from '../store/actions/ui/home';


class EntryFilterByAgentToolbarContainer extends Component {
  componentWillMount() {
    if (!this.props.areAgentsLoaded) {
      this.props.loadAgents();
    }
  }

  render() {
    return <EntryFilterByAgentToolbar {...this.props} />;
  }
}

const mapStateToProps = state => ({
  agentsList: getAgentsList(state),
  selectedAgentId: getAgentIdFilter(state),
  areAgentsLoaded: getAreAgentsLoaded(state),
});

const mapDispatchToProps = {
  changeAgentFilter,
  loadAgents,
};

EntryFilterByAgentToolbarContainer.propTypes = {
  loadAgents: PropTypes.func.isRequired,
  areAgentsLoaded: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EntryFilterByAgentToolbarContainer);
