import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import get from 'lodash/get';
import Home, { tabValues } from '../components/Home';
import { goToHomeTab } from '../store/actions/ui/routes';
import {
  subscribeToQueues,
  unsubscribeFromQueues,
  subscribeToMetabolicChatList,
  unsubscribeFromMetabolicChatList,
} from '../store/actions/data/subscriptions';
import { getCurrentUserSuperAgentModeCapability } from '../store/selectors/current-user';

class HomeContainer extends Component {
  componentWillMount() {
    this.props.subscribeToQueues();

    if (this.props.currentUserSuperAgentModeCapability) {
      this.props.subscribeToMetabolicChatList();
    }
  }

  componentWillUnmount() {
    this.props.unsubscribeFromQueues();

    if (this.props.currentUserSuperAgentModeCapability) {
      this.props.unsubscribeFromMetabolicChatList();
    }
  }

  render() {
    return <Home {...this.props} />;
  }
}

HomeContainer.propTypes = {
  currentUserSuperAgentModeCapability: PropTypes.bool.isRequired,
  subscribeToQueues: PropTypes.func.isRequired,
  unsubscribeFromQueues: PropTypes.func.isRequired,
  subscribeToMetabolicChatList: PropTypes.func.isRequired,
  unsubscribeFromMetabolicChatList: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  selectedTab: get(ownProps.location, 'query.tab', tabValues.pending),
  currentUserSuperAgentModeCapability: getCurrentUserSuperAgentModeCapability(state),
});

const mapDispatchToProps = {
  handleTabChange: goToHomeTab,
  subscribeToQueues,
  unsubscribeFromQueues,
  subscribeToMetabolicChatList,
  unsubscribeFromMetabolicChatList,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
