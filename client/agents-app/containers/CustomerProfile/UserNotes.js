import { connect } from 'react-redux';
import { updateUserNotes } from '../../store/actions/data/customers';
import UserNotes from '../../components/CustomerProfile/UserNotes';

const mapDispatchToProps = { updateUserNotes };

export default connect(null, mapDispatchToProps)(UserNotes);
