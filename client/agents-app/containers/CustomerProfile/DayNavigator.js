import { connect } from 'react-redux';
import moment from 'moment';
import { getCustomerDayFromApi } from '../../store/actions/data/customers';
import { goToReadOnlyEntry } from '../../store/actions/ui/routes';
import { moveToCustomerDay } from '../../store/actions/ui/customers';
import { getCurrentDayEntrySummaries } from '../../store/selectors/data/entries';
import { getCurrentCustomerDay, getIsLoading } from '../../store/selectors/data/customers';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import DayNavigator from '../../components/CustomerProfile/DayNavigator';

const promiseFn = props => ({
  customerDayLoading: props.getCustomerDayFromApi(props.customerId, moment(props.customerDay, 'YYYY-MM-DD')),
});

const mapStateToProps = state => ({
  loading: getIsLoading(state),
  currentCustomerDay: getCurrentCustomerDay(state),
  entriesForCurrentDay: getCurrentDayEntrySummaries(state),
});

const mapDispatchToProps = { getCustomerDayFromApi, moveToCustomerDay, goToReadOnlyEntry };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PromiseLoadingWrapper(promiseFn, DayNavigator));
