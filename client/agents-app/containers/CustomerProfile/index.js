import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import { getCustomerFromApi } from '../../store/actions/data/customers';
import { getCustomer } from '../../store/selectors/data/customers';
import { goToHome } from '../../store/actions/ui/routes';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import CustomerProfile from '../../components/CustomerProfile';

const promiseFn = props => ({
  customerLoading: props.getCustomerFromApi(props.params.id),
});

const mapStateToProps = (state, ownProps) => ({
  customer: getCustomer(state, { customerId: ownProps.params.id }),
  customerDay: ownProps.location.query.day,
});

const mapDispatchToProps = { getCustomerFromApi, goToHome, goBack };

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, CustomerProfile),
);
