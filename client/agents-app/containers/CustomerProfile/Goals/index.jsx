import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import times from 'lodash/times';
import zipObject from 'lodash/zipObject';
import isEmpty from 'lodash/isEmpty';
import { getInternalGoalsValues } from '../../../store/selectors/forms/goals';
import { getIsSubmittingGoals } from '../../../store/selectors/data/customers';
import ReadOnlyGoals from '../../../components/CustomerProfile/Goals/ReadOnlyGoals';
import GoalsForm from './GoalsForm';

function zipCustomerGoals(customerInternalGoals = []) {
  return zipObject(times(customerInternalGoals.length, String), customerInternalGoals);
}

class GoalsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editing: false,
      internalGoalsActiveIds: Object.keys(this.props.internalGoals),
    };

    this.onEditClicked = this.onEditClicked.bind(this);
    this.onStopEditing = this.onStopEditing.bind(this);
  }

  componentWillReceiveProps({ submitting, customerInternalGoals }) {
    // After submitting, activeIds get reset to their indexes from the API
    if (!submitting && submitting !== this.props.submitting) {
      this.setState({
        internalGoalsActiveIds: Object.keys(zipCustomerGoals(customerInternalGoals)),
      });
    }
  }

  onEditClicked() {
    this.setState({ editing: true });
  }

  onStopEditing() {
    this.setState({ editing: false });
  }

  render() {
    if (this.state.editing) {
      return (
        <GoalsForm
          {...this.props}
          internalGoalsActiveIds={this.state.internalGoalsActiveIds}
          handleStopEditing={this.onStopEditing}
        />
      );
    }

    return (
      <ReadOnlyGoals
        {...this.props}
        internalGoalsActiveIds={this.state.internalGoalsActiveIds}
        handleEditClicked={this.onEditClicked}
      />
    );
  }
}

GoalsContainer.propTypes = {
  submitting: PropTypes.bool.isRequired,
  internalGoals: PropTypes.shape({}),
};

GoalsContainer.defaultProps = {
  internalGoals: [],
};

const mapStateToProps = (state, { customerInternalGoals }) => {
  const internalGoals = getInternalGoalsValues(state);

  return {
    submitting: getIsSubmittingGoals(state),
    internalGoals: isEmpty(internalGoals) ? zipCustomerGoals(customerInternalGoals) : internalGoals,
  };
};

export default connect(mapStateToProps)(GoalsContainer);
