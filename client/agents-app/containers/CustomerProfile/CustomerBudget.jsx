import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CustomerBudget from '../../components/CustomerProfile/CustomerBudget';
import NutritionalBudget from './NutritionalBudget';
import customPropTypes from '../../prop-types';
import { getCurrentNutritionalBudget } from '../../store/selectors/data/customers';
import {
  loadCustomerCurrentNutritionalBudget,
  submitNutritionalBudget,
} from '../../store/actions/data/customers';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import { calculateCurrentNutritionalBudget } from '../../../../common/lib/nutritional-budget';

function promiseFn(props) {
  return {
    customerBudgetLoaded: props.loadCustomerCurrentNutritionalBudget(props.customerId),
  };
}

class CustomerNutritionalBudgetContainer extends Component {
  constructor(props) {
    super(props);
    const nutritionalBudget = calculateCurrentNutritionalBudget(props.biometricData, null);
    this.state = {
      editing: false,
      nutritionalBudget,
      recommendedWeight: nutritionalBudget.recommendedWeight,
      caloriesAdjustment: nutritionalBudget.caloriesAdjustment,
    };
    this.handleEditClicked = this.handleEditClicked.bind(this);
    this.handleStopEditing = this.handleStopEditing.bind(this);
    this.handleCaloriesAdjustmentChange = this.handleCaloriesAdjustmentChange.bind(this);
    this.handleRecommendedWeightChange = this.handleRecommendedWeightChange.bind(this);
    this.handleSubmitNutritionalBudget = this.handleSubmitNutritionalBudget.bind(this);
    this.handleCloseNutritionalBudgetModal = this.handleCloseNutritionalBudgetModal.bind(this);
  }

  handleEditClicked() {
    this.setState({ editing: true });
  }

  handleStopEditing() {
    this.setState({ editing: false });
  }

  handleCloseNutritionalBudgetModal() {
    this.handleStopEditing();
  }

  handleRecommendedWeightChange(event) {
    const { biometricData } = this.props;
    const nutritionalBudget = calculateCurrentNutritionalBudget(
      biometricData,
      event.target.value,
      this.state.caloriesAdjustment,
    );
    this.setState({
      nutritionalBudget,
      caloriesAdjustment: nutritionalBudget.caloriesAdjustment,
      recommendedWeight: nutritionalBudget.recommendedWeight,
    });
  }

  handleCaloriesAdjustmentChange(event) {
    const { biometricData } = this.props;
    const nutritionalBudget = calculateCurrentNutritionalBudget(
      biometricData,
      this.state.recommendedWeight,
      event.target.value,
    );
    this.setState({
      nutritionalBudget,
      caloriesAdjustment: nutritionalBudget.caloriesAdjustment,
      recommendedWeight: nutritionalBudget.recommendedWeight,
    });
  }

  handleSubmitNutritionalBudget(calculatedBudget) {
    const { customerId, handleSubmitNutritionalBudget } = this.props;
    handleSubmitNutritionalBudget(customerId, calculatedBudget);
    this.handleStopEditing();
  }

  render() {
    if (this.state.editing) {
      return (
        <NutritionalBudget
          {...this.props}
          nutritionalBudget={this.state.nutritionalBudget}
          nutritionalBudgetModalOpen={this.state.editing}
          onRecommendedWeightChange={this.handleRecommendedWeightChange}
          onCaloriesAdjustmentChange={this.handleCaloriesAdjustmentChange}
          onSubmitNutritionalBudget={this.handleSubmitNutritionalBudget}
          onCloseNutritionalBudgetModal={this.handleCloseNutritionalBudgetModal}
        />
      );
    }

    return (
      <CustomerBudget {...this.props} onEditClicked={this.handleEditClicked} />
    );
  }
}

CustomerNutritionalBudgetContainer.propTypes = {
  biometricData: customPropTypes.biometricData,
  handleSubmitNutritionalBudget: PropTypes.func.isRequired,
  customerId: PropTypes.string.isRequired,
};

CustomerNutritionalBudgetContainer.defaultProps = {
  biometricData: {},
};

function mapStateToProps(state) {
  return {
    customerBudgetList: getCurrentNutritionalBudget(state),
  };
}

const mapDispatchToProps = {
  loadCustomerCurrentNutritionalBudget,
  handleSubmitNutritionalBudget: submitNutritionalBudget,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PromiseLoadingWrapper(promiseFn, CustomerNutritionalBudgetContainer));
