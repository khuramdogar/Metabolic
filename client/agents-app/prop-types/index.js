import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {
  photoDataShape,
  customerShape,
  customerSummaryShape,
} from '../../common/prop-types/agents';

const ids = PropTypes.arrayOf(PropTypes.string);

const agent = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
});

const agentSummary = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
});

const agentsList = ImmutablePropTypes.listOf(agent);

const agentSummariesList = ImmutablePropTypes.listOf(agentSummary);

const error = PropTypes.shape({
  getHumanMessage: PropTypes.func,
});

const nutrients = PropTypes.shape({
  proteins: PropTypes.number,
  fats: PropTypes.number,
  carbs: PropTypes.number,
});

const nutrientsSummary = PropTypes.arrayOf(PropTypes.shape({
  budget: PropTypes.number.isRequired,
  humanName: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
}));

const nutritionalData = PropTypes.shape({
  proteins: PropTypes.number,
  fats: PropTypes.number,
  carbs: PropTypes.number,
  calories: PropTypes.number,
  caloriesAdjustment: PropTypes.number,
  recommendedWeight: PropTypes.number,
});

const eatenFood = PropTypes.shape({
  foodId: PropTypes.string,
  measureId: PropTypes.string,
  measureAmount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
});

const eatenFoods = PropTypes.arrayOf(eatenFood);
const eatenFoodList = ImmutablePropTypes.listOf(eatenFood);

const massInformation = PropTypes.shape({
  totalGrams: PropTypes.number,
  biggestMass: PropTypes.number,
  perFood: PropTypes.shape({}),
});

const reviewMessage = PropTypes.shape({
  text: PropTypes.string,
});

const message = PropTypes.shape({
  id: PropTypes.string.isRequired,
  author: PropTypes.shape({
    email: PropTypes.string,
    name: PropTypes.string,
  }),
  authorId: PropTypes.string,
  chatId: PropTypes.string,
  text: PropTypes.string,
  source: PropTypes.shape({ message: PropTypes.string }),
  sourceType: PropTypes.string,
  imagesData: PropTypes.arrayOf(photoDataShape),
  updatedAt: PropTypes.string,
  createdAt: PropTypes.string,
});

const messagesList = ImmutablePropTypes.listOf(message);

const files = PropTypes.arrayOf(PropTypes.shape({ preview: PropTypes.string }));

const foodSearchResult = PropTypes.shape({
  name: PropTypes.string,
  externalId: PropTypes.string,
  externalSource: PropTypes.string,
});

const foodSearchResultsList = ImmutablePropTypes.listOf(foodSearchResult);

const nutrientValue = PropTypes.shape({
  id: PropTypes.string,
  name: PropTypes.string,
  unit: PropTypes.string,
  value: PropTypes.number,
});

const measureValues = PropTypes.shape({
  id: PropTypes.string,
  label: PropTypes.string,
  equivalentValue: PropTypes.number,
});

const measuresValues = PropTypes.arrayOf(measureValues);

const nutrientValues = PropTypes.arrayOf(nutrientValue);

const food = PropTypes.shape({
  name: PropTypes.string,
  externalSource: PropTypes.string,
  externalId: PropTypes.string,
  nutrientValues,
  measuresValues,
});

const foodList = ImmutablePropTypes.listOf(food);

const customers = PropTypes.arrayOf(customerShape);

const customerSummariesSet = ImmutablePropTypes.setOf(customerSummaryShape);

const location = PropTypes.shape({
  state: PropTypes.shape({}),
});

const nutritionTip = PropTypes.shape({
  id: PropTypes.string.isRequired,
  agentId: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  createdAt: PropTypes.string,
  updatedAt: PropTypes.string,
});

const nutritionTips = PropTypes.arrayOf(nutritionTip);
const nutritionTipsList = ImmutablePropTypes.listOf(nutritionTip);

const nutritionSummary = PropTypes.arrayOf(PropTypes.shape({
  budget: PropTypes.number.isRequired,
  humanName: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
}));

const biometricData = PropTypes.shape({
  birthdate: PropTypes.string,
  height: PropTypes.number,
  weight: PropTypes.number,
  sex: PropTypes.string,
});

const internalGoal = PropTypes.shape({
  text: PropTypes.string,
  status: PropTypes.string,
});

const internalGoals = PropTypes.arrayOf(internalGoal);

const presetGoal = PropTypes.shape({
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  status: PropTypes.string,
});

const presetGoals = PropTypes.arrayOf(presetGoal);

const otherGoal = PropTypes.shape({
  text: PropTypes.string,
  status: PropTypes.string,
});

const spellingError = PropTypes.shape({
  offset: PropTypes.number.isRequired,
  length: PropTypes.number.isRequired,
  bad: PropTypes.string.isRequired,
  better: PropTypes.arrayOf(PropTypes.string).isRequired,
});

const spellingErrors = PropTypes.arrayOf(spellingError);

const entryAnalytics = PropTypes.shape({
  id: PropTypes.string,
  trafficLightEvaluation: PropTypes.string,
});

const entry = PropTypes.shape({
  id: PropTypes.string.isRequired,
  parentEntryId: PropTypes.string,
  userId: PropTypes.string.isRequired,
  agentId: PropTypes.string,
  text: PropTypes.string,
  photosData: PropTypes.arrayOf(photoDataShape),
  status: PropTypes.string,
  datetime: PropTypes.string,
  timezone: PropTypes.string,
  claimedDate: PropTypes.string,
  agentActionDate: PropTypes.string,
  finishedDate: PropTypes.string,
  nutrients,
  eatenFoodsData: PropTypes.arrayOf(eatenFood),
  entryAnalytics,
});

const entries = PropTypes.arrayOf(entry);

const entrySummary = PropTypes.shape({
  id: PropTypes.string.isRequired,
  parentEntryId: PropTypes.string,
  userId: PropTypes.string.isRequired,
  agentId: PropTypes.string,
  text: PropTypes.string,
  photosData: PropTypes.arrayOf(photoDataShape),
  status: PropTypes.string,
  datetime: PropTypes.string,
  timezone: PropTypes.string,
});

const entrySummaries = PropTypes.arrayOf(entrySummary);
const entrySummariesList = ImmutablePropTypes.listOf(entrySummary);
const entrySummariesOrderedSet = ImmutablePropTypes.setOf(entrySummary);

const customerDay = PropTypes.shape({
  id: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  timezone: PropTypes.string,
  entrySummaries: PropTypes.oneOfType([entrySummaries, ids]),
  lastNutritionTips: nutritionTips,
  nutritionSummary,
});

const metabolicChat = PropTypes.shape({
  lastMessageAt: PropTypes.string,
  customerSummary: customerSummaryShape,
});
const metabolicChatsList = ImmutablePropTypes.listOf(metabolicChat);

export default {
  photosData: photoDataShape,
  entry,
  entries,
  entrySummary,
  entrySummaries,
  entrySummariesList,
  entrySummariesOrderedSet,
  error,
  nutrients,
  nutrientsSummary,
  nutritionalData,
  eatenFoods,
  eatenFoodList,
  massInformation,
  reviewMessage,
  message,
  messagesList,
  files,
  foodSearchResult,
  foodSearchResultsList,
  food,
  foodList,
  measuresValues,
  nutrientValues,
  agent,
  agentsList,
  agentSummariesList,
  customer: customerShape,
  customerSummary: customerSummaryShape,
  customers,
  customerSummariesSet,
  location,
  customerDay,
  biometricData,
  internalGoal,
  internalGoals,
  presetGoal,
  presetGoals,
  otherGoal,
  nutritionTip,
  nutritionTipsList,
  spellingErrors,
  metabolicChat,
  metabolicChatsList,
};
