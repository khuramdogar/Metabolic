export const lifecycleEvents = {
  open: 'open',
  close: 'close',
  end: 'end',
  reconnected: 'reconnected',
  reconnectFailed: 'reconnect failed',
};

export const queuesEvents = {
  subscribe: 'queues:subscribe',
  unsubscribe: 'queues:unsubscribe',
  subscribed: 'queues:subscribed',
  unsubscribed: 'queues:unsubscribed',
  current: 'queues:current',
  changes: 'queues:changes',
};

export const entriesEvents = {
  error: 'entries:error',
  subscribe: 'entries:subscribe',
  unsubscribe: 'entries:unsubscribe',
  subscribed: 'entries:subscribed',
  unsubscribed: 'entries:unsubscribed',
  currentMessages: 'entries:chat:messages',
  newMessage: 'entries:chat:new-message',
};

export const metabolicChatEvents = {
  list: {
    subscribe: 'metabolic-chat:list:subscribe',
    unsubscribe: 'metabolic-chat:list:unsubscribe',
    subscribed: 'metabolic-chat:list:subscribed',
    unsubscribed: 'metabolic-chat:list:unsubscribed',
    users: 'metabolic-chat:list:users',
    newMessage: 'metabolic-chat:list:new-message',
  },
  users: {
    subscribe: 'users:metabolic-chat:subscribe',
    unsubscribe: 'users:metabolic-chat:unsubscribe',
    subscribed: 'users:metabolic-chat:subscribed',
    unsubscribed: 'users:metabolic-chat:unsubscribed',
    messages: 'users:metabolic-chat:messages',
    newMessage: 'users:metabolic-chat:new-message',
  },
};
