import { normalize } from 'normalizr';
import configureStore from '../../store';
import { entriesEvents } from '../events';
import { createListener } from './utils';
import { entryMessagesSchema, entryMessageSchema } from '../../normalizr';
import { setCurrentMessages, addNewMessage } from '../../store/actions/data/entries';

const store = configureStore();

export default [
  createListener(
    entriesEvents.currentMessages,
    data => store.dispatch(setCurrentMessages(normalize(data, entryMessagesSchema))),
  ),
  createListener(
    entriesEvents.newMessage,
    data => store.dispatch(addNewMessage(normalize(data, entryMessageSchema))),
  ),
];
