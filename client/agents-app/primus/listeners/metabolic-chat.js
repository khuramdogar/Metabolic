import { normalize } from 'normalizr';
import configureStore from '../../store';
import {
  setChats,
  addChat,
  setMessages,
  addMessage,
} from '../../store/actions/data/metabolic-chat';
import { metabolicChatEvents } from '../events';
import { createListener } from './utils';
import {
  metabolicChatSchema,
  metabolicChatListSchema,
  newMetabolicMessageSchema,
  metabolicMessagesSchema,
} from '../../normalizr';

const store = configureStore();

export default [
  createListener(
    metabolicChatEvents.list.users,
    data => store.dispatch(setChats(normalize(data, metabolicChatListSchema))),
  ),
  createListener(
    metabolicChatEvents.list.newMessage,
    data => store.dispatch(addChat(normalize(data, metabolicChatSchema))),
  ),
  createListener(
    metabolicChatEvents.users.messages,
    data => store.dispatch(setMessages(normalize(data, metabolicMessagesSchema))),
  ),
  createListener(
    metabolicChatEvents.users.newMessage,
    data => store.dispatch(addMessage(normalize(data, newMetabolicMessageSchema))),
  ),
];
