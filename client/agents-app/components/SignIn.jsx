import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import injectSheet from 'react-jss';
import { TextField } from 'redux-form-material-ui';
import Notification from '../../common/components/Notification';
import FormActions from '../../common/components/FormActions';
import SubmitButton from '../../common/components/SubmitButton';
import { required, email } from '../../common/forms/validators';

const styles = {
  signInUp: {
    '& h2': {
      marginBottom: 0,
    },
  },
};

function SignIn(props) {
  const { handleSubmit, submitting } = props;
  const errorMessage = props.error && props.error.getHumanMessage();
  return (
    <div>
      <Notification type="error" message={errorMessage} />
      <h1>Enter your account</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <Field
            name="email" component={TextField}
            hintText="your.email@example.org" floatingLabelText="Your e-mail"
            disabled={submitting} validate={[required, email]}
          />
        </div>
        <div>
          <Field
            name="password" component={TextField}
            type="password" hintText="******" floatingLabelText="Your password"
            disabled={submitting} validate={required}
          />
        </div>
        <FormActions>
          <SubmitButton
            label={'Sign In'}
            submitting={submitting}
            disabled={submitting}
          />
        </FormActions>
      </form>
    </div>
  );
}

SignIn.propTypes = {
  error: PropTypes.shape({ getHumanMessage: PropTypes.func.isRequired }),
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

SignIn.defaultProps = {
  error: undefined,
};

export default injectSheet(styles)(SignIn);
