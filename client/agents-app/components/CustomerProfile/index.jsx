import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import FlatButton from 'material-ui/FlatButton';
import BackIcon from 'material-ui/svg-icons/navigation/chevron-left';
import Divider from 'material-ui/Divider';
import moment from 'moment';
import Goals from '../../containers/CustomerProfile/Goals';
import DayNavigator from '../../containers/CustomerProfile/DayNavigator';
import NutritionTips from '../../containers/CustomerProfile/NutritionTips';
import CustomerBudget from '../../containers/CustomerProfile/CustomerBudget';
import customPropTypes from '../../prop-types';
import BiometricData from './BiometricData';
import UserNotes from '../../containers/CustomerProfile/UserNotes';

const styles = {
  button: {
    margin: '2px',
  },
};

const CustomerProfile = ({ customer, customerDay, goBack, goToHome, classes }) => (
  <div>
    <FlatButton label="Back" icon={<BackIcon />} onClick={goBack} className={classes.button} />
    <FlatButton label="Home" onClick={goToHome} className={classes.button} />

    <h1>{customer.name}</h1>

    <BiometricData unitsSystem={customer.unitsSystem} biometricData={customer.biometricData} />

    <Divider />
    <CustomerBudget
      customerId={customer.id}
      unitsSystem={customer.unitsSystem}
      biometricData={customer.biometricData}
    />
    <Divider />

    <Goals
      customerId={customer.id}
      customerInternalGoals={customer.internalGoals}
      customerPresetGoals={customer.goalsPresets}
      customerOtherGoal={customer.otherGoal}
    />

    <Divider />

    <DayNavigator
      customerId={customer.id}
      customerDay={customerDay}
      customerTimezone={customer.timezone}
    />

    <UserNotes
      userId={customer.id}
      notes={customer.notes}
    />
    <NutritionTips customerId={customer.id} />
  </div>
);

CustomerProfile.propTypes = {
  customer: customPropTypes.customer.isRequired,
  customerDay: PropTypes.string,
  goBack: PropTypes.func.isRequired,
  goToHome: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    button: PropTypes.string.isRequired,
  }).isRequired,
};

CustomerProfile.defaultProps = {
  customerDay: moment().format('YYYY-MM-DD'),
};

export default injectSheet(styles)(CustomerProfile);
