import React from 'react';
import injectSheet from 'react-jss';
import PropTypes from 'prop-types';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import map from 'lodash/map';
import { goalStatuses } from '../../../utils/goal-constants';
import { errorStyle } from '../../../../common/theme';
import customPropTypes from '../../../prop-types';

const styles = {
  inputWrapper: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginTop: '20px',
  },
  field: {
    marginLeft: '10px',
    marginRight: '10px',
  },
  error: errorStyle,
};

export function userGoalIntegrity(value) {
  if (!value.status) {
    return 'Status is required';
  }

  if (value.status === goalStatuses.unrated) {
    return 'Status can not be unrated';
  }

  return undefined;
}

const renderMenuItems = () =>
  map(goalStatuses, (status, key) => <MenuItem key={key} value={status} primaryText={status} />);

const GoalInput = ({ input: { value, onChange }, meta: { error }, text, classes }) => (
  <div>
    <div className={classes.inputWrapper}>
      <SelectField
        floatingLabelText={text}
        onChange={(event, key, status) => onChange({ text: value.text, status })}
        value={value.status}
        className={classes.field}
      >
        {renderMenuItems()}
      </SelectField>
    </div>
    {error && <p className={classes.error}>{error}</p>}
  </div>
);

GoalInput.propTypes = {
  input: PropTypes.shape({
    value: customPropTypes.internalGoal,
    onChange: PropTypes.func.isRequired,
  }),
  meta: PropTypes.shape({ error: PropTypes.string }),
  text: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    inputWrapper: PropTypes.string.isRequired,
    field: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired,
  }).isRequired,
};

GoalInput.defaultProps = {
  input: { value: undefined },
  meta: { error: undefined },
};

export default injectSheet(styles)(GoalInput);
