import React from 'react';
import injectSheet from 'react-jss';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import map from 'lodash/map';
import { goalStatuses } from '../../../utils/goal-constants';
import customPropTypes from '../../../prop-types';
import { errorStyle } from '../../../../common/theme';

const styles = {
  inputWrapper: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginTop: '20px',
  },
  field: {
    marginLeft: '10px',
    marginRight: '10px',
  },
  error: errorStyle,
};

export function internalGoalIntegrity(value) {
  if (!value.text || !value.status) {
    return 'Both text and status are required';
  }

  if (value.status === goalStatuses.unrated) {
    return 'Status can not be unrated';
  }

  return undefined;
}

const renderMenuItems = () =>
  map(goalStatuses, (status, key) => <MenuItem key={key} value={status} primaryText={status} />);

const AgentGoalInput = ({ input: { value, onChange }, meta: { error }, handleRemove, classes }) => (
  <div>
    <div className={classes.inputWrapper}>
      <TextField
        floatingLabelText="Text"
        onChange={(event, text) => onChange({ text, status: value.status })}
        value={value.text}
        className={classes.field}
      />

      <SelectField
        floatingLabelText="Status"
        onChange={(event, key, status) => onChange({ text: value.text, status })}
        value={value.status}
        className={classes.field}
      >
        {renderMenuItems()}
      </SelectField>

      <FlatButton label="remove" onClick={handleRemove} primary />
    </div>
    {error && <p className={classes.error}>{error}</p>}
  </div>
);

AgentGoalInput.propTypes = {
  input: PropTypes.shape({
    value: customPropTypes.internalGoal,
    onChange: PropTypes.func.isRequired,
  }),
  meta: PropTypes.shape({ error: PropTypes.string }),
  handleRemove: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    inputWrapper: PropTypes.string.isRequired,
    field: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired,
  }).isRequired,
};

AgentGoalInput.defaultProps = {
  input: { value: undefined },
  meta: { error: undefined },
};

export default injectSheet(styles)(AgentGoalInput);
