import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import customPropTypes from '../../prop-types';
import { tableWrapper, container } from '../../../common/theme';

const styles = {
  buttonContainerRight: {
    display: 'flex',
    justifyContent: 'flex-end',
    float: 'right',
  },
  tableWrapper,
  container,
};

function displayRecommendedWeight(data) {
  return data ? `${data}lb` : 'N/A';
}

function displayCaloriesAdjustment(data) {
  return data ? `${data}cal` : 'N/A';
}

function CustomerBudget({
  classes,
  customerBudgetList: {
    protein,
    fat,
    carbs,
    calories,
    recommendedWeight,
    caloriesAdjustment,
  },
  onEditClicked,
}) {
  return (
    <div className={classes.container}>
      <div className={classes.buttonContainerRight}>
        <FlatButton label="Edit" onClick={onEditClicked} primary />
      </div>
      <h3 >Current Nutritional Budget</h3>
      <Table selectable={false} style={styles.tableWrapper}>
        <TableBody displayRowCheckbox={false}>
          <TableRow>
            <TableRowColumn>Protein</TableRowColumn>
            <TableRowColumn>{protein || 'N/A'}g</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Fat</TableRowColumn>
            <TableRowColumn>{fat || 'N/A'}g</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Carbs</TableRowColumn>
            <TableRowColumn>{carbs || 'N/A'}g</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Calories</TableRowColumn>
            <TableRowColumn>{calories || 'N/A'}cal</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Recommended Weight</TableRowColumn>
            <TableRowColumn>{displayRecommendedWeight(recommendedWeight)}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Calories Adjustment</TableRowColumn>
            <TableRowColumn>{displayCaloriesAdjustment(caloriesAdjustment)}</TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
    </div>
  );
}

CustomerBudget.propTypes = {
  customerBudgetList: customPropTypes.nutrients.isRequired,
  onEditClicked: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
    buttonContainerRight: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(CustomerBudget);
