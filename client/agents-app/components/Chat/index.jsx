import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import RaisedButton from 'material-ui/RaisedButton';
import Warning from 'material-ui/svg-icons/alert/warning';
import ArrowUp from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import ArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import customPropTypes from '../../prop-types';
import ChatMessage from './ChatMessage';
import Composer from '../../containers/Chat/Composer';
import LightBox from '../../../common/components/LightBox';
import { textCentered } from '../../../common/theme';

const styles = {
  image: {
    height: '640px',
    width: '640px',
    objectFit: 'contain',
    display: 'block',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  textCentered,
  messagesLimit: {
    float: 'left',
    clear: 'both',
  },
  messagesAction: {
    position: 'fixed',
    right: '50px',
    zIndex: 1,
  },
  messagesActionTop: {
    bottom: '165px',
  },
  messagesActionBottom: {
    bottom: '125px',
  },
};

class Chat extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { hasUnreadMessages: false };
    this.renderMessage = this.renderMessage.bind(this);
    this.scrollToStart = this.scrollToStart.bind(this);
    this.scrollToEnd = this.scrollToEnd.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { messages } = this.props;
    const nextMessages = nextProps.messages;

    const shouldSetHasUnreadMessages = nextMessages !== messages &&
      !nextMessages.last().fromAgent &&
      messages.size; // To avoid triggering this when primus sets up

    if (shouldSetHasUnreadMessages) {
      this.setState({ hasUnreadMessages: true });
    }
  }

  scrollToStart() {
    this.messagesStart.scrollIntoView({ behavior: 'smooth' });
  }

  scrollToEnd() {
    this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
    this.setState({ hasUnreadMessages: false });
  }

  renderScrollToStartButton() {
    return (
      <RaisedButton
        icon={<ArrowUp />}
        style={{ ...styles.messagesAction, ...styles.messagesActionTop }}
        onClick={this.scrollToStart}
        primary
      />
    );
  }

  renderScrollToEndButton() {
    const { hasUnreadMessages } = this.state;
    return (
      <RaisedButton
        icon={hasUnreadMessages ? <Warning /> : <ArrowDown />}
        style={{ ...styles.messagesAction, ...styles.messagesActionBottom }}
        onClick={this.scrollToEnd}
        primary={!hasUnreadMessages}
        secondary={hasUnreadMessages}
      />
    );
  }

  renderMessage(message) {
    return (
      <ChatMessage
        key={message.id}
        message={message}
        handleOpenLightBox={this.props.handleOpenLightBox}
        currentUserId={this.props.currentUserId}
      />
    );
  }

  render() {
    const { messages, writable, lightBoxOpen, classes, disabled } = this.props;
    const showScrollControls = this.props.messages.size > 5;

    return (
      <div>
        {(messages.length > 0 || writable) && <h2 className={classes.textCentered}>Comments</h2>}

        <div className={classes.messagesLimit} ref={(el) => { this.messagesStart = el; }} />
        {messages.map(this.renderMessage)}
        <div className={classes.messagesLimit} ref={(el) => { this.messagesEnd = el; }} />
        {showScrollControls && this.renderScrollToStartButton()}
        {showScrollControls && this.renderScrollToEndButton()}

        {writable && <Composer disabled={disabled} />}

        {lightBoxOpen &&
          <LightBox
            images={this.props.lightBoxImages}
            startingIndex={this.props.lightBoxStartingIndex}
            handleClose={this.props.handleCloseLightBox}
          />}
      </div>
    );
  }
}

Chat.propTypes = {
  messages: customPropTypes.messagesList.isRequired,
  lightBoxOpen: PropTypes.bool.isRequired,
  lightBoxImages: PropTypes.arrayOf(PropTypes.string),
  lightBoxStartingIndex: PropTypes.number,
  handleOpenLightBox: PropTypes.func.isRequired,
  handleCloseLightBox: PropTypes.func.isRequired,
  writable: PropTypes.bool,
  classes: PropTypes.shape({
    image: PropTypes.string.isRequired,
    textCentered: PropTypes.string.isRequired,
    messagesLimit: PropTypes.string.isRequired,
  }).isRequired,
  disabled: PropTypes.bool,
  currentUserId: PropTypes.string.isRequired,
};

Chat.defaultProps = {
  writable: true,
  disabled: false,
  lightBoxImages: [],
  lightBoxStartingIndex: 0,
};

export default injectSheet(styles)(Chat);
