import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'material-ui/Popover';
import RaisedButton from 'material-ui/RaisedButton';
import Face from 'material-ui/svg-icons/image/tag-faces';
import injectSheet from 'react-jss';
import 'emoji-mart/css/emoji-mart.css';
import { Picker } from 'emoji-mart';

const styles = {
  popover: {
    display: 'inline-block',
  },
  button: {
    margin: '5px',
  },
};

class EmojiPopover extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };

    this.handleTouchTap = this.handleTouchTap.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.handlePick = this.handlePick.bind(this);
  }

  handlePick(emoji) {
    this.handleRequestClose();
    this.props.handleEmojiPick(emoji);
  }

  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  }

  handleRequestClose() {
    this.setState({ open: false });
  }

  render() {
    return (
      <div className={this.props.classes.popover}>
        <RaisedButton
          label="Emojis"
          labelPosition="before"
          icon={<Face />}
          onClick={this.handleTouchTap}
          className={this.props.classes.button}
          disabled={this.props.disabled}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}
        >
          <Picker onClick={this.handlePick} />
        </Popover>
      </div>
    );
  }
}

EmojiPopover.propTypes = {
  handleEmojiPick: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    button: PropTypes.string.isRequired,
    popover: PropTypes.string.isRequired,
  }).isRequired,
  disabled: PropTypes.bool,
};

EmojiPopover.defaultProps = {
  disabled: false,
};

export default injectSheet(styles)(EmojiPopover);
