import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Field } from 'redux-form';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import RaisedButton from 'material-ui/RaisedButton';
import Title from 'material-ui/svg-icons/editor/title';
import Clear from 'material-ui/svg-icons/content/clear';
import Image from 'material-ui/svg-icons/image/image';
import TextSms from 'material-ui/svg-icons/communication/textsms';
import { TextField } from 'redux-form-material-ui';
import Dropzone from 'react-dropzone';
import EmojiPopover from './EmojiPopover';
import customPropTypes from '../../prop-types';

const styles = {
  button: {
    margin: '5px',
  },
  photosWrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: '5px',
  },
  form: {
    borderWidth: '2px',
    borderRadius: '10px',
    borderStyle: 'solid',
    padding: '15px',
    margin: '10px auto',
    width: '90%',
  },
  dropzone: {
    borderWidth: '2px',
    borderRadius: '10px',
    borderStyle: 'dashed',
    padding: '10px',
  },
  dropzoneDescription: {
    textAlign: 'center',
  },
  picker: {
    position: 'absolute',
    zIndex: 2,
  },
};

const renderPhotos = (photos, handlePhotoChanges) => {
  if (!photos) {
    return null;
  }

  return (
    photos.map((photo, index) => (
      <Chip
        key={photo.name}
        style={styles.chip}
        onRequestDelete={() =>
          handlePhotoChanges([...photos.slice(0, index), ...photos.slice(index + 1)])}
      >
        <Avatar src={photo.preview} />{photo.name}
      </Chip>
    ))
  );
};

const renderDropzoneInput = (field, handlePhotoChanges, classes) => (
  <Dropzone
    name={field.name}
    accept="image/*"
    className={classes.dropzone}
    onDrop={newPhotos => handlePhotoChanges(newPhotos, field.input.value)}
  >
    <p className={classes.dropzoneDescription}>
      Try dropping some photos here, or click this area to select photos manually.
    </p>
  </Dropzone>
);

const renderInputs = (
  dropzoneVisible,
  handlePhotoChanges,
  handleSaveCursorPosition,
  classes,
  disabled,
) => {
  if (dropzoneVisible) {
    return (
      <Field
        name="photos"
        disabled={disabled}
        component={field => renderDropzoneInput(field, handlePhotoChanges, classes)}
      />
    );
  }

  return (
    <Field
      name="text"
      component={TextField}
      onKeyUp={handleSaveCursorPosition}
      onClick={handleSaveCursorPosition}
      disabled={disabled}
      fullWidth
      multiLine
    />
  );
};

const renderProperInputIcon = (dropzoneVisible, text) => {
  if (dropzoneVisible && text) {
    return <TextSms />;
  } else if (dropzoneVisible) {
    return <Title />;
  }

  return <Image />;
};

const Composer = ({
  dropzoneVisible,
  photos,
  text,
  handleDropzoneToggle,
  handlePhotoChanges,
  handleSaveCursorPosition,
  handleEmojiPick,
  handleReset,
  classes,
  disabled,
}) => (
  <form className={classes.form}>
    <div className={classes.photosWrapper}>
      {renderPhotos(photos, handlePhotoChanges)}
    </div>
    {renderInputs(dropzoneVisible, handlePhotoChanges, handleSaveCursorPosition, classes, disabled)}
    <RaisedButton
      label={`Add ${dropzoneVisible ? 'text' : 'images'}`}
      labelPosition="before"
      icon={renderProperInputIcon(dropzoneVisible, text)}
      onClick={handleDropzoneToggle}
      className={classes.button}
      disabled={disabled}
      primary
    />
    <RaisedButton
      label="Reset"
      labelPosition="before"
      icon={<Clear />}
      onClick={handleReset}
      className={classes.button}
      disabled={disabled}
      secondary
    />
    <EmojiPopover handleEmojiPick={handleEmojiPick} disabled={disabled} />
  </form>
);

Composer.propTypes = {
  dropzoneVisible: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  photos: customPropTypes.files.isRequired,
  handleDropzoneToggle: PropTypes.func.isRequired,
  handleSaveCursorPosition: PropTypes.func.isRequired,
  handleEmojiPick: PropTypes.func.isRequired,
  handlePhotoChanges: PropTypes.func.isRequired,
  handleReset: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    button: PropTypes.string.isRequired,
    photosWrapper: PropTypes.string.isRequired,
    form: PropTypes.string.isRequired,
    dropzone: PropTypes.string.isRequired,
    dropzoneDescription: PropTypes.string.isRequired,
  }).isRequired,
  disabled: PropTypes.bool,
};

Composer.defaultProps = {
  disabled: false,
};

export default injectSheet(styles)(Composer);
