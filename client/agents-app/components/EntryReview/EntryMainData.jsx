import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import customPropTypes from '../../prop-types';
import { textCentered } from '../../../common/theme';
import Carousel from '../../../common/components/Carousel';
import { timezonedDateTime } from '../../../common/lib/time';

const styles = {
  textCentered,
};

function allImagesReviewedCheck(
  photosData,
  handleAllImagesReviewedCheck,
  allImagesReviewed,
) {
  if (photosData.length === 1) {
    handleAllImagesReviewedCheck(true);
  } else {
    handleAllImagesReviewedCheck(allImagesReviewed);
  }
}

function EntryMainData(
  { entry, customerTimezone, className, classes, handleAllImagesReviewedCheck }) {
  const shouldShowCarousel = entry.photosData && entry.photosData.length > 0;
  return (
    <div className={className}>
      <h3 className={classes.textCentered}>
        {timezonedDateTime(entry.datetime, entry.timezone || customerTimezone)}
      </h3>

      <p className={classes.textCentered}>
        {entry.text}
      </p>

      {shouldShowCarousel &&
        <Carousel
          content={entry.photosData}
          handleAllImagesReviewedCheck={allImagesReviewed => allImagesReviewedCheck(
              entry.photosData,
              handleAllImagesReviewedCheck,
              allImagesReviewed,
            )}
        />}
    </div>
  );
}

EntryMainData.propTypes = {
  entry: customPropTypes.entry.isRequired,
  handleAllImagesReviewedCheck: PropTypes.func.isRequired,
  customerTimezone: PropTypes.string.isRequired,
  className: PropTypes.string,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
  }).isRequired,
};

EntryMainData.defaultProps = {
  className: undefined,
};

export default injectSheet(styles)(EntryMainData);
