import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Field } from 'redux-form';
import isEmpty from 'lodash/isEmpty';
import EatenFoodInput, { eatenFoodIntegrity } from './EatenFoodInput';
import customPropTypes from '../../../prop-types';
import { shouldBeMainFood } from '../index';
import { textCentered } from '../../../../common/theme';

const styles = {
  textCentered,
};

function renderEatenFoodInput(food, massInformation, entryId, onFoodRemove, disabled) {
  const { measureAmount, equivalentValue } = massInformation.perFood[food.id];
  const { totalGrams, biggestMass } = massInformation;
  const foodGrams = measureAmount * equivalentValue;

  return (
    <Field
      key={food.id}
      name={food.id}
      component={EatenFoodInput}
      foodId={food.id}
      foodName={food.name}
      massPercentage={Math.round((foodGrams / totalGrams) * 100 * 100) / 100}
      mainFood={shouldBeMainFood(foodGrams, totalGrams, biggestMass)}
      measuresValues={food.measuresValues}
      nutrientsValues={food.nutrientsValues}
      handleRemove={() => onFoodRemove(entryId, food.externalSource, food.externalId)}
      validate={eatenFoodIntegrity}
      disabled={disabled}
    />
  );
}

class EatenFoodForm extends Component {
  componentWillUnmount() {
    this.props.onUnmount(this.props.entryId);
  }

  render() {
    const { addedFood, massInformation, entryId, onFoodRemove, classes, disabled } = this.props;

    if (addedFood.isEmpty() || isEmpty(massInformation)) {
      return <h2 className={classes.textCentered}>No Food Has Been Added Yet</h2>;
    }
    const eatenFoodInputs = addedFood
      .map(food => renderEatenFoodInput(food, massInformation, entryId, onFoodRemove, disabled));

    return (
      <form noValidate>
        <h2 className={classes.textCentered}>Added Food</h2>
        {eatenFoodInputs}
      </form>
    );
  }
}

EatenFoodForm.propTypes = {
  addedFood: customPropTypes.foodList.isRequired,
  massInformation: customPropTypes.massInformation,
  entryId: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
  }).isRequired,
  onFoodRemove: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  onUnmount: PropTypes.func.isRequired,
};

EatenFoodForm.defaultProps = {
  massInformation: undefined,
  disabled: false,
};

export default injectSheet(styles)(EatenFoodForm);
