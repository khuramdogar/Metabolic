import React from 'react';
import PropTypes from 'prop-types';
import { ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import AddCircleOutline from 'material-ui/svg-icons/content/add-circle-outline';
import CheckCircle from 'material-ui/svg-icons/action/check-circle';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import customPropTypes from '../../../prop-types';

function renderRightIcon(added, loading) {
  if (added) {
    return <CheckCircle />;
  } else if (loading) {
    return <FileDownload />;
  }

  return <AddCircleOutline />;
}

const FoodSearchListItem = ({ entryId, food, added, loading, entity, handleTap }) => (
  <div>
    <Divider />
    <ListItem
      primaryText={food.name}
      rightIcon={renderRightIcon(added, loading)}
      onClick={() => handleTap(entryId, food, added, entity)}
      disabled={loading}
    />
  </div>
);

FoodSearchListItem.propTypes = {
  entryId: PropTypes.string.isRequired,
  food: customPropTypes.foodSearchResult.isRequired,
  entity: customPropTypes.food,
  added: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  handleTap: PropTypes.func.isRequired,
};

FoodSearchListItem.defaultValues = {
  entity: undefined,
};

export default FoodSearchListItem;
