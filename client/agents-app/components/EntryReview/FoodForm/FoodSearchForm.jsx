import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Field } from 'redux-form';
import { MenuItem } from 'material-ui';
import { TextField, SelectField } from 'redux-form-material-ui';
import keyMirror from 'keymirror';
import SubmitButton from '../../../../common/components/SubmitButton';

export const TYPE_OPTIONS = keyMirror({
  all: null,
  branded: null,
  standard: null,
});

const styles = {
  form: {
    display: 'flex',
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    alignItems: 'flex-end',
  },
};

const searchMenuItems = Object.keys(TYPE_OPTIONS)
  .map(key => <MenuItem key={key} value={TYPE_OPTIONS[key]} primaryText={key} />);

const FoodSearchForm = ({ submitting, handleSubmit, classes }) => (
  <div>
    <h3>Search</h3>
    <form className={classes.form} onSubmit={handleSubmit}>
      <Field
        name="query"
        component={TextField}
        hintText="e.g. Apple"
        floatingLabelText="Search for a food"
        fullWidth
      />
      <Field name="type" component={SelectField} floatingLabelText="Type">
        {searchMenuItems}
      </Field>
      <SubmitButton
        label="Search"
        submitting={submitting}
        disabled={submitting}
      />
    </form>
  </div>
);

FoodSearchForm.propTypes = {
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    form: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(FoodSearchForm);
