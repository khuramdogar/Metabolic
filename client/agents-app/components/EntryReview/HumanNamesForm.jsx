import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import injectSheet from 'react-jss';
import customPropTypes from '../../prop-types';
import { required } from '../../../common/forms/validators';
import { textCentered } from '../../../common/theme';
import HumanNameInput from '../../containers/EntryReview/HumanNameInput';

const styles = {
  textCentered,
  fieldContainer: {
    display: 'flex',
    alignItems: 'baseline',
    justifyContent: 'center',
  },
};

function renderFields(addedFood, classes) {
  return addedFood.map(food => (
    <div key={food.id} className={classes.fieldContainer}>
      <Field
        type="text"
        name={food.id}
        foodName={food.name}
        component={HumanNameInput}
        disabled={!!food.humanName}
        validate={[required]}
        fullWidth
      />
    </div>
  ));
}

const HumanNamesForm = ({
  classes,
  addedFood,
}) => (
  <div>
    <h2 className={classes.textCentered}>Food Display Names</h2>
    <form noValidate>
      {renderFields(addedFood, classes)}
    </form>
  </div>
);

HumanNamesForm.propTypes = {
  addedFood: customPropTypes.foodList.isRequired,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
    fieldContainer: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(HumanNamesForm);
