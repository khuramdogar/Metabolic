import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import customPropTypes from '../../../prop-types';
import SimilarEntry from '../../../containers/EntryReview/SimilarEntries/SimilarEntry';
import Image from '../../../../common/components/Image';
import theme from '../../../../common/theme';

/* This is an image size that lets you see at least 5 images and a fraction
 * of an eventual sixth one within the max-width of the container
 */
const IMAGE_SIZE = 145;

const styles = {
  similarEntriesContainer: {
    textAlign: 'center',
  },
  entriesWrapper: {
    display: 'flex',
    overflowX: 'auto',
    margin: '0 2px',
  },
  entry: {
    margin: '20px',
    cursor: 'pointer',
  },
  iconContainer: {
    width: `${IMAGE_SIZE}px`,
    height: `${IMAGE_SIZE}px`,
  },
};

class SimilarEntries extends Component {
  constructor(props) {
    super(props);
    this.state = { dialogEntryId: undefined };
    this.closeDialog = this.closeDialog.bind(this);
    this.renderEntry = this.renderEntry.bind(this);
    this.renderDialogEntry = this.renderDialogEntry.bind(this);
  }

  openDialog(entry) {
    this.setState({ dialogEntryId: entry.id });
  }

  closeDialog() {
    this.setState({ dialogEntryId: undefined });
  }

  renderTitle() {
    const entriesText = this.props.similarEntries.size > 1
      ? 'Entries'
      : 'Entry';
    return `${this.props.similarEntries.size} ${entriesText} Similar to This One`;
  }

  /* eslint-disable jsx-a11y/no-static-element-interactions */
  renderEntry(entry) {
    const { classes } = this.props;
    return (
      <div
        key={entry.id}
        onClick={() => this.openDialog(entry)}
        className={classes.entry}
      >
        {/* Here it's assumed that entries will always have images,
          * since for now their source is the image similarity system
          */}
        <Image
          src={entry.photosData[0].location}
          size={IMAGE_SIZE}
          className={classes.iconContainer}
          color={theme.palette.primary1Color}
          square
          thumbnail
        />
      </div>
    );
  }

  renderDialogEntry() {
    return (
      <SimilarEntry
        entryId={this.state.dialogEntryId}
        targetEntryId={this.props.entryId}
        onClose={this.closeDialog}
      />
    );
  }

  render() {
    const { similarEntries, classes } = this.props;
    return (
      <div className={classes.similarEntriesContainer}>
        <h2>{this.renderTitle()}</h2>
        <div className={classes.entriesWrapper}>
          {similarEntries.map(this.renderEntry)}
        </div>
        { this.state.dialogEntryId && this.renderDialogEntry() }
      </div>
    );
  }
}

SimilarEntries.propTypes = {
  similarEntries: customPropTypes.entrySummariesList.isRequired,
  entryId: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    similarEntriesContainer: PropTypes.string.isRequired,
    entriesWrapper: PropTypes.string.isRequired,
    entry: PropTypes.string.isRequired,
    iconContainer: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(SimilarEntries);
