import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import TrafficLight from 'react-trafficlight';

const styles = {
  textCentered: {
    textAlign: 'center',
  },
  trafficLightContainer: {
    transform: 'rotate(-90deg)',
    display: 'inline-block',
    padding: '2px',
    marginTop: '-40px',
  },
  analyticsItem: {
    padding: '5px',
  },
  hiddenElement: {
    display: 'none',
  },
};

function AnalyticsInformation(props) {
  const { classes } = props;
  const trafficLight = {
    red: false,
    yellow: false,
    green: false,
    [props.activeTrafficLight]: true,
  };
  return (
    <div className={classes.textCentered}>
      <div className={classes.analyticsItem}>
        <h2>Traffic Light Evaluation</h2>
        <div className={classes.trafficLightContainer}>
          <TrafficLight
            RedOn={trafficLight.red}
            YellowOn={trafficLight.yellow}
            GreenOn={trafficLight.green}
            Size={50}
          />
        </div>
      </div>
    </div>
  );
}

AnalyticsInformation.propTypes = {
  activeTrafficLight: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
    trafficLightContainer: PropTypes.string.isRequired,
    analyticsItem: PropTypes.string.isRequired,
    hiddenElement: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(AnalyticsInformation);
