import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';

const styles = {
  paperStyles: {
    padding: '20px',
    maxWidth: '1024px',
    margin: '30px auto 0',
  },
};

export default function LayoutPaper({ children }) {
  return <Paper style={styles.paperStyles}>{children}</Paper>;
}

LayoutPaper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

LayoutPaper.defaultProps = {
  children: null,
};
