import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import { reviewStates, reviewTypes } from '../utils/review-constants';

const styles = {
  button: {
    margin: '2px',
  },
};

const renderReviewTypeButton = (currentReviewType, type, setReviewType, disabled) => (
  <RaisedButton
    label={type}
    primary={currentReviewType === type}
    onClick={() => setReviewType(type)}
    style={styles.button}
    disabled={disabled}
  />
);

const renderReviewButtonsSet = (currentReviewState, currentReviewType, setReviewType, disabled) => {
  if (currentReviewState !== reviewStates.resolve) {
    return null;
  }

  return (
    <span>
      {renderReviewTypeButton(currentReviewType, reviewTypes.search, setReviewType, disabled)}
      {renderReviewTypeButton(currentReviewType, reviewTypes.manual, setReviewType, disabled)}
      {renderReviewTypeButton(currentReviewType, reviewTypes.na, setReviewType, disabled)}
    </span>
  );
};

const renderReviewStateButton = (currentReviewState, setReviewState, disabled) => {
  const newState = currentReviewState === reviewStates.resolve
    ? reviewStates.needMoreInfo : reviewStates.resolve;
  return (
    <RaisedButton
      label={newState}
      onClick={() => setReviewState(newState)}
      disabled={disabled}
      primary
    />
  );
};

const EntryReviewToolbar = ({
  currentReviewState,
  currentReviewType,
  setReviewState,
  setReviewType,
  disabled,
}) => (
  <Toolbar>
    <ToolbarGroup firstChild>
      <ToolbarTitle text={`Review: ${currentReviewState}`} />
    </ToolbarGroup>
    <ToolbarGroup>
      {renderReviewButtonsSet(currentReviewState, currentReviewType, setReviewType, disabled)}
      <ToolbarSeparator />
      {renderReviewStateButton(currentReviewState, setReviewState, disabled)}
    </ToolbarGroup>
  </Toolbar>
);

EntryReviewToolbar.propTypes = {
  currentReviewState: PropTypes.string.isRequired,
  currentReviewType: PropTypes.string.isRequired,
  setReviewState: PropTypes.func.isRequired,
  setReviewType: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

EntryReviewToolbar.defaultProps = {
  disabled: false,
};

export default EntryReviewToolbar;
