import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { ListItem } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import Title from 'material-ui/svg-icons/editor/title';
import Forward from 'material-ui/svg-icons/navigation/chevron-right';
import CircularProgress from 'material-ui/CircularProgress';
import moment from 'moment-timezone';
import Image from '../../common/components/Image';
import customPropTypes from '../prop-types';

const styles = {
  secondaryText: {
    margin: 0,
    padding: 0,
  },
  secondaryTextSpaceBetween: {
    extend: 'secondaryText',
    display: 'flex',
    justifyContent: 'space-between',
  },
};

const truncateTitle = title => (
  title.length < 150 ? title : `${title.slice(0, 100)}...`
);

const renderAvatar = (photosData) => {
  if (photosData[0]) {
    return (
      <Image
        src={photosData[0].thumbnailLocation || photosData[0].location}
        alt="Entry Image"
        thumbnail
      />
    );
  }

  return <Avatar icon={<Title />} />;
};

const EntryListItem = ({
  entry: { id, text, datetime, createdAt, photosData, agentSummary, userSummary },
  entryBeingClaimed,
  extendedInfo,
  onClick,
  classes,
}) => {
  const rightIconButton = (
    <IconButton>
      {entryBeingClaimed === id ? <CircularProgress size={24} /> : <Forward />}
    </IconButton>
  );

  const listItemProps = {
    primaryText: text ? truncateTitle(text) : 'No Text',
    secondaryTextLines: 2,
    leftAvatar: renderAvatar(photosData),
    rightIconButton,
    disabled: Boolean(entryBeingClaimed),
    onClick,
  };

  const timeText = `${moment(datetime).tz(userSummary.timezone).format('lll z')} (created ${moment(createdAt).fromNow()})`;
  const secondaryTextTags = [
    <p key="firstLine" className={classes.secondaryText}>{userSummary.name}</p>,
  ];
  if (extendedInfo) {
    secondaryTextTags.push(
      <p key="secondLine" className={classes.secondaryTextSpaceBetween}>
        <span>{timeText}</span>
        <span>Claimed by {agentSummary.name}</span>
      </p>,
    );
  } else {
    secondaryTextTags.push(
      <p key="secondLine" className={classes.secondaryText}>{timeText}</p>,
    );
  }

  listItemProps.secondaryText = <div>{secondaryTextTags}</div>;

  return (
    <div>
      <Divider />
      <ListItem {...listItemProps} />
    </div>
  );
};

EntryListItem.propTypes = {
  entry: customPropTypes.entrySummary.isRequired,
  entryBeingClaimed: PropTypes.string,
  extendedInfo: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    secondaryText: PropTypes.string.isRequired,
    secondaryTextSpaceBetween: PropTypes.string.isRequired,
  }),
};

EntryListItem.defaultProps = {
  entryBeingClaimed: undefined,
  extendedInfo: false,
};

export default injectSheet(styles)(EntryListItem);
