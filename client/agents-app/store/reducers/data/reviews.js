import { Map, List } from 'immutable';
import keyMirror from 'keymirror';
import pick from 'lodash/pick';
import matches from 'lodash/matches';
import ACTION_TYPES from '../../actions/types';

const REVIEWS_ACTION_TYPES = ACTION_TYPES.REVIEWS;

const SEARCH_TYPES = keyMirror({
  all: null,
  branded: null,
  standard: null,
});

const INITIAL_STATE = Map({
  foodSearchResults: Map({
    [SEARCH_TYPES.all]: List(),
    [SEARCH_TYPES.branded]: List(),
    [SEARCH_TYPES.standard]: List(),
  }),
  addedFood: Map({}),
  loadingFood: Map({}),
  similarEntryIds: Map({}),
});

export default function queuesReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case REVIEWS_ACTION_TYPES.SET_FOOD_SEARCH_RESULTS: {
      const { searchType, result: { foodSearchResults } } = action.payload;
      return state.setIn(['foodSearchResults', searchType], List(foodSearchResults));
    }
    case REVIEWS_ACTION_TYPES.ADD_LOADING_FOOD: {
      const { entryId, externalId, externalSource } = action.payload;
      const loadingFoodForEntry = state.getIn(['loadingFood', entryId]) || List();

      return state.setIn(['loadingFood', entryId], loadingFoodForEntry.push({ externalId, externalSource }));
    }
    case REVIEWS_ACTION_TYPES.REMOVE_LOADING_FOOD: {
      const { entryId, externalId, externalSource } = action.payload;
      const loadingFoodForEntry = state.getIn(['loadingFood', entryId]) || List();

      return state.setIn(
        ['loadingFood', entryId],
        loadingFoodForEntry.filterNot(matches({ externalId, externalSource })),
      );
    }
    case REVIEWS_ACTION_TYPES.ADD_FOOD_FROM_SEARCH: {
      const { entryId, entities: { food }, result } = action.payload;
      const addedFoodForEntry = state.getIn(['addedFood', entryId]) || List();

      return state.setIn(['addedFood', entryId], addedFoodForEntry.push(pick(food[result], 'id', 'externalId', 'externalSource')));
    }
    case REVIEWS_ACTION_TYPES.REMOVE_FOOD_FROM_SEARCH: {
      const { entryId, externalId, externalSource } = action.payload;
      const addedFoodForEntry = state.getIn(['addedFood', entryId]) || List();

      return state.setIn(
        ['addedFood', entryId],
        addedFoodForEntry.filterNot(matches({ externalId, externalSource })),
      );
    }
    case REVIEWS_ACTION_TYPES.RESET_REVIEW: {
      const { entryId } = action.payload;
      return Object.keys(SEARCH_TYPES)
        .map(key => ({ keyPath: ['foodSearchResults', SEARCH_TYPES[key]], value: List() }))
        .concat([
          { keyPath: ['addedFood', entryId], value: List() },
          { keyPath: ['similarEntryIds', entryId], value: undefined },
        ])
        .reduce((newState, { keyPath, value }) => newState.setIn(keyPath, value), state);
    }
    case REVIEWS_ACTION_TYPES.ADD_FOOD_FROM_API: {
      const { entryId, entities: { food }, result: { food: foodIds } } = action.payload;
      const foodToAdd = foodIds.map(id => pick(food[id], 'id', 'externalId', 'externalSource'));
      const addedFoodForEntry = state.getIn(['addedFood', entryId]) || List();

      return state.setIn(['addedFood', entryId], addedFoodForEntry.concat(foodToAdd));
    }
    case REVIEWS_ACTION_TYPES.ADD_FOOD: {
      const { entryId, food } = action.payload;
      const foodToAdd = food.map(singleFood => pick(singleFood, 'id', 'externalId', 'externalSource'));
      const addedFoodForEntry = state.getIn(['addedFood', entryId]) || List();

      return state.setIn(['addedFood', entryId], addedFoodForEntry.concat(foodToAdd));
    }
    case REVIEWS_ACTION_TYPES.SET_SIMILAR_ENTRY_ID: {
      const { entryId, similarEntryId } = action.payload;
      return state.setIn(['similarEntryIds', entryId], similarEntryId);
    }
    default:
      return state;
  }
}
