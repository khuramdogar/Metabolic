import { Map, List } from 'immutable';
import ACTION_TYPES from '../../actions/types';

const METABOLIC_CHAT_ACTION_TYPES = ACTION_TYPES.METABOLIC_CHATS;

const INITIAL_STATE = Map({
  chatListInitialized: false,
  orderedChats: List(),
  messagesByCustomer: Map(),
});

export default function queuesReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case METABOLIC_CHAT_ACTION_TYPES.LIST.SET_CHATS: {
      return state.merge({
        orderedChats: List(action.payload.result.chats),
        chatListInitialized: true,
      });
    }
    case METABOLIC_CHAT_ACTION_TYPES.LIST.ADD_CHAT: {
      const orderedChats = state.get('orderedChats');
      const metabolicChatId = action.payload.result;
      const index = orderedChats.indexOf(metabolicChatId);

      return state.set(
        'orderedChats',
        (index >= 0 ? orderedChats.delete(index) : orderedChats).insert(0, metabolicChatId),
      );
    }
    case METABOLIC_CHAT_ACTION_TYPES.USER.SET_MESSAGES: {
      const { userId, messages } = action.payload.result;
      return state.setIn(['messagesByCustomer', userId], List(messages));
    }
    case METABOLIC_CHAT_ACTION_TYPES.USER.ADD_MESSAGE: {
      const { userId, message } = action.payload.result;
      return state.setIn(
        ['messagesByCustomer', userId],
        state.getIn(['messagesByCustomer', userId]).push(message),
      );
    }
    default:
      return state;
  }
}
