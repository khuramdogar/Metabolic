import { Map, OrderedSet, List } from 'immutable';
import ACTION_TYPES from '../../actions/types';

const ENTRIES_ACTION_TYPES = ACTION_TYPES.ENTRIES;

const INITIAL_STATE = Map({
  queues: Map({
    active: OrderedSet(),
    inactive: OrderedSet(),
    pending: OrderedSet(),
    waiting: OrderedSet(),
  }),
  queuesInitialized: false,
  entryMessages: Map({}),
  similarEntries: Map({}),
});

export default function queuesReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ENTRIES_ACTION_TYPES.SET_CURRENT_QUEUES: {
      const { active, inactive, pending, waiting } = action.payload.queues;
      return state.merge({
        queues: Map({
          active: OrderedSet(active),
          inactive: OrderedSet(inactive),
          pending: OrderedSet(pending),
          waiting: OrderedSet(waiting),
        }),
        queuesInitialized: true,
      });
    }
    case ENTRIES_ACTION_TYPES.ADD_ENTRY: {
      const { entryId, queue } = action.payload;
      return state.setIn(
        ['queues', queue],
        state.getIn(['queues', queue]).add(entryId),
      );
    }
    case ENTRIES_ACTION_TYPES.REMOVE_ENTRY: {
      const { entryId, queue } = action.payload;
      return state.setIn(
        ['queues', queue],
        state.getIn(['queues', queue]).delete(entryId),
      );
    }
    case ENTRIES_ACTION_TYPES.SET_CURRENT_MESSAGES: {
      const { result: { entryId, messages } } = action.payload;
      return state.setIn(['entryMessages', entryId], List(messages));
    }
    case ENTRIES_ACTION_TYPES.NEW_MESSAGE: {
      const { result: { entryId, message } } = action.payload;
      return state.setIn(
        ['entryMessages', entryId],
        state.getIn(['entryMessages', entryId]).push(message),
      );
    }
    case ENTRIES_ACTION_TYPES.SET_SIMILAR_ENTRIES: {
      const { entryId, result } = action.payload;
      return state.setIn(['similarEntries', entryId], List(result));
    }
    default:
      return state;
  }
}
