import Immutable from 'immutable';
import pick from 'lodash/pick';
import ACTION_TYPES from '../actions/types';

const API_ACTION_TYPES = ACTION_TYPES.API;
const { Map } = Immutable;

const INITIAL_STATE = Map({
  accessToken: null,
  userId: null,
  signInError: null,
});

export default function sessionAgentReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ACTION_TYPES.REHYDRATE:
      if (action.payload && action.payload.session) {
        return new Map(pick(action.payload.session, 'accessToken', 'userId'));
      }
      return state;
    case API_ACTION_TYPES.SIGNIN.START:
      return state.merge({
        accessToken: null,
        userId: null,
        signInError: null,
      });
    case API_ACTION_TYPES.SIGNIN.SUCCESS:
      return state.merge({
        accessToken: action.payload.id,
        userId: action.payload.userId,
      });
    case API_ACTION_TYPES.SIGNIN.FAILURE:
      return state.merge({
        signInError: action.error,
      });
    case API_ACTION_TYPES.SIGNOUT.START:
      return state;
    case API_ACTION_TYPES.SIGNOUT.SUCCESS:
      return new Map(INITIAL_STATE);
    case API_ACTION_TYPES.SIGNOUT.FAILURE:
      return new Map(INITIAL_STATE);
    default:
      return state;
  }
}
