import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import data from './data';
import ui from './ui';
import session from './session';
import currentUser from './current-user';
import ACTION_TYPES from '../actions/types';

const SIGNOUT_ACTIONS = ACTION_TYPES.API.SIGNOUT;

const appReducer = combineReducers({
  data,
  ui,
  session,
  currentUser,
  form: formReducer,
  routing: routerReducer,
});

export default function rootReducer(state, action) {
  if (action.type === SIGNOUT_ACTIONS.SUCCESS || action.type === SIGNOUT_ACTIONS.FAILURE) {
    // eslint-disable-next-line no-param-reassign
    state = undefined;
  }

  return appReducer(state, action);
}
