import Immutable from 'immutable';
import ACTION_TYPES from '../../../common/store/actions/types';

const CURRENT_USER_ACTION_TYPES = ACTION_TYPES.CURRENT_USER;
const { Map } = Immutable;

const INITIAL_STATE = Map();

export default function currentUserReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CURRENT_USER_ACTION_TYPES.SET_CURRENT_USER:
      return new Map(action.payload);
    case CURRENT_USER_ACTION_TYPES.UNSET_CURRENT_USER:
      return INITIAL_STATE;
    default:
      return state;
  }
}
