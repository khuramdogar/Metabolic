import { Map } from 'immutable';
import ACTION_TYPES from '../../actions/types';
import { reviewTypes, reviewStates } from '../../../utils/review-constants';

const REVIEWS_ACTION_TYPES = ACTION_TYPES.REVIEWS;

const INITIAL_STATE = Map({
  state: 'Resolve',
  type: reviewTypes.search,
  submitting: false,
});

export default function reviewsReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case REVIEWS_ACTION_TYPES.SET_REVIEW_STATE: {
      const reviewType = action.payload.state === reviewStates.resolve ? reviewTypes.search : null;
      return state.merge({ state: action.payload.state, type: reviewType });
    }
    case REVIEWS_ACTION_TYPES.SET_REVIEW_TYPE: {
      return state.set('type', action.payload.type);
    }
    case REVIEWS_ACTION_TYPES.REVIEW_SUBMISSION: {
      return state.set('submitting', action.payload.submitting);
    }
    default:
      return state;
  }
}
