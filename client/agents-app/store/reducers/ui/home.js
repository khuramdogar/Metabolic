import Immutable from 'immutable';
import ACTION_TYPES from '../../actions/types';

const HOME_ACTION_TYPES = ACTION_TYPES.HOME;
const ACTIONS = ACTION_TYPES;

const { Map } = Immutable;

const INITIAL_STATE = Map({
  superAgentMode: false,
  agentIdFilter: '',
});

export default function homeReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ACTIONS.REHYDRATE: {
      if (action.payload && action.payload.superAgent) {
        return state.merge({
          superAgentMode: action.payload.superAgent,
        });
      }
      return state;
    }
    case HOME_ACTION_TYPES.CHANGE_AGENT_FILTER: {
      return state.merge({
        agentIdFilter: action.payload.agentId,
      });
    }
    case HOME_ACTION_TYPES.TOGGLE_SUPERAGENT_MODE: {
      return state.merge({
        superAgentMode: !state.get('superAgentMode'),
      });
    }
    default:
      return state;
  }
}
