import { Map } from 'immutable';
import keyMirror from 'keymirror';
import ACTION_TYPES from '../../actions/types';

export const origins = keyMirror({ composer: null });

const SPELLCHECK_ACTION_TYPES = ACTION_TYPES.SPELLCHECK;

const INITIAL_STATE = Map({
  loading: Map({
    composer: false,
  }),
  errors: Map({
    composer: [],
  }),
});

export default function reviewsReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SPELLCHECK_ACTION_TYPES.SET_SPELL_CHECKING: {
      return state.setIn(['loading', action.payload.origin], action.payload.spellChecking);
    }
    case SPELLCHECK_ACTION_TYPES.SET_ERRORS: {
      return state.setIn(['errors', action.payload.origin], action.payload.errors);
    }
    default:
      return state;
  }
}
