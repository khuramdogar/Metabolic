import Immutable from 'immutable';
import ACTION_TYPES from '../../actions/types';

const APP_ACTION_TYPES = ACTION_TYPES.APP;
const API_ACTION_TYPES = ACTION_TYPES.API;
const { Map } = Immutable;

const INITIAL_STATE = Map({
  appLoaded: false,
});

export default function appReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case APP_ACTION_TYPES.LOADING_APP: {
      return state.merge({
        appLoaded: false,
      });
    }
    /* After signout, all reducers are reset. But if appLoaded sets to false again,
    then users will only see a loading spinner when signing out. */
    case APP_ACTION_TYPES.LOADED_APP:
    case API_ACTION_TYPES.SIGNOUT.SUCCESS:
    case API_ACTION_TYPES.SIGNOUT.FAILURE: {
      return state.merge({
        appLoaded: true,
      });
    }
    default:
      return state;
  }
}
