import { createSelector } from 'reselect';
import { List } from 'immutable';
import isEmpty from 'lodash/isEmpty';
import map from 'lodash/map';
import pick from 'lodash/pick';
import intersection from 'lodash/intersection';
import differenceBy from 'lodash/differenceBy';
import getEntities from './entities';
import { getEntryId } from './entries';
import { getFoodEntities, getFoodEntitiesValues, getEatenFoodsData } from './food';
import { getCurrentSearchType } from '../forms/search-foods';
import getEatenFoodFormValues from '../forms/search-review';

export default function getReviews(state) {
  return state.data.reviews;
}

export const getSimilarEntryIds = createSelector(
  getReviews,
  reviews => reviews.get('similarEntryIds'),
);

export const getFoodSearchResultEntities = createSelector(
  getEntities,
  entities => entities.get('foodSearchResults'),
);

export const getFoodSearchResults = createSelector(
  getReviews,
  reviews => reviews.get('foodSearchResults'),
);

export const getCurrentFoodSearchResults = createSelector(
  getCurrentSearchType,
  getFoodSearchResultEntities,
  getFoodSearchResults,
  (searchType, foodSearchResultEntities, foodSearchResults) => (
    foodSearchResults && searchType
      ? foodSearchResults.get(searchType).map(id => foodSearchResultEntities.get(id))
      : List()
  ),
);

const compareFoodByExternalId = (food1, food2) => (
  food1.externalId === food2.externalId && food1.externalSource === food2.externalSource
);

function getSearchedFood(state, props) {
  return props.food;
}

export const getAddedFood = createSelector(
  getReviews,
  reviews => reviews.get('addedFood'),
);

export const getEntryAddedFood = createSelector(
  getEntryId,
  getAddedFood,
  (entryId, addedFood) => addedFood.get(entryId) || List(),
);

export const getEntryAddedFoodEntities = createSelector(
  getFoodEntitiesValues,
  getEntryAddedFood,
  (foodEntitiesValues, addedFood) =>
    addedFood.map(({ externalSource, externalId }) =>
      foodEntitiesValues.find(foodEntity =>
        externalSource === foodEntity.externalSource && externalId === foodEntity.externalId)),
);

export const getAlreadyLoadedFood = createSelector(
  getEatenFoodsData,
  getEntryAddedFood,
  getFoodEntities,
  (eatenFoodsData, entryAddedFood, foodEntities) => {
    const loadedFoodIds = intersection(map(eatenFoodsData, 'foodId'), Object.keys(foodEntities.toJS()));
    const loadedFood = loadedFoodIds.map(id => foodEntities.get(id));
    return differenceBy(loadedFood, entryAddedFood.toArray(), 'id');
  },
);

export const getIsFoodAddedInEntry = createSelector(
  getSearchedFood,
  getEntryAddedFood,
  (searchedFood, entryAddedFood) =>
    !!entryAddedFood.find(food => compareFoodByExternalId(food, searchedFood)),
);

const getLoadingFood = createSelector(
  getReviews,
  reviews => reviews.get('loadingFood'),
);

const getEntryLoadingFood = createSelector(
  getEntryId,
  getLoadingFood,
  (entryId, loadingFood) => loadingFood.get(entryId) || List(),
);

export const getIsFoodLoadingInEntry = createSelector(
  getSearchedFood,
  getEntryLoadingFood,
  (searchedFood, entryLoadingFood) =>
    !!entryLoadingFood.find(food => compareFoodByExternalId(food, searchedFood)),
);

export const getFoodEntityFromExternalData = createSelector(
  getFoodEntitiesValues,
  getSearchedFood,
  (foodEntities, searchedFood) =>
    foodEntities.find(foodEntity => compareFoodByExternalId(foodEntity, searchedFood)),
);

export const getEatenFoodInitialValues = createSelector(
  getEatenFoodsData,
  getFoodEntities,
  (eatenFoodsData, foodEntities) => {
    if (!foodEntities) {
      return {};
    }

    const initialValues = {};
    eatenFoodsData.forEach((eatenFood) => {
      const { foodId, quantity, measureAmount, measureId } = eatenFood;
      initialValues[foodId] = {
        ...eatenFood,
        measureAmount: measureAmount && !isNaN(measureAmount)
          ? measureAmount.toString()
          : (quantity || 0).toString(),
        measureId: measureId || foodEntities.get(foodId).measuresValues[0].id,
      };
    });

    return initialValues;
  },
);

function computeMassInformation(addedFood, eatenFood) {
  if (!addedFood || !(eatenFood.length || eatenFood.size)) {
    return undefined;
  }

  const perFood = {};
  let totalGrams = 0;
  let biggestMass = 0;

  addedFood.forEach((food) => {
    const { measureId, measureAmount } = eatenFood.find(({ foodId }) => foodId === food.id);
    const { label, equivalentValue } = food.measuresValues.find(({ id }) => id === measureId);
    const grams = measureAmount * equivalentValue;

    perFood[food.id] = { label, measureAmount, equivalentValue };
    biggestMass = Math.max(biggestMass, grams);
    totalGrams += grams;
  });

  return { totalGrams, biggestMass, perFood };
}

// TODO: fix the patched filter.
export const getEatenFood = createSelector(
  getEntryId,
  getEatenFoodFormValues,
  getAddedFood,
  (entryId, searchReviewValues, addedFood) => (searchReviewValues && addedFood.get(entryId)
    ? addedFood.get(entryId)
        .map(({ id }) => pick(searchReviewValues[id], 'foodId', 'measureAmount', 'measureId'))
        .filter(food => !isEmpty(food))
    : List()
  ),
);

export const getEatenFoodReadOnlyMassInformation = createSelector(
  getEntryAddedFoodEntities,
  getEatenFoodsData,
  computeMassInformation,
);

export const getEatenFoodFormMassInformation = createSelector(
  getEntryAddedFoodEntities,
  getEatenFood,
  computeMassInformation,
);
