import { createSelector } from 'reselect';
import getEntities from './entities';

export default function nutritionTipsSelector(state) {
  return state.data.nutritionTips;
}

export const getNutritionTipsEntities = createSelector(
  getEntities,
  entities => entities.get('nutritionTips'),
);

export const getNutritionTipsList = createSelector(
  nutritionTipsSelector,
  getNutritionTipsEntities,
  (nutritionTips, nutritionTipsEntities) => nutritionTips.get('nutritionTips')
    .map(id => nutritionTipsEntities.get(id)),
);

export const getIsLoadingNutritionTips = createSelector(
  nutritionTipsSelector,
  nutritionTips => nutritionTips.get('loading'),
);
