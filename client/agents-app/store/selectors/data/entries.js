import { createSelector } from 'reselect';
import get from 'lodash/get';
import { List } from 'immutable';
import getEntities from './entities';
import { getCurrentCustomerDay } from './customers';

export default function getEntries(state) {
  return state.data.entries;
}

function addSummaryEntities(entry, entities) {
  const entryToReturn = { ...entry };

  if (entryToReturn.agentId) {
    entryToReturn.agentSummary = entities.getIn(['agentSummaries', entryToReturn.agentId]);
  }

  if (entryToReturn.userId) {
    entryToReturn.userSummary = entities.getIn(['customerSummaries', entryToReturn.userId]);
  }

  return entryToReturn;
}

export const getEntriesEntities = createSelector(
  getEntities,
  entities => entities.get('entries').map(entry => addSummaryEntities(entry, entities)),
);

export const getEntrySummaryEntities = createSelector(
  getEntities,
  entities => entities.get('entrySummaries').map(entry => addSummaryEntities(entry, entities)),
);

export const getCurrentDayEntrySummaries = createSelector(
  getCurrentCustomerDay,
  getEntrySummaryEntities,
  (currentCustomerDay, entrySummariesEntities) =>
    get(currentCustomerDay, 'entrySummaries', []).map(id => entrySummariesEntities.get(id)),
);

export const getQueuesInitialized = createSelector(
  getEntries,
  entries => entries.get('queuesInitialized'),
);

// TODO: avoid repetition between getPending, Active, Waiting and Inactive Entries.
export const getPendingEntries = createSelector(
  getEntries,
  getEntrySummaryEntities,
  (entries, entrySummaryEntities) =>
    entries.getIn(['queues', 'pending']).map(id => entrySummaryEntities.get(id)),
);

export const getActiveEntries = createSelector(
  getEntries,
  getEntrySummaryEntities,
  (entries, entrySummaryEntities) =>
    entries.getIn(['queues', 'active']).map(id => entrySummaryEntities.get(id)),
);

export const getWaitingEntries = createSelector(
  getEntries,
  getEntrySummaryEntities,
  (entries, entrySummaryEntities) =>
    entries.getIn(['queues', 'waiting']).map(id => entrySummaryEntities.get(id)),
);

export const getInactiveEntries = createSelector(
  getEntries,
  getEntrySummaryEntities,
  (entries, entrySummaryEntities) =>
    entries.getIn(['queues', 'inactive']).map(id => entrySummaryEntities.get(id)),
);

export function getEntryId(state, props) {
  return props.entryId;
}

const getSimilarEntryIdsForEntry = createSelector(
  getEntries,
  getEntryId,
  (entries, entryId) => entries.getIn(['similarEntries', entryId]) || List(),
);

export const getSimilarEntriesList = createSelector(
  getSimilarEntryIdsForEntry,
  getEntrySummaryEntities,
  (entryIds, entrySummaryEntities) => entryIds.map(id => entrySummaryEntities.get(id)),
);

export const getEntry = createSelector(
  getEntryId,
  getEntriesEntities,
  (entryId, entriesEntities) => entriesEntities.get(entryId),
);
