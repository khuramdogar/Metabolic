import { createSelector } from 'reselect';
import { List } from 'immutable';
import getEntities from './entities';
import getEntries, { getEntry } from './entries';

export const getMessages = createSelector(
  getEntities,
  entities => entities.get('messages'),
);

const getMessageIdsForEntry = createSelector(
  getEntries,
  getEntry,
  (entries, entry) => entries.getIn(['entryMessages', entry.id]) || List(),
);

export const getMessagesForEntry = createSelector(
  getMessageIdsForEntry,
  getMessages,
  (messageIds, messages) => messageIds.map(id => messages.get(id)),
);
