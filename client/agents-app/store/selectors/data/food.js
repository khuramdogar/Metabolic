import { createSelector } from 'reselect';
import { List } from 'immutable';
import map from 'lodash/map';
import difference from 'lodash/difference';
import getEntities from './entities';

export const getFoodEntities = createSelector(
  getEntities,
  entities => entities.get('food'),
);

export const getFoodEntitiesValues = createSelector(
  getFoodEntities,
  foodEntities => (foodEntities ? foodEntities.valueSeq().toList() : List()),
);

export function getEatenFoodsData(state, props) {
  return props.eatenFoodsData;
}

export const getFoodIdsToLoad = createSelector(
  getEatenFoodsData,
  getFoodEntities,
  (eatenFoodsData, foodEntities) =>
    difference(map(eatenFoodsData, 'foodId'), Object.keys(foodEntities.toJS())),
);
