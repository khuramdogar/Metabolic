import { createSelector } from 'reselect';

export default function subscriptionsSelector(state) {
  return state.data.subscriptions;
}

export const getSubscribedToQueues = createSelector(
  subscriptionsSelector,
  subscriptions => subscriptions.get('queues'),
);

export const getSubscribedEntries = createSelector(
  subscriptionsSelector,
  subscriptions => subscriptions.get('entries').toArray(),
);

export const getSubscribedToMetabolicChatList = createSelector(
  subscriptionsSelector,
  subscriptions => subscriptions.get('metabolicChatList'),
);

export const getSubscribedChats = createSelector(
  subscriptionsSelector,
  subscriptions => subscriptions.get('metabolicChats').toArray(),
);
