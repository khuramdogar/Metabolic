import { createSelector } from 'reselect';
import { getCurrentUserSuperAgentModeCapability } from '../current-user';

export default function homeSelector(state) {
  return state.ui.home;
}

export const getIsSuperAgentModeEnabled = createSelector(
  homeSelector,
  getCurrentUserSuperAgentModeCapability,
  (home, currentUserSuperAgentModeCapability) =>
    currentUserSuperAgentModeCapability && home.get('superAgentMode'),
);

export const getAgentIdFilter = createSelector(
  homeSelector,
  home => home.get('agentIdFilter'),
);
