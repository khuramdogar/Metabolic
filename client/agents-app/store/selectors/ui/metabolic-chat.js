import { createSelector } from 'reselect';

export default function metabolicChatsSelector(state) {
  return state.ui.metabolicChats;
}

export const getIsSubmitting = createSelector(
  metabolicChatsSelector,
  metabolicChats => metabolicChats.get('submitting'),
);
