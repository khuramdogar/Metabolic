import { createSelector } from 'reselect';

export default function getSpellCheck(state) {
  return state.ui.spellCheck;
}

function getOrigin(state, props) {
  return props.origin;
}

export const getIsSpellChecking = createSelector(
  getSpellCheck,
  getOrigin,
  (spellCheck, origin) => spellCheck.getIn(['loading', origin]),
);

export const getSpellingErrors = createSelector(
  getSpellCheck,
  getOrigin,
  (spellCheck, origin) => spellCheck.getIn(['errors', origin]),
);
