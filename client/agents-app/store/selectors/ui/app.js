import { createSelector } from 'reselect';

export default function appSelector(state) {
  return state.ui.app;
}

export const getAppLoaded = createSelector(
  appSelector,
  app => app.get('appLoaded'),
);
