import { createSelector } from 'reselect';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { getEntry, getEntryId } from '../data/entries';
import { getEntryAddedFood, getEatenFood, getSimilarEntryIds } from '../data/reviews';
import { getUserId } from '../../../../common/store/selectors/session';
import { getIsSpellChecking } from './spell-check';
import { getHasSearchErrors } from '../forms/search-review';
import { getHasHumanNamesErrors } from '../forms/human-names';
import { getNutritionalData, getHasManualErrors } from '../forms/manual-review';
import getAnalyticsValues, { getHasAnalyticsFormErrors } from '../forms/analytics';
import getComposerValues from '../forms/composer';
import { reviewStates, reviewTypes, reviewActions } from '../../../utils/review-constants';

export default function getReviews(state) {
  return state.ui.reviews;
}

export const getReviewState = createSelector(
  getReviews,
  reviews => reviews.get('state'),
);

export const getReviewType = createSelector(
  getReviews,
  reviews => reviews.get('type'),
);

export const getIsSubmitting = createSelector(
  getReviews,
  reviews => reviews.get('submitting'),
);

const getSubFormErrors = createSelector(
  getReviewState,
  getReviewType,
  getHasManualErrors,
  getHasSearchErrors,
  getEntryAddedFood,
  getComposerValues,
  getHasAnalyticsFormErrors,
  getHasHumanNamesErrors,
  (
    reviewState,
    reviewType,
    hasManualErrors,
    hasSearchErrors,
    addedFood,
    composerValues,
    hasAnalyticsErrors,
    hasHumanNamesErrors,
  ) => ({
    manual: hasManualErrors || hasAnalyticsErrors,
    search: isEmpty(addedFood) || hasSearchErrors || hasAnalyticsErrors || hasHumanNamesErrors,
    composer: (!get(composerValues, 'text') &&
      (reviewState === reviewStates.needMoreInfo || reviewType === reviewTypes.na)),
  }),
);

export const getDisabledSubmit = createSelector(
  getUserId,
  getEntry,
  getReviewState,
  getReviewType,
  getSubFormErrors,
  getIsSubmitting,
  getIsSpellChecking,
  (currentUserId, entry, reviewState, reviewType, subFormErrors, submitting, spellChecking) => {
    const disabled = submitting || spellChecking || currentUserId !== entry.agentId;

    switch (reviewType) {
      case reviewTypes.manual:
        return disabled || subFormErrors.manual;
      case reviewTypes.search:
        return disabled || subFormErrors.search;
      default:
        return disabled || subFormErrors.composer;
    }
  },
);

const getSubmitAction = createSelector(
  getReviewState,
  getReviewType,
  (currentReviewState, currentReviewType) => {
    if (currentReviewState === reviewStates.needMoreInfo) {
      return reviewActions.requestInfo;
    }

    if (currentReviewType === reviewTypes.search || currentReviewType === reviewTypes.manual) {
      return reviewActions.evaluate;
    }

    return reviewActions.answer;
  },
);

const getSimilarEntryId = createSelector(
  getSimilarEntryIds,
  getEntryId,
  (similarEntryIds, entryId) => similarEntryIds.get(entryId),
);

export const getSubmitBody = createSelector(
  getReviewType,
  getNutritionalData,
  getEatenFood,
  getAnalyticsValues,
  getComposerValues,
  getSimilarEntryId,
  getSubmitAction,
  (
    currentReviewType,
    nutritionalData,
    eatenFoods,
    analytics,
    composerValues,
    similarEntryId,
    submitAction,
  ) => {
    const body = { action: submitAction };

    if (currentReviewType === reviewTypes.manual) {
      const { proteins: protein, carbs, fats: fat, calories } = nutritionalData;
      body.nutritionalData = { protein, carbs, fat, calories };
    } else if (currentReviewType === reviewTypes.search) {
      body.eatenFoods = eatenFoods.toArray();
    }

    if (!isEmpty(analytics)) {
      body.analytics = analytics;
    }

    if (!isEmpty(composerValues)) {
      body.message = composerValues;
      body.message.text = body.message.text || '';
    }

    if (similarEntryId) {
      body.similarEntryId = similarEntryId;
    }

    return body;
  },
);
