import { getFormValues, getFormSyncErrors } from 'redux-form';

export default function getEatenFoodFormValues(state) {
  return getFormValues('searchReview')(state) || {};
}

export function getHasSearchErrors(state) {
  return !!getFormSyncErrors('searchReview')(state);
}
