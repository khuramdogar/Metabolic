import { createSelector } from 'reselect';
import { getFormValues } from 'redux-form';

export default function getFoodSearchValues(state) {
  return getFormValues('foodSearch')(state) || {};
}

export const getCurrentSearchQuery = createSelector(
  getFoodSearchValues,
  values => values.query,
);

export const getCurrentSearchType = createSelector(
  getFoodSearchValues,
  values => values.type,
);
