import { getFormValues, getFormSyncErrors } from 'redux-form';
import { createSelector } from 'reselect';

export default function getHumanNamesInitialValues(state, props) {
  return props.addedFood
    .map(food => ({ [food.id]: food.humanName }))
    .reduce((values, food) => (Object.assign(values, food)), {});
}

const getFormValuesFromHumanNamesForm = getFormValues('humanNamesForm');

export function getHumanNamesValues(state) {
  return getFormValuesFromHumanNamesForm(state) || {};
}

const getFormErrorsFromHumanNamesForm = getFormSyncErrors('humanNamesForm');

export function getHasHumanNamesErrors(state) {
  return !!getFormErrorsFromHumanNamesForm(state);
}

export const getHumanNamesAsFoods = createSelector(
  getHumanNamesValues,
  humanNames => Object.keys(humanNames).map(id => ({ id, humanName: humanNames[id] })),
);
