import { createSelector } from 'reselect';

export default function currentUserSelector(state) {
  return state.currentUser;
}

export const getCurrentUserSuperAgentModeCapability = createSelector(
  currentUserSelector,
  currentUser => !!currentUser.get('superAgent'),
);

