import mapValues from 'lodash/mapValues';
import keyMirror from 'keymirror';

const API_CALL_ACTION_TYPES = keyMirror({
  START: null,
  SUCCESS: null,
  FAILURE: null,
});

function buildApiActions(apiCall) {
  return mapValues(API_CALL_ACTION_TYPES, (value, key) => `API_${apiCall}_${key}`);
}

const apiActions = {
  API: mapValues({
    SIGNIN: null,
    SIGNOUT: null,
    SIGNUP: null,
  }, (value, key) => buildApiActions(key)),
};

const subscriptionActions = {
  SUBSCRIPTIONS: keyMirror({
    SUBSCRIBE_TO_QUEUES: null,
    UNSUBSCRIBE_FROM_QUEUES: null,
    SUBSCRIBE_TO_ENTRY: null,
    UNSUBSCRIBE_FROM_ENTRY: null,
    SUBSCRIBE_TO_METABOLIC_CHAT_LIST: null,
    UNSUBSCRIBE_FROM_METABOLIC_CHAT_LIST: null,
    SUBSCRIBE_TO_METABOLIC_CHAT: null,
    UNSUBSCRIBE_FROM_METABOLIC_CHAT: null,
  }),
};

const entryActions = {
  ENTRIES: keyMirror({
    SET_CURRENT_QUEUES: null,
    ADD_ENTRY: null,
    SET_ENTRY: null,
    REMOVE_ENTRY: null,
    SET_CURRENT_MESSAGES: null,
    NEW_MESSAGE: null,
    SET_ENTRY_BEING_CLAIMED: null,
    REMOVE_ENTRY_BEING_CLAIMED: null,
    SET_SIMILAR_ENTRIES: null,
  }),
};

const reviewActions = {
  REVIEWS: keyMirror({
    REVIEW_SUBMISSION: null,
    SET_REVIEW_STATE: null,
    SET_REVIEW_TYPE: null,
    SET_FOOD_SEARCH_RESULTS: null,
    ADD_FOOD_FROM_SEARCH: null,
    ADD_FOOD_FROM_API: null,
    ADD_FOOD: null,
    REMOVE_FOOD_FROM_SEARCH: null,
    RESET_REVIEW: null,
    ADD_LOADING_FOOD: null,
    REMOVE_LOADING_FOOD: null,
    SET_SIMILAR_ENTRY_ID: null,
  }),
};

const spellCheckActions = {
  SPELLCHECK: keyMirror({
    SET_SPELL_CHECKING: null,
    SET_ERRORS: null,
  }),
};

const customersActions = {
  CUSTOMERS: keyMirror({
    SET_CUSTOMER: null,
    SET_CUSTOMER_DAY_LOADING: null,
    SET_CURRENT_CUSTOMER_DAY: null,
    SET_CUSTOMER_CURRENT_BUDGET_LOADING: null,
    SET_CUSTOMER_CURRENT_BUDGET: null,
    ADD_CUSTOMERS: null,
    SEARCHING_USERS: null,
    SEARCHED_USERS: null,
    SET_SUBMITTING_GOALS: null,
  }),
};

const homeActions = {
  HOME: keyMirror({
    TOGGLE_SUPERAGENT_MODE: null,
    CHANGE_AGENT_FILTER: null,
  }),
};

const generalActions = keyMirror({
  REHYDRATE: null,
  SET_SIGN_IN_REDIRECTION: null,
  CLEAR_SIGN_IN_REDIRECTION: null,
  SHOW_NOTIFICATION: null,
  HIDE_NOTIFICATION: null,
});

const appActions = {
  APP: keyMirror({
    LOADING_APP: null,
    LOADED_APP: null,
  }),
};

const agentsActions = {
  AGENTS: keyMirror({
    LOADING_AGENTS: null,
    ADD_AGENTS: null,
    LOADED_AGENTS: null,
  }),
};

const nutritionTipsActions = {
  NUTRITION_TIPS: keyMirror({
    LOADING_NUTRITION_TIPS: null,
    ADD_NUTRITION_TIPS: null,
  }),
};

const metabolicChatActions = {
  METABOLIC_CHATS: {
    LIST: keyMirror({
      SET_CHATS: null,
      ADD_CHAT: null,
    }),
    USER: keyMirror({
      SET_MESSAGES: null,
      ADD_MESSAGE: null,
      SET_SUBMITTING: null,
    }),
  },
};

export default {
  ...apiActions,
  ...subscriptionActions,
  ...entryActions,
  ...reviewActions,
  ...customersActions,
  ...homeActions,
  ...generalActions,
  ...appActions,
  ...agentsActions,
  ...nutritionTipsActions,
  ...spellCheckActions,
  ...metabolicChatActions,
};
