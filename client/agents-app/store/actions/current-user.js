import apiService from '../../services/api';
import storageService from '../../../common/services/storage';

import {
  setCurrentUser,
  unsetCurrentUser,
} from '../../../common/store/actions/current-user';

export function assignCurrentUser() {
  return dispatch =>
    apiService.agents.me()
      .then(currentUser => dispatch(setCurrentUser(currentUser)))
      .catch(() => {
        dispatch(unsetCurrentUser());
        storageService.set('redux-store-agents', { session: null });
      });
}

export function unassignCurrentUser() {
  return unsetCurrentUser();
}

export default {
  assignCurrentUser,
  unassignCurrentUser,
};
