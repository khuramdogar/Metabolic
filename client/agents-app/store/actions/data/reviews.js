import { normalize } from 'normalizr';
import { change, reset } from 'redux-form';
import map from 'lodash/map';
import pick from 'lodash/pick';
import { foodSearchResultsSchema, foodSchema, foodArraySchema, entrySchema } from '../../../normalizr';
import ACTION_TYPES from '../types';
import api from '../../../services/api';
import { notification } from '../../actions';
import { setReviewType } from '../../actions/ui/reviews';
import { reviewTypes } from '../../../utils/review-constants';
import { entryStatuses } from '../../../utils/entry-constants';
import { pluralizeNutrientName } from '../../../../common/lib/nutrients';
import { getEntryFromApi, setEntry } from '../../actions/data/entries';

const REVIEWS_ACTION_TYPES = ACTION_TYPES.REVIEWS;

export function setFoodSearchResults(normalizedSearchResults, searchType) {
  return {
    type: REVIEWS_ACTION_TYPES.SET_FOOD_SEARCH_RESULTS,
    payload: {
      searchType,
      entities: normalizedSearchResults.entities,
      result: normalizedSearchResults.result,
    },
  };
}

const addLoadingFood = (entryId, food) => ({
  type: REVIEWS_ACTION_TYPES.ADD_LOADING_FOOD,
  payload: { entryId, externalSource: food.externalSource, externalId: food.externalId },
});

const removeLoadingFood = (entryId, food) => ({
  type: REVIEWS_ACTION_TYPES.REMOVE_LOADING_FOOD,
  payload: { entryId, externalSource: food.externalSource, externalId: food.externalId },
});

export const setSearchReviewValue = (food, eatenFood) => {
  const { foodId, quantity, measureAmount, measureId } = eatenFood;
  return change('searchReview', foodId, {
    ...eatenFood,
    measureAmount: measureAmount && !isNaN(measureAmount)
      ? measureAmount.toString()
      : (quantity || 0).toString(),
    measureId: measureId || food.measuresValues[0].id,
  });
};

export const setManualReviewValue = (field, value) => (
  change('manualReview', field, value ? parseInt(value, 10) : 0)
);

export const setSearchReviewInitialValue = food => (
  change('searchReview', food.id, { foodId: food.id, measureAmount: '0', measureId: food.measuresValues[0].id })
);

export const addNormalizedFood = (entryId, food) => ({
  type: REVIEWS_ACTION_TYPES.ADD_FOOD_FROM_SEARCH,
  payload: { ...normalize(food, foodSchema), entryId },
});

export function addNormalizedFoodAgain(entryId, food) {
  return (dispatch) => {
    dispatch(setSearchReviewInitialValue(food));
    dispatch(addNormalizedFood(entryId, food));
  };
}

export const addFoodFromApi = (entryId, food) => ({
  type: REVIEWS_ACTION_TYPES.ADD_FOOD_FROM_API,
  payload: { ...normalize({ food }, foodArraySchema), entryId },
});

export const addFood = (entryId, food) => ({
  type: REVIEWS_ACTION_TYPES.ADD_FOOD,
  payload: { entryId, food },
});

export function addFoodFromSearch(entryId, foundFood) {
  return (dispatch) => {
    dispatch(addLoadingFood(entryId, foundFood));
    api.agents.getFood(foundFood.externalSource, foundFood.externalId)
      .then((food) => {
        dispatch(setSearchReviewInitialValue(food));
        dispatch(addNormalizedFood(entryId, food));
        dispatch(removeLoadingFood(entryId, foundFood));
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while adding this food.',
          duration: 7000,
        }));
        dispatch(removeLoadingFood(entryId, foundFood));
      });
  };
}

export function removeFoodFromSearch(entryId, externalSource, externalId) {
  return {
    type: REVIEWS_ACTION_TYPES.REMOVE_FOOD_FROM_SEARCH,
    payload: { entryId, externalSource, externalId },
  };
}

export function resetReview(entryId) {
  return (dispatch) => {
    dispatch(reset('manualReview'));
    dispatch(reset('searchReview'));
    dispatch({
      type: REVIEWS_ACTION_TYPES.RESET_REVIEW,
      payload: { entryId },
    });
  };
}

export function submitSearch(searchQuery, searchType) {
  return dispatch => (
    api.agents.search(searchQuery, searchType)
      .then((response) => {
        const toNormalize = { foodSearchResults: response };
        dispatch(setFoodSearchResults(normalize(toNormalize, foodSearchResultsSchema), searchType));
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while searching.',
          duration: 7000,
        }));
      })
  );
}

const setSimilarEntryId = (entryId, similarEntryId) => ({
  type: REVIEWS_ACTION_TYPES.SET_SIMILAR_ENTRY_ID,
  payload: { entryId, similarEntryId },
});

export function copyEntryReview(toEntryId, fromEntry, foods) {
  return (dispatch) => {
    dispatch(resetReview(toEntryId));
    const foodIdsToLoad = map(fromEntry.eatenFoodsData, 'foodId');
    if (foodIdsToLoad.length) {
      dispatch(setReviewType(reviewTypes.search));
      const foodById = foods.reduce((byId, food) => ({ ...byId, [food.id]: food }), {});
      fromEntry.eatenFoodsData.forEach((eatenFood) => {
        dispatch(setSearchReviewValue(foodById[eatenFood.foodId], eatenFood));
        dispatch(addNormalizedFood(toEntryId, foodById[eatenFood.foodId]));
      });
      dispatch(setSimilarEntryId(toEntryId, fromEntry.id));
    } else {
      dispatch(setReviewType(reviewTypes.manual));
      fromEntry.nutrientsSummary.forEach((nutrientSummary) => {
        const nutrientField = pluralizeNutrientName(nutrientSummary.name);
        dispatch(setManualReviewValue(nutrientField, nutrientSummary.value));
      });
      dispatch(setSimilarEntryId(toEntryId, fromEntry.id));
    }
    dispatch(notification.showNotification({
      message: 'The evaluation was successfully applied',
      duration: 7000,
    }));
  };
}

export function getEntryFromApiPrefilled(entryId) {
  return dispatch => (
    dispatch(getEntryFromApi(entryId))
      .then((entry) => {
        if (entry.parentEntry && entry.status === entryStatuses.claimed) {
          const finalEntry = { ...entry, ...pick(entry.parentEntry, ['eatenFoodsData', 'entryAnalytics']) };
          const parentHasManualReview = !entry.parentEntry.eatenFoodsData.length;
          if (parentHasManualReview) {
            finalEntry.nutrientsSummary = Object.keys(entry.parentEntry.nutrients).map(nutrient => (
              { name: nutrient, value: entry.parentEntry.nutrients[nutrient] }
            ));
          }
          const normalizedEntry = normalize(finalEntry, entrySchema);
          dispatch(setEntry(normalizedEntry));
          dispatch(notification.showNotification({
            message: 'Values prefilled from a previous evaluated entry.',
            duration: 7000,
          }));
          if (parentHasManualReview) {
            dispatch(setReviewType(reviewTypes.manual));
          } else {
            dispatch(setReviewType(reviewTypes.search));
          }
          return normalizedEntry.entities.entries[entryId];
        }
        return entry;
      })
  );
}
