import { normalize } from 'normalizr';
import api from '../../../services/api';
import { entrySchema, entrySummarySchema } from '../../../normalizr';
import { notification } from '../../actions';
import { goToEntryReview } from '../ui/routes';
import ACTION_TYPES from '../types';

const ENTRIES_ACTION_TYPES = ACTION_TYPES.ENTRIES;

export function setCurrentQueues(queues) {
  const { entities, result } = queues;

  return {
    type: ENTRIES_ACTION_TYPES.SET_CURRENT_QUEUES,
    payload: {
      queues: result,
      entities,
    },
  };
}

export function changesFromQueues(data, normalizedEntities) {
  const { entryId, operation, queue } = data;
  if (operation === 'add') {
    return {
      type: ENTRIES_ACTION_TYPES.ADD_ENTRY,
      payload: {
        entryId,
        queue,
        entities: normalizedEntities.entities,
      },
    };
  }

  return {
    type: ENTRIES_ACTION_TYPES.REMOVE_ENTRY,
    payload: {
      entryId,
      queue,
    },
  };
}

export function setCurrentMessages(normalizedEntryMessages) {
  return {
    type: ENTRIES_ACTION_TYPES.SET_CURRENT_MESSAGES,
    payload: normalizedEntryMessages,
  };
}

export function addNewMessage(normalizedEntryMessage) {
  return {
    type: ENTRIES_ACTION_TYPES.NEW_MESSAGE,
    payload: normalizedEntryMessage,
  };
}

export function setEntry(normalizedEntry) {
  return {
    type: ENTRIES_ACTION_TYPES.SET_ENTRY,
    payload: normalizedEntry,
  };
}

export function getEntryFromApi(entryId) {
  return dispatch => (
    api.agents.getEntry(entryId)
      .then((entry) => {
        const normalizedEntry = normalize(entry, entrySchema);
        dispatch(setEntry(normalizedEntry));
        return normalizedEntry.entities.entries[entryId];
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while getting this entry.',
          duration: 7000,
        }));
      })
  );
}

function setEntryBeingClaimed(entryId) {
  return {
    type: ENTRIES_ACTION_TYPES.SET_ENTRY_BEING_CLAIMED,
    payload: { entryId },
  };
}

function removeEntryBeingClaimed(entryId) {
  return {
    type: ENTRIES_ACTION_TYPES.REMOVE_ENTRY_BEING_CLAIMED,
    payload: { entryId },
  };
}

export function claimEntry(entryId) {
  return (dispatch) => {
    dispatch(setEntryBeingClaimed(entryId));
    return api.agents.claimEntry(entryId)
      .then((entry) => {
        dispatch(setEntry(normalize(entry, entrySchema)));
        dispatch(goToEntryReview(entryId, true));
        dispatch(removeEntryBeingClaimed(entryId));
        dispatch(notification.showNotification({
          message: 'Entry claimed successfully.',
          duration: 7000,
        }));
      })
      .catch(() => {
        dispatch(removeEntryBeingClaimed(entryId));
        dispatch(notification.showNotification({
          message: 'There was an error while claiming this entry.',
          duration: 7000,
        }));
      });
  };
}

export function loadSimilarEntries(entryId) {
  return dispatch => (
    api.agents.getSimilarEntries(entryId)
      .then((entries) => {
        dispatch({
          type: ENTRIES_ACTION_TYPES.SET_SIMILAR_ENTRIES,
          payload: { entryId, ...normalize(entries, [entrySummarySchema]) },
        });
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while getting similar entries to this one.',
          duration: 7000,
        }));
      })
  );
}
