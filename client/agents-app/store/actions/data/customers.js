import { normalize } from 'normalizr';
import { change } from 'redux-form';
import get from 'lodash/get';
import api from '../../../services/api';
import { customerSchema, customerSummarySchema, customerDaySchema, currentCustomerBudgetSchema } from '../../../normalizr';
import { notification } from '../../actions';
import { customerDayId } from '../../../utils/composite-ids';
import { goalStatuses } from '../../../utils/goal-constants';
import ACTION_TYPES from '../types';
import { getUserId } from '../../../../common/store/selectors/session';
import { customerLists } from '../../../utils/customer-constants';

const CUSTOMERS_ACTION_TYPES = ACTION_TYPES.CUSTOMERS;

export function setCustomer(normalizedCustomer) {
  return {
    type: CUSTOMERS_ACTION_TYPES.SET_CUSTOMER,
    payload: normalizedCustomer,
  };
}

export function setCurrentCustomerDay(normalizedCustomerDay) {
  return {
    type: CUSTOMERS_ACTION_TYPES.SET_CURRENT_CUSTOMER_DAY,
    payload: normalizedCustomerDay,
  };
}

export function getCustomerFromApi(customerId) {
  return dispatch =>
    api.agents.getUser(customerId)
      .then((customer) => {
        const normalizedCustomer = normalize(customer, customerSchema);
        dispatch(setCustomer(normalizedCustomer));
        return normalizedCustomer.entities.customers[customerId];
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while getting this customer.',
          duration: 7000,
        }));
      });
}

export function setCustomerDayLoading(loading) {
  return {
    type: CUSTOMERS_ACTION_TYPES.SET_CUSTOMER_DAY_LOADING,
    payload: { loading },
  };
}

export function getCustomerDayFromApi(customerId, date) {
  return (dispatch) => {
    dispatch(setCustomerDayLoading(true));
    return api.agents.getCustomerDay(customerId, date)
      .then((customerDay) => {
        const id = customerDayId(customerId, customerDay.timezone, customerDay.date);
        dispatch(setCurrentCustomerDay(normalize({ ...customerDay, id }, customerDaySchema)));
        dispatch(setCustomerDayLoading(false));
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while getting this day.',
          duration: 7000,
        }));
        dispatch(setCustomerDayLoading(false));
      });
  };
}

function addCustomers(customerList, normalizedCustomers) {
  return {
    type: CUSTOMERS_ACTION_TYPES.ADD_CUSTOMERS,
    payload: { customerList, ...normalizedCustomers },
  };
}

export function searchUsers(query = '', limit = 20, skip = 0) {
  return (dispatch) => {
    dispatch({ type: CUSTOMERS_ACTION_TYPES.SEARCHING_USERS });
    return api.agents.users(query, limit, skip)
      .then((users) => {
        dispatch(addCustomers(customerLists.others, normalize(users, [customerSummarySchema])));
        dispatch({ type: CUSTOMERS_ACTION_TYPES.SEARCHED_USERS });
      });
  };
}


export function loadCustomers() {
  return (dispatch, getState) => {
    const others = dispatch(searchUsers());
    const engaged = api.agents.customers(getUserId(getState()))
      .then((customers) => {
        dispatch(addCustomers(
          customerLists.engaged,
          normalize(customers, [customerSummarySchema],
        )));
      });
    return Promise.all([others, engaged]);
  };
}

export function updateUserNotes(userId, notes) {
  return dispatch =>
    api.agents.updateUserNotes(userId, notes)
      .then((customer) => {
        const normalizedCustomer = normalize(customer, customerSchema);
        dispatch(setCustomer(normalizedCustomer));
        dispatch(notification.showNotification({
          message: 'Notes saved successfully.',
          duration: 7000,
        }));
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while updating notes.',
          duration: 7000,
        }));
      });
}

export function addInternalGoalToForm(id, internalGoal) {
  return change('goals', `internalGoals.${id}`, {
    text: get(internalGoal, 'text', 'Goal text'),
    status: get(internalGoal, 'status', goalStatuses.unrated),
  });
}

export function setSubmittingGoals(submitting) {
  return {
    type: CUSTOMERS_ACTION_TYPES.SET_SUBMITTING_GOALS,
    payload: { submitting },
  };
}

export function handleSubmitGoals(customerId, internalGoals, goalsPresets, otherGoal) {
  return (dispatch) => {
    dispatch(setSubmittingGoals(true));
    return api.agents.submitGoals(customerId, internalGoals, goalsPresets, otherGoal)
      .then((customer) => {
        dispatch(setCustomer(normalize(customer, customerSchema)));
        dispatch(setSubmittingGoals(false));
        dispatch(notification.showNotification({
          message: 'Goals submitted successfully.',
          duration: 7000,
        }));
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while submitting the goals.',
          duration: 7000,
        }));
        dispatch(setSubmittingGoals(false));
      });
  };
}

export function setCustomerCurrentNutritionalBudget(budget) {
  return {
    type: CUSTOMERS_ACTION_TYPES.SET_CUSTOMER_CURRENT_BUDGET,
    payload: budget,
  };
}

export function loadCustomerCurrentNutritionalBudget(customerId) {
  return dispatch =>
     api.agents.getCustomerCurrentBudget(customerId)
      .then((customerBudget) => {
        dispatch(setCustomerCurrentNutritionalBudget(
          normalize(customerBudget, currentCustomerBudgetSchema)));
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while getting customer current nutritional budget.',
          duration: 7000,
        }));
      });
}

export function submitNutritionalBudget(customerId, budget) {
  return dispatch =>
    api.agents.submitCalculatedNutritionalBudget(customerId, budget)
      .then((customerBudget) => {
        dispatch(setCustomerCurrentNutritionalBudget(
          normalize(customerBudget, currentCustomerBudgetSchema),
        ));
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while submitting customer nutritional budget.',
          duration: 7000,
        }));
      });
}
