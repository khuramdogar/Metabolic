import { push } from 'react-router-redux';
import apiService from '../../services/api';
import storageService from '../../../common/services/storage';

import {
  startSignIn,
  successSignIn,
  failureSignIn,
  startSignOut,
  successSignOut,
  failureSignOut,
} from '../../../common/store/actions/session';

import {
  assignCurrentUser,
  unassignCurrentUser,
} from './current-user';

export function signIn(credentials) {
  return (dispatch, getState) => {
    dispatch(startSignIn());
    return apiService.agents.signIn(credentials)
      .then((payload) => {
        dispatch(successSignIn(payload));
        const { session } = getState();
        storageService.set('redux-store-agents', { session });
        return dispatch(assignCurrentUser());
      })
      .catch((error) => {
        dispatch(failureSignIn(error));
        throw error;
      });
  };
}

export function signOut() {
  return (dispatch) => {
    dispatch(startSignOut());
    return apiService.agents.signOut()
      .catch((error) => {
        if (error.statusCode === 401) {
          return undefined;
        }
        throw error;
      })
      .then((payload) => {
        dispatch(successSignOut(payload));
        storageService.set('redux-store-agents', { session: null });
        dispatch(unassignCurrentUser());
        dispatch(push('/'));
      })
      .catch((error) => {
        dispatch(failureSignOut(error));
        throw error;
      });
  };
}

export default {
  signIn,
  signOut,
};
