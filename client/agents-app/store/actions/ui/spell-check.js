import ACTION_TYPES from '../types';

const SPELLCHECK_ACTION_TYPES = ACTION_TYPES.SPELLCHECK;

export function setSpellChecking(spellChecking, origin) {
  return {
    type: SPELLCHECK_ACTION_TYPES.SET_SPELL_CHECKING,
    payload: {
      spellChecking,
      origin,
    },
  };
}

export function setSpellingErrors(errors, origin) {
  return {
    type: SPELLCHECK_ACTION_TYPES.SET_ERRORS,
    payload: {
      errors,
      origin,
    },
  };
}
