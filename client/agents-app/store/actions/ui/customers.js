import { replaceCustomerDay } from '../../actions/ui/routes';
import { getCustomerDayFromApi } from '../../actions/data/customers';

export function moveToCustomerDay(customerId, customerDate) {
  return (dispatch) => {
    dispatch(replaceCustomerDay(customerId, customerDate.format('YYYY-MM-DD')));
    return dispatch(getCustomerDayFromApi(customerId, customerDate));
  };
}

export default {
  moveToCustomerDay,
};
