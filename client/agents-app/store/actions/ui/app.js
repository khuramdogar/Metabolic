import ACTION_TYPES from '../types';

const APP_ACTION_TYPES = ACTION_TYPES.APP;

export function markAppLoading() {
  return { type: APP_ACTION_TYPES.LOADING_APP };
}

export function markAppLoaded() {
  return { type: APP_ACTION_TYPES.LOADED_APP };
}

export default {
  markAppLoading,
  markAppLoaded,
};
