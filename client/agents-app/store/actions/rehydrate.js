import ACTION_TYPES from './types';

export default function rehydrate(payload) {
  return { action: ACTION_TYPES.REHYDRATE, payload };
}
