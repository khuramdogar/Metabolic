import React from 'react';
import PropTypes from 'prop-types';

/* eslint-disable max-len */
export default function Food(props) {
  return (
    <svg {...props} viewBox="0 0 100 69" fill={props.color} xmlns="http://www.w3.org/2000/svg" version="1.1">
      <path d="M19.8 2.5c0-3.2-4.6-3.3-4.6 0v14.5c0 0.7-0.6 1.3-1.3 1.3h-0.3c-0.7 0-1.3-0.6-1.3-1.3V2.4c0-1.8-1.2-2.4-2.4-2.4C8.7 0 7.5 0.5 7.5 2.4v14.5c0 0.7-0.6 1.3-1.3 1.3H5.9c-0.7 0-1.3-0.6-1.3-1.3V2.4c0-3.3-4.6-3.2-4.6 0 0 3.8 0 14 0 14 0 5.9 1.4 7.4 3.7 9.3 1.9 1.5 3.4 2.3 3.4 6V69h5.6V31.7c0-3.7 1.6-4.5 3.4-6 2.3-1.8 3.7-3.4 3.7-9.3C19.8 16.4 19.8 6.3 19.8 2.5z" />
      <path d="M92.9 3.5c-1 3.4-4 14.6-4.8 21.5 -0.8 6.9 2.8 9.2 5.1 13l1.3 3.2V69h5.5 0c0-0.9 0-25.8 0-37.3 0-11.3 0-26.2 0-28.2C100-0.6 94.4-1.6 92.9 3.5z" />
      <path d="M52.4 1.2c-18.2 0-33 14.9-33 33.3 0 18.4 14.8 33.3 33 33.3 18.2 0 33-14.9 33-33.3C85.4 16.1 70.6 1.2 52.4 1.2zM71.6 53.8c-4.9 5-11.7 8-19.2 8 -7.5 0-14.3-3.1-19.2-8 -4.9-5-7.9-11.8-7.9-19.3 0-7.6 3-14.4 7.9-19.3 4.9-5 11.7-8 19.2-8 7.5 0 14.3 3.1 19.2 8 4.9 5 7.9 11.8 7.9 19.3C79.5 42.1 76.5 48.9 71.6 53.8z" />
      <path d="M34 34.5c0 3.4 0.9 6.6 2.5 9.3l2.9-1.7c-1.3-2.2-2-4.8-2-7.6 0-4.2 1.7-8 4.4-10.7 2.7-2.7 6.5-4.4 10.6-4.4 2.7 0 5.3 0.8 7.5 2.1l1.7-2.9c-2.7-1.6-5.9-2.5-9.2-2.5C42.3 16 34 24.3 34 34.5z" />
    </svg>
  );
}

Food.propTypes = {
  color: PropTypes.string,
};

Food.defaultProps = {
  color: '#ffffff',
};
