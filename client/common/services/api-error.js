import pick from 'lodash/pick';
import BaseError from './base-error';

export default class ApiError extends BaseError {
  constructor(errorObj) {
    super(errorObj.error);
    this.name = 'ApiError';
    Object.assign(this, pick(errorObj.error, 'code', 'name', 'statusCode', 'details'));
  }

  getHumanMessage() {
    const parentHumanMessage = super.getHumanMessage();
    if (!parentHumanMessage) {
      /**
       * In general errors should always have a human message to display to users, so we
       * return this default in case the original doesn't have one.
       * ValidationErrors, however, can have a details property with human error messages
       * for specific properties, so we don't need a general human message for them unless
       * they don't have details at all.
       */
      if (this.name !== 'ValidationError' || !this.details) {
        return this.getDefaultHumanMessage();
      }
    }
    return parentHumanMessage;
  }
}
