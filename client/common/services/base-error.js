/**
 * We cannot use ES6 classes here because transpiling to ES5 has serious limitations
 * when extending builtin classes (like Error).
 */

const DEFAULT_HUMAN_MESSAGE = 'Oops! We had an unfortunate error :(';

export default function BaseError(errorObj = {}) {
  Error.call(this, errorObj.message);
  this.name = 'BaseError';
  // TODO: eventually add here human message building using "code" property
  this.humanMessage = errorObj.humanMessage;
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, BaseError);
  } else {
    this.stack = (new Error()).stack;
  }
}
BaseError.prototype = Object.create(Error.prototype);
BaseError.prototype.constructor = BaseError;

BaseError.prototype.getHumanMessage = function getHumanMessage() {
  return this.humanMessage;
};

BaseError.prototype.getDefaultHumanMessage = function getHumanMessage() {
  return DEFAULT_HUMAN_MESSAGE;
};
