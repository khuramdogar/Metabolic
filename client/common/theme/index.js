import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import { green300, yellow300, red300 } from 'material-ui/styles/colors';
import muiTransitions from 'material-ui/styles/transitions';
import merge from 'lodash/merge';

const mtblcTheme = getMuiTheme(merge({}, darkBaseTheme, {
  palette: {
    primary1Color: '#20CCF6',
    textColor: '#26292D',
    disabledColor: '#BEBFC0',
    greenColor: '#058807',
    goodColor: green300,
    okColor: yellow300,
    badColor: red300,
  },
  fontFamily: '"Noto Sans", sans-serif',
  colors: {
    nutritionBar: '#337AB7',
    nutritionBarExceeded: '#B721B0',
  },
}));

export default mtblcTheme;

export const defaultTransition =
  muiTransitions.create(null, null, null, muiTransitions.easeOutFunction);

export const errorStyle = {
  position: 'relative',
  bottom: '15px',
  fontSize: '12px',
  lineHeight: '12px',
  color: mtblcTheme.textField.errorColor,
  transition: defaultTransition,
  textAlign: 'center',
};

export const textCentered = {
  textAlign: 'center',
};

export const centeredButtonWrapper = {
  margin: '50px auto',
  width: '240px',
};

export const tableWrapper = {
  width: '50%',
  marginRight: 'auto',
  marginLeft: 'auto',
};

export const container = {
  margin: '15px auto',
  width: '75%',
};

export const narrowContainer = {
  margin: '15px auto',
  width: '33%',
  minWidth: '320px',
};
