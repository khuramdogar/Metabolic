import mapValues from 'lodash/mapValues';
import zipObject from 'lodash/zipObject';

import {
  nutrientsSummaryKeys as nutrientsNames,
  oldNutrientsSummaryKeys as oldNutrientsNames,
} from '../../../common/lib/nutrients';

export function pluralizeNutrientName(name) {
  return name[name.length - 1] === 's' ? name : `${name}s`;
}

export function calculateNutrientPercentage(nutrientGrams, totalCalories, gramToCalories) {
  const percentage = ((nutrientGrams * gramToCalories) / totalCalories) * 100;
  return isFinite(percentage) ? percentage : 0;
}

export { nutrientsNames, oldNutrientsNames };

export const caloriesPerGram = {
  [nutrientsNames.proteins]: 4,
  [nutrientsNames.fats]: 9,
  [nutrientsNames.carbs]: 4,
};

export const translatedCaloriesPerGram = {
  [oldNutrientsNames.protein]: caloriesPerGram.proteins,
  [oldNutrientsNames.fat]: caloriesPerGram.fats,
  [oldNutrientsNames.carbs]: caloriesPerGram.carbs,
};

export function processNutrientsSummary(nutrientsSummary) {
  const processedNutrientsSummary = zipObject(
    nutrientsSummary.map(({ name }) => pluralizeNutrientName(name)),
    nutrientsSummary.map(nutrient => +(nutrient.value.toFixed(2))),
  );

  const { calories } = processedNutrientsSummary;
  delete processedNutrientsSummary.calories;

  return {
    calories,
    nutrients: mapValues(processedNutrientsSummary, (value, key) => ({
      amount: value,
      percentage: calculateNutrientPercentage(value, calories, caloriesPerGram[key]),
    })),
  };
}

export default {
  pluralizeNutrientName,
  calculateNutrientPercentage,
  caloriesPerGram,
  translatedCaloriesPerGram,
  processNutrientsSummary,
};
