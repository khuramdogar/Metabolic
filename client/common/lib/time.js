import moment from 'moment-timezone';

export function timezonedDateTime(datetime, timezone) {
  return moment(datetime).tz(timezone).format('dddd, MMMM Do YYYY, hh:mm a z');
}

export default {
  timezonedDateTime,
};
