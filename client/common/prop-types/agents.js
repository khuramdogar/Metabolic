import PropTypes from 'prop-types';
import { photoDataShape } from './common';

export * from './common';

export const entryShape = PropTypes.shape({
  id: PropTypes.string.isRequired,
  text: PropTypes.string,
  datetime: PropTypes.string,
  timezone: PropTypes.string,
  photosData: PropTypes.arrayOf(photoDataShape),
  agentId: PropTypes.string,
});

export const customerShape = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  phoneNumber: PropTypes.string,
  email: PropTypes.string,
  avatarData: photoDataShape,
  locale: PropTypes.string,
  timezone: PropTypes.string,
  unitsSystem: PropTypes.string,
  createdAt: PropTypes.string,
  updatedAt: PropTypes.string,
});
