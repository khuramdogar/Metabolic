export const required = value => (value ? undefined : 'required');
export const repeatPassword = (value, values) =>
  (value === values.password ? undefined : 'must match above one');
export const email = value =>
  (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? undefined : 'invalid e-mail address');
