import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { getSignedIn } from '../store/selectors/session';
import { setSignInRedirection } from '../store/actions/ui/redirections';

class EnsureSignedInContainer extends React.Component {
  componentDidMount() {
    if (!this.props.signedIn) {
      this.props.setSignInRedirection(this.props.currentLocation);
      this.props.goToSignIn();
    }
  }

  render() {
    if (this.props.signedIn) {
      return this.props.children;
    }
    return null;
  }
}

EnsureSignedInContainer.propTypes = {
  signedIn: PropTypes.bool,
  currentLocation: PropTypes.shape({}),
  setSignInRedirection: PropTypes.func.isRequired,
  goToSignIn: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

EnsureSignedInContainer.defaultProps = {
  signedIn: false,
  currentLocation: undefined,
};

function mapStateToProps(state, ownProps) {
  return {
    signedIn: getSignedIn(state),
    currentLocation: ownProps.location,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    goToSignIn: () => dispatch(push('sign-in')),
    setSignInRedirection: location => dispatch(setSignInRedirection(location)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EnsureSignedInContainer);
