import { createSelector } from 'reselect';

export function getRedirections(state) {
  return state.ui.redirections;
}

export const getSignInRedirection = createSelector(getRedirections, redirections =>
  redirections.get('signInRedirection'));
