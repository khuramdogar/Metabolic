import { createSelector } from 'reselect';

function sessionSelector(state) {
  return state.session;
}

const signInError = createSelector(sessionSelector, session => session.get('signInError'));
const signUpError = createSelector(sessionSelector, session => session.get('signUpError'));

export const signInErrorMessage = createSelector(signInError, (error) => {
  if (!error) {
    return undefined;
  }
  return error.statusCode === 401
    ? 'Incorrect e-mail or password'
    : 'We had an error while signing you in. Please try again';
});

export const signUpErrorMessage = createSelector(signUpError, (error) => {
  if (!error) {
    return undefined;
  }
  return error.body && error.body.error.message;
});

export const signingIn = createSelector(sessionSelector, session => session.get('signingIn'));

export const getSignedIn = createSelector(sessionSelector, session => session && !!session.get('accessToken'));

export const getAccessToken = createSelector(sessionSelector, session => session.get('accessToken'));

export const getUserId = createSelector(sessionSelector, session => session.get('userId'));
