import actionTypes from '../../actions/types';

export default function notificationReducer(state = {}, action) {
  switch (action.type) {
    case actionTypes.SHOW_NOTIFICATION:
      return { ...action.payload };
    case actionTypes.HIDE_NOTIFICATION:
      return { ...state, open: false };
    default:
      return state;
  }
}
