import Immutable from 'immutable';
import actionTypes from '../../actions/types';

const { Map } = Immutable;

export default function redirectionsReducer(state = new Map(), action) {
  switch (action.type) {
    case actionTypes.SET_SIGN_IN_REDIRECTION:
      return state.set('signInRedirection', action.location.pathname);
    case actionTypes.CLEAR_SIGN_IN_REDIRECTION:
      return state.delete('signInRedirection');
    default:
      return state;
  }
}
