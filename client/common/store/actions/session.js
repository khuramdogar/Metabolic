import ACTION_TYPES from './types';

const { API } = ACTION_TYPES;

export function startSignIn() {
  return { type: API.SIGNIN.START };
}

export function successSignIn(payload) {
  return { type: API.SIGNIN.SUCCESS, payload };
}

export function failureSignIn(error) {
  return { type: API.SIGNIN.FAILURE, error };
}

export function startSignOut() {
  return { type: API.SIGNOUT.START };
}

export function successSignOut(payload) {
  return { type: API.SIGNOUT.SUCCESS, payload };
}

export function failureSignOut(error) {
  return { type: API.SIGNOUT.FAILURE, error };
}

export default {
  startSignIn,
  successSignIn,
  failureSignIn,
  startSignOut,
  successSignOut,
  failureSignOut,
};
