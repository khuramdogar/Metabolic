import React, { Component } from 'react';
import Promise from 'bluebird';
import Loading from '../components/Loading';

export default function PromiseLoadingWrapper(propsPromisesFn, Wrapped, ErrorHandler) {
  return class Enhancer extends Component {
    componentWillMount() {
      this.setState({ loading: true, promisedProps: undefined, error: undefined });
      Promise.props(propsPromisesFn(this.props))
        .then((props = {}) => this.setState({ promisedProps: props }))
        .catch(error => this.setState({ error }))
        .finally(() => this.setState({ loading: false }));
    }

    render() {
      if (this.state.loading) {
        return <Loading />;
      }
      if (this.state.error && ErrorHandler) {
        return <ErrorHandler error={this.state.error} />;
      }
      const propsToMerge = [{}, this.props, this.state.promisedProps];
      if (this.state.error) {
        propsToMerge.push({ error: this.state.error });
      }
      const props = Object.assign(...propsToMerge);
      return <Wrapped {...props} />;
    }
  };
}
