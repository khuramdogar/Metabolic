import React from 'react';
import PropTypes from 'prop-types';
import Loading from '../components/Loading';

export default function LoadingWrapper(Wrapped) {
  const wrapper = (props) => {
    if (props.loading) {
      return <Loading />;
    }

    return <Wrapped {...props} />;
  };

  wrapper.propTypes = {
    loading: PropTypes.bool,
  };

  wrapper.defaultProps = {
    loading: false,
  };

  return wrapper;
}
