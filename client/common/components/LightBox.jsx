import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ReactImageLightBox from 'react-image-lightbox';

class LightBox extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { index: this.props.startingIndex };
    this.onMovePrevious = this.onMovePrevious.bind(this);
    this.onMoveNext = this.onMoveNext.bind(this);
  }

  onMovePrevious() {
    const { images } = this.props;
    this.setState(previousState =>
      ({ index: ((previousState.index + images.length) - 1) % images.length }));
  }

  onMoveNext() {
    this.setState(previousState =>
      ({ index: (previousState.index + 1) % this.props.images.length }));
  }

  render() {
    const { images } = this.props;
    const { index } = this.state;

    return (
      <ReactImageLightBox
        mainSrc={images[index]}
        nextSrc={images[(index + 1) % images.length]}
        prevSrc={images[((index + images.length) - 1) % images.length]}
        onCloseRequest={this.props.handleClose}
        onMovePrevRequest={this.onMovePrevious}
        onMoveNextRequest={this.onMoveNext}
        reactModalStyle={{ overlay: { zIndex: 1501 } }}
      />
    );
  }
}

LightBox.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string).isRequired,
  startingIndex: PropTypes.number,
  handleClose: PropTypes.func.isRequired,
};

LightBox.defaultProps = {
  startingIndex: 0,
};

export default LightBox;
