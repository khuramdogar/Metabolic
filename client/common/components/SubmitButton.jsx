import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';

export default function SubmitButton(props) {
  const { label, submitting, disabled } = props;
  return (<RaisedButton
    primary
    label={label}
    type="submit"
    labelPosition="before"
    disabled={disabled}
    icon={submitting ? <CircularProgress size={25} color="white" /> : null}
  />);
}

SubmitButton.propTypes = {
  disabled: PropTypes.bool,
  label: PropTypes.string,
  submitting: PropTypes.bool,
};

SubmitButton.defaultProps = {
  disabled: false,
  label: 'Submit',
  submitting: false,
};
