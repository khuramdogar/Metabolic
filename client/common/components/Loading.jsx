import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from 'material-ui/CircularProgress';
import injectSheet from 'react-jss';

const styles = {
  loading: {
    padding: '10px',
    margin: '15px',
    textAlign: 'center',
  },
};

function Loading({ classes }) {
  return <div className={classes.loading}><CircularProgress /></div>;
}

Loading.propTypes = {
  classes: PropTypes.shape({ footer: PropTypes.string }).isRequired,
};

export default injectSheet(styles)(Loading);
