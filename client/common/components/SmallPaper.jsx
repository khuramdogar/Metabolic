import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';

const paperStyles = {
  padding: '20px',
  textAlign: 'center',
  maxWidth: '600px',
  margin: '30px auto 0',
};

export default function SmallPaper({ children }) {
  return <Paper style={paperStyles}>{children}</Paper>;
}

SmallPaper.defaultProps = {
  children: null,
};

SmallPaper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};
