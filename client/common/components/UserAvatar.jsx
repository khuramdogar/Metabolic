import React from 'react';
import PropTypes from 'prop-types';
import Avatar from 'material-ui/Avatar';
import Account from 'material-ui/svg-icons/action/account-circle';
import { photoDataShape } from '../prop-types/agents';
import Image from './Image';

export default function UserAvatar(props) {
  if (props.avatarData) {
    return (
      <Image
        src={props.avatarData.thumbnailLocation || props.avatarData.location}
        alt="User Image"
        loadingIcon={Account}
        style={props.style}
        thumbnail
      />
    );
  }
  return <Avatar icon={<Account />} style={props.style} />;
}

UserAvatar.propTypes = {
  avatarData: photoDataShape,
  style: PropTypes.shape({}),
};

UserAvatar.defaultProps = {
  avatarData: undefined,
  style: undefined,
};
