import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import classNames from 'classnames';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import SvgIcon from 'material-ui/SvgIcon';
import styleConstants from '../style-constants';

const styles = {
  footer: {
    padding: '30px 10% 20px',
    position: 'fixed',
    bottom: 0,
    width: '80%',
    fontSize: '14px',
    background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 1) 20%, rgba(255, 255, 255, 1) 100%)',
    fallbacks: {
      background: '#fff',
    },
    textAlign: 'center',
    zIndex: 10,
  },
  icons: {
    '& a': {
      marginLeft: '10px',
    },
    marginTop: '20px',
  },
  [`@media ${styleConstants.responsiveMediaQueries.mediumLarge}`]: {
    left: {
      float: 'left',
    },
    right: {
      float: 'right',
    },
    footer: {
      '&:after': {
        content: '',
        clear: 'both',
        display: 'block',
      },
    },
    icons: {
      marginTop: 0,
    },
  },
};

const FacebookBoxIcon = props => (
  <SvgIcon {...props}>
    <path
      d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M18,5H15.5A3.5,
      3.5 0 0,0 12,8.5V11H10V14H12V21H15V14H18V11H15V9A1,1 0 0,1 16,8H18V5Z"
    />
  </SvgIcon>
);

function Footer({ classes }) {
  return (
    <footer className={classes.footer}>
      <div className={classes.left}>© 2016 Metabolic Inc. All rights reserved.</div>
      <div className={classNames(classes.right, classes.icons)}>
        <a href="https://facebook.com/metabolicinc"><FacebookBoxIcon /></a>
        <a href="mailto:info@mtblc.co"><EmailIcon /></a>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.shape({ footer: PropTypes.string }).isRequired,
};

export default injectSheet(styles)(Footer);
