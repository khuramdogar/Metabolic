import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

const styles = {
  actions: {
    marginTop: '20px',
  },
};

function FormActions({ classes, children }) {
  return <div className={classes.actions}>{children}</div>;
}

FormActions.propTypes = {
  classes: PropTypes.shape({ actions: PropTypes.string }).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

FormActions.defaultProps = {
  children: null,
};

export default injectSheet(styles)(FormActions);
