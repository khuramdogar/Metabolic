import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import moment from 'moment-timezone';
import FlatButton from 'material-ui/FlatButton';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import ArrowForward from 'material-ui/svg-icons/navigation/arrow-forward';
import Today from 'material-ui/svg-icons/action/today';

const styles = {
  titleWrapper: {
    width: '150px',
    textAlign: 'center',
  },
  leftNavigation: {
    width: '150px',
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-end',
  },
  rightNavigation: {
    width: '150px',
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
  },
  navigatorWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
  },
};

function dayTitle(date) {
  return date.calendar(moment(), {
    sameDay: '[Today]',
    nextDay: '[Tomorrow]',
    nextWeek: 'dddd',
    lastDay: '[Yesterday]',
    lastWeek: '[Last] dddd',
    sameElse: 'DD/MM/YYYY',
  });
}

function DaySwitcher({
  timezone,
  currentDate,
  disabled,
  onDaySwitchClick,
  classes,
}) {
  const date = moment.tz(currentDate, timezone);
  const today = moment().tz(timezone);
  const isToday = today.isSame(date, 'd');
  return (

    <div className={classes.navigatorWrapper}>
      <div className={classes.leftNavigation}>
        <FlatButton
          icon={<ArrowBack />}
          disabled={disabled}
          onClick={() => onDaySwitchClick(date.subtract(1, 'day'))}
          primary
        />
      </div>
      <div className={classes.titleWrapper}>
        {disabled ? '...' : dayTitle(date)}
      </div>
      <div className={classes.rightNavigation}>
        <FlatButton
          icon={<ArrowForward />}
          disabled={disabled || isToday}
          onClick={() => onDaySwitchClick(date.add(1, 'day'))}
          primary
        />
        <FlatButton
          title="Today"
          icon={<Today />}
          disabled={disabled || isToday}
          onClick={() => onDaySwitchClick(today)}
          primary
        />
      </div>
    </div>
  );
}

DaySwitcher.propTypes = {
  timezone: PropTypes.string.isRequired,
  currentDate: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]),
  disabled: PropTypes.bool,
  onDaySwitchClick: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    titleWrapper: PropTypes.string,
    navigatorWrapper: PropTypes.string,
    leftNavigation: PropTypes.string,
    rightNavigation: PropTypes.string,
  }).isRequired,
};

DaySwitcher.defaultProps = {
  disabled: false,
  currentDate: undefined,
};

export default injectSheet(styles)(DaySwitcher);
